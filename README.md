# ABR Project - Peter Staunton

This is the base directory for my ABR project. It's based on the [Pensieve](https://github.com/hongzimao/pensieve) deep reinforcement learning paper, using transfer learning to improve performance and training speed by starting with a pretrained model.
Additionally, an implementation of [Oboe](https://dl.acm.org/doi/10.1145/3230543.3230558) ([github](https://github.com/godka/oboe-reproduce)) was used for comparison, alongside standard rule-based

## Getting started

The project was developed using Ubuntu version 22.04.4 with python version 3.7.16 as this was the latest version compatible with tensorflow 1.x. This proved to be a significant limitation and ideally the project should be refactored using either Keras, Pytorch or just tensorflow 2.x.

The follow package versions were used:

- matploblib=3.5.3
- numpy=1.18.5=pypi_0
- pandas=1.3.5=pypi_0
- psutil=5.9.8=pypi_0
- pyvirtualdisplay=3.0=pypi_0
- scipy=1.7.3=pypi_0
- seaborn=0.12.2=pypi_0
- selenium=4.11.2=pypi_0
- sqlite=3.45.3=h5eee18b_0
- sympy=1.10.1=py37h06a4308_0
- tensorboard=1.15.0=pypi_0
- **tensorflow=1.15.5=pypi_0**
- tflearn=0.5.0=pypi_0

# Project Structre

- datasets : contains the raw traces used for training and testing as well as any scripts used for data preprocessing plus the resulting files
  - cooked_traces, cooked_test_traces: used to train model and test it in simulations
  - mahimahi, mahimahi_sliced: used to test algorithms with mahimahi and http video servers
- custom_pensieve : follows the project structure from Pensieve
  - sim: training neural network
  - test: evaluating neural network against other algorithms in simulation environment
  - rl_server: contains http servers for different algorithms to play video files
  - run_exp: runs experiments inside of Mahimahi network emulation shells recreating the network conditions from recorded network traces
- results: Contains screenshots of results for simulation and emulation tests and results in text format as well as final trained reinforcement learning (Pensieve/RL) and transfer learning (TF) models for each dataset
