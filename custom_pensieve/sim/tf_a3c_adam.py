import numpy as np
import tensorflow as tf
import tflearn
from tflearn.layers.normalization import batch_normalization
import logging
import warnings
warnings.filterwarnings("ignore", category=FutureWarning) # suppresses Numpy NaN vs None comparison warning
# Suppress specific TensorFlow warnings
warnings.filterwarnings('ignore', category=DeprecationWarning, module='tensorflow')
warnings.filterwarnings('ignore', category=DeprecationWarning, module='tflearn')

GAMMA = 0.99
A_DIM = 6
ENTROPY_WEIGHT = 1 # Now set from agent
ENTROPY_EPS = 1e-6
S_INFO = 4
LR_MODIFIER = 1.5 # Should be 1


class ActorNetwork(object):
    """
    Input to the network is the state, output is the distribution
    of all actions.
    """
    def __init__(self, sess, state_dim, action_dim, learning_rate, pretrained_weights_file=None, entropy_weight=ENTROPY_WEIGHT, num_frozen_layers=2):
        self.sess = sess
        self.s_dim = state_dim
        self.a_dim = action_dim
        self.lr_rate = learning_rate * LR_MODIFIER # Should be 1, adjusted to test/experiment
        self.pretrained_weights_file = pretrained_weights_file # if None, will set first 2 layers as trainable
        # Forcing cast to correct types: rl_test uses string sys arguments
        self.entropy_weight = float(entropy_weight) # Will enable setting entropy weight from tf_agent.py
        self.num_frozen_layers = int(num_frozen_layers) # Cannot dynammicaly freeze/unfreeze layers with tensorflow 1.x but allows me to set parameter from the agent more flexibly

        # Create the actor network
        self.inputs, self.out = self.create_actor_network()

        # Get all network parameters
        self.network_params = \
            tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES, scope='actor')
            
        # Initialize trainable_vars to all network params
        # should select network based on supplied number of frozen layers
        self.trainable_vars = self.network_params[self.num_frozen_layers:]

        # Set all network parameters
        self.input_network_params = []
        for param in self.network_params:
            self.input_network_params.append(
                tf.compat.v1.placeholder(tf.float32, shape=param.get_shape()))
        self.set_network_params_op = []
        for idx, param in enumerate(self.input_network_params):
            self.set_network_params_op.append(self.network_params[idx].assign(param))

        # Selected action, 0-1 vector
        self.acts = tf.compat.v1.placeholder(tf.float32, [None, self.a_dim])

        # This gradient will be provided by the critic network
        self.act_grad_weights = tf.compat.v1.placeholder(tf.float32, [None, 1])

        # Compute the objective (log action_vector and entropy)
        # entropy_weight replaces ENTROPY_WEIGHT
        self.obj = tf.compat.v1.reduce_sum(tf.compat.v1.multiply(
            tf.compat.v1.log(tf.reduce_sum(tf.compat.v1.multiply(self.out, self.acts),
                                 axis=1, keepdims=True)),
            -self.act_grad_weights)) \
                   + self.entropy_weight * tf.reduce_sum(tf.multiply(self.out,
                                                                tf.compat.v1.log(self.out + ENTROPY_EPS)))
                                
        # Combine/compute the gradients here
        self.actor_gradients = tf.gradients(self.obj, self.network_params)

        # Initialize all variables
        self.sess.run(tf.compat.v1.global_variables_initializer())

        # Load pretrained weights if provided
        if pretrained_weights_file: # Load pretrained weights where trainable=False
            self.load_pretrained_weights(pretrained_weights_file)
            
        # Update trainable_vars to exclude non-trainable variables
        self.trainable_vars = [v for v in self.network_params if v.trainable]

        # Create the optimization op
        # Gradient clipping would be inserted here to combat exploding gradients
        trainable_gradients = [g for g, v in zip(self.actor_gradients, self.network_params) if v in self.trainable_vars]
        if trainable_gradients:
            self.optimize = tf.compat.v1.train.RMSPropOptimizer(self.lr_rate).apply_gradients(
                zip(trainable_gradients, self.trainable_vars))
        else:
            print("Warning: No trainable gradients found. The network may not be able to learn.")
            self.optimize = tf.no_op()

    # In tensorflow 1.x, static graphs used so cannot dynamically adjust the trainable parameter
    # self.num_frozen_layers used to set the number of frozen layers at network creation
    def create_actor_network(self):
        with tf.compat.v1.variable_scope('actor', reuse=tf.compat.v1.AUTO_REUSE):
            inputs = tflearn.input_data(shape=[None, self.s_dim[0], self.s_dim[1]])

            split_0 = tflearn.fully_connected(inputs[:, 0:1, -1], 128, activation='relu', trainable=self.num_frozen_layers <= 0)
            # Using batch normalization to stabilize data, frozen layers will still have inputs normalized for training
            # Running mean/variance used instead
            split_0 = batch_normalization(split_0, trainable=self.num_frozen_layers <= 0)
            
            split_1 = tflearn.fully_connected(inputs[:, 1:2, -1], 128, activation='relu', trainable=self.num_frozen_layers <= 1)
            split_1 = batch_normalization(split_1, trainable=self.num_frozen_layers <= 1)
            
            split_2 = tflearn.conv_1d(inputs[:, 2:3, :], 128, 4, activation='relu', trainable=self.num_frozen_layers <= 2)
            split_2 = batch_normalization(split_2, trainable=self.num_frozen_layers <= 2)
            
            split_3 = tflearn.conv_1d(inputs[:, 3:4, :], 128, 4, activation='relu', trainable=self.num_frozen_layers <= 3)
            split_3 = batch_normalization(split_3, trainable=self.num_frozen_layers <= 3)
            
            split_4 = tflearn.conv_1d(inputs[:, 4:5, :A_DIM], 128, 4, activation='relu', trainable=self.num_frozen_layers <= 4)
            split_4 = batch_normalization(split_4, trainable=self.num_frozen_layers <= 4)
            
            split_5 = tflearn.fully_connected(inputs[:, 5:6, -1], 128, activation='relu', trainable=self.num_frozen_layers <= 5)
            split_5 = batch_normalization(split_5, trainable=self.num_frozen_layers <= 5)

            split_2_flat = tflearn.flatten(split_2)
            split_3_flat = tflearn.flatten(split_3)
            split_4_flat = tflearn.flatten(split_4)

            merge_net = tflearn.merge([split_0, split_1, split_2_flat, split_3_flat, split_4_flat, split_5], 'concat')

            dense_net = tflearn.fully_connected(merge_net, 128, activation='relu', trainable=True)
            dense_net = batch_normalization(dense_net, trainable=True)
            
            out = tflearn.fully_connected(dense_net, self.a_dim, activation='softmax', trainable=True)

        return inputs, out
    
    # Load pretrained weights from weights_file for frozen layers
    def load_pretrained_weights(self, weights_file):
        pretrained_weights = np.load(weights_file, allow_pickle=True)
        print("Shape of pretrained weights:", pretrained_weights.shape)
        
        # Get only the first two layers (non-trainable variables) in the 'actor' scope
        pretrained_vars = self.network_params[:self.num_frozen_layers]
        
        if len(pretrained_weights) != len(pretrained_vars):
            print(f"Warning: Number of weights ({len(pretrained_weights)}) does not match number of non-trainable variables ({len(pretrained_vars)})")
        
        # Create a list of assignments
        assign_ops = []
        weights_loaded = False
        for var, weight in zip(pretrained_vars, pretrained_weights):
            if var.shape == weight.shape:
                assign_ops.append(tf.compat.v1.assign(var, weight))
                print(f"Loaded weight for: {var.name}, shape: {weight.shape}")
                weights_loaded = True
            else:
                print(f"Warning: Shape mismatch for {var.name}. Expected {var.shape}, got {weight.shape}")
        
        # Run all the assignment operations
        self.sess.run(assign_ops)
        
        if weights_loaded:
            print("Pretrained weights loaded successfully for non-trainable layers.")
        else:
            print("No pretrained weights were loaded.")
        
        print(f"Skipped {len(pretrained_weights) - len(assign_ops)} weights.")
        
        return weights_loaded
            
    def train(self, inputs, acts, act_grad_weights):
        feed_dict = {
            self.inputs: inputs,
            self.acts: acts,
            self.act_grad_weights: act_grad_weights
        }
        
        self.sess.run(self.optimize, feed_dict=feed_dict)


    def predict(self, inputs):
        return self.sess.run(self.out, feed_dict={
            self.inputs: inputs
        })

    def get_gradients(self, inputs, acts, act_grad_weights):
        return self.sess.run([grad for grad in self.actor_gradients if grad is not None], feed_dict={
            self.inputs: inputs,
            self.acts: acts,
            self.act_grad_weights: act_grad_weights
        })
        
    def apply_gradients(self, actor_gradients):
        return self.sess.run(self.optimize, feed_dict={
            i: d for i, d in zip(self.actor_gradients, actor_gradients) if i is not None
        })
        
    def get_network_params(self):
        return self.sess.run(self.network_params)

    def set_network_params(self, input_network_params):
        self.sess.run(self.set_network_params_op, feed_dict={
            i: d for i, d in zip(self.input_network_params, input_network_params)
        })
    
    # Adjust entropy weight from tf_agent as epoch increases
    def set_entropy_weight(self, new_weight):
        self.entropy_weight = new_weight
        
    # Set weights for layers directly: When restoring want to create completely new network
    # New network can change number of frozeon layers then load previous weights
    # Unused - didnt get feature working in agent
    def set_weights(self, weights):
        self.sess.run([tf.compat.v1.assign(v, w) 
                       for v, w in zip(self.network_params, weights)])

class CriticNetwork(object):
    """
    Input to the network is the state and action, output is V(s).
    On policy: the action must be obtained from the output of the Actor network.
    """
    # def __init__(self, sess, state_dim, learning_rate):
    def __init__(self, sess, state_dim, learning_rate, pretrained_weights_file=None, num_frozen_layers=0):
        self.sess = sess
        self.s_dim = state_dim
        self.lr_rate = learning_rate * LR_MODIFIER # will be 1 ideally, easier to adjust as constant
        self.pretrained_weights_file = pretrained_weights_file
        self.num_frozen_layers = int(num_frozen_layers)

        # Create the critic network
        self.inputs, self.out = self.create_critic_network()

        # Get all network parameters
        self.network_params = \
            tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES, scope='critic')

        # Initialize trainable_vars to all network params except frozen layers
        self.trainable_vars = self.network_params[self.num_frozen_layers:]

        # Set all network parameters
        self.input_network_params = []
        for param in self.network_params:
            self.input_network_params.append(
                tf.compat.v1.placeholder(tf.float32, shape=param.get_shape()))
        self.set_network_params_op = []
        for idx, param in enumerate(self.input_network_params):
            self.set_network_params_op.append(self.network_params[idx].assign(param))

        # Network target V(s)
        self.td_target = tf.compat.v1.placeholder(tf.float32, [None, 1])

        # Temporal Difference, will also be weights for actor_gradients
        self.td = tf.subtract(self.td_target, self.out)

        # Mean square error
        self.loss = tflearn.mean_square(self.td_target, self.out)

        # Compute critic gradient
        self.critic_gradients = tf.gradients(self.loss, self.trainable_vars)

        # Optimization Op
        self.optimize = tf.compat.v1.train.RMSPropOptimizer(self.lr_rate).apply_gradients(
            zip(self.critic_gradients, self.trainable_vars))
          
        # Numpy weights from pretrained model
        if pretrained_weights_file:
            # Weights loaded but no layers frozen
            self.load_pretrained_weights(pretrained_weights_file)

    # Same as actor network, variable number of frozen layers at creation based on self.num_frozen_layers
    def create_critic_network(self):
        with tf.compat.v1.variable_scope('critic'):
            inputs = tflearn.input_data(shape=[None, self.s_dim[0], self.s_dim[1]])

            split_0 = tflearn.fully_connected(inputs[:, 0:1, -1], 128, activation='relu', trainable=self.num_frozen_layers <= 0)
            split_1 = tflearn.fully_connected(inputs[:, 1:2, -1], 128, activation='relu', trainable=self.num_frozen_layers <= 1)
            split_2 = tflearn.conv_1d(inputs[:, 2:3, :], 128, 4, activation='relu', trainable=self.num_frozen_layers <= 2)
            split_3 = tflearn.conv_1d(inputs[:, 3:4, :], 128, 4, activation='relu', trainable=self.num_frozen_layers <= 3)
            split_4 = tflearn.conv_1d(inputs[:, 4:5, :A_DIM], 128, 4, activation='relu', trainable=self.num_frozen_layers <= 4)
            split_5 = tflearn.fully_connected(inputs[:, 5:6, -1], 128, activation='relu', trainable=self.num_frozen_layers <= 5)

            split_2_flat = tflearn.flatten(split_2)
            split_3_flat = tflearn.flatten(split_3)
            split_4_flat = tflearn.flatten(split_4)

            merge_net = tflearn.merge([split_0, split_1, split_2_flat, split_3_flat, split_4_flat, split_5], 'concat')

            dense_net_0 = tflearn.fully_connected(merge_net, 128, activation='relu', trainable=self.num_frozen_layers <= 6)
            out = tflearn.fully_connected(dense_net_0, 1, activation='linear', trainable=True)
            return inputs, out
        

    def load_pretrained_weights(self, weights_file):
        pretrained_weights = np.load(weights_file, allow_pickle=True)
        print("Shape of pretrained weights:", pretrained_weights.shape)
        
        # Get only the frozen layers in the 'critic' scope
        pretrained_vars = self.network_params[:self.num_frozen_layers]
        
        if len(pretrained_weights) != len(pretrained_vars):
            print(f"Warning: Number of weights ({len(pretrained_weights)}) does not match number of non-trainable variables ({len(pretrained_vars)})")
        
        # Create a list of assignments
        assign_ops = []
        weights_loaded = False
        for var, weight in zip(pretrained_vars, pretrained_weights):
            if var.shape == weight.shape:
                assign_ops.append(tf.compat.v1.assign(var, weight))
                print(f"Loaded weight for: {var.name}, shape: {weight.shape}")
                weights_loaded = True
            else:
                print(f"Warning: Shape mismatch for {var.name}. Expected {var.shape}, got {weight.shape}")
        
        # Run all the assignment operations
        self.sess.run(assign_ops)
        
        if weights_loaded:
            print("Pretrained weights loaded successfully for non-trainable layers.")
        else:
            print("No pretrained weights were loaded.")
        
        print(f"Skipped {len(pretrained_weights) - len(assign_ops)} weights.")
        
        return weights_loaded


    def train(self, inputs, td_target):
        return self.sess.run([self.loss, self.optimize], feed_dict={
            self.inputs: inputs,
            self.td_target: td_target
        })

    def predict(self, inputs):
        return self.sess.run(self.out, feed_dict={
            self.inputs: inputs
        })

    def get_td(self, inputs, td_target):
        return self.sess.run(self.td, feed_dict={
            self.inputs: inputs,
            self.td_target: td_target
        })

    def get_gradients(self, inputs, td_target):
        return self.sess.run(self.critic_gradients, feed_dict={
            self.inputs: inputs,
            self.td_target: td_target
        })

    def apply_gradients(self, critic_gradients):
        return self.sess.run(self.optimize, feed_dict={
            i: d for i, d in zip(self.critic_gradients, critic_gradients)
        })
        
    def get_network_params(self):
        return self.sess.run(self.network_params)

    def set_network_params(self, input_network_params):
        self.sess.run(self.set_network_params_op, feed_dict={
            i: d for i, d in zip(self.input_network_params, input_network_params)
        })
        
    # Set weights for layers directly: When restoring want to create completely new network
    # New network can change number of frozeon layers then load previous weights
    # Unused - didnt get feature working in agent
    def set_weights(self, weights):
        self.sess.run([tf.compat.v1.assign(v, w) 
                       for v, w in zip(self.network_params, weights)])


def compute_gradients(s_batch, a_batch, r_batch, terminal, actor, critic):
    """
    batch of s, a, r is from samples in a sequence
    the format is in np.array([batch_size, s/a/r_dim])
    terminal is True when sequence ends as a terminal state
    """
    assert s_batch.shape[0] == a_batch.shape[0]
    assert s_batch.shape[0] == r_batch.shape[0]
    ba_size = s_batch.shape[0]

    v_batch = critic.predict(s_batch)

    R_batch = np.zeros(r_batch.shape)

    if terminal:
        R_batch[-1, 0] = 0  # terminal state
    else:
        R_batch[-1, 0] = v_batch[-1, 0]  # boot strap from last state

    for t in reversed(range(ba_size - 1)):
        R_batch[t, 0] = r_batch[t] + GAMMA * R_batch[t + 1, 0]

    td_batch = R_batch - v_batch

    actor_gradients = actor.get_gradients(s_batch, a_batch, td_batch)
    critic_gradients = critic.get_gradients(s_batch, R_batch)

    return actor_gradients, critic_gradients, td_batch

def discount(x, gamma):
    """
    Given vector x, computes a vector y such that
    y[i] = x[i] + gamma * x[i+1] + gamma^2 x[i+2] + ...
    """
    out = np.zeros(len(x))
    out[-1] = x[-1]
    for i in reversed(range(len(x)-1)):
        out[i] = x[i] + gamma*out[i+1]
    assert x.ndim >= 1
    # More efficient version:
    # scipy.signal.lfilter([1],[1,-gamma],x[::-1], axis=0)[::-1]
    return out


def compute_entropy(x):
    """
    Given vector x, computes the entropy
    H(x) = - sum( p * log(p))
    """
    H = 0.0
    for i in range(len(x)):
        if 0 < x[i] < 1:
            H -= x[i] * np.log(x[i])
    return H

def build_summaries():
    td_loss = tf.compat.v1.Variable(0.)
    tf.compat.v1.summary.scalar("TD_loss", td_loss)
    eps_total_reward = tf.compat.v1.Variable(0.)
    tf.compat.v1.summary.scalar("Eps_total_reward", eps_total_reward)
    avg_entropy = tf.compat.v1.Variable(0.)
    tf.compat.v1.summary.scalar("Avg_entropy", avg_entropy)

    summary_vars = [td_loss, eps_total_reward, avg_entropy]
    summary_ops = tf.compat.v1.summary.merge_all()

    return summary_ops, summary_vars

