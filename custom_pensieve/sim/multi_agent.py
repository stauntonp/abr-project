import datetime
import os, time
import logging
import numpy as np
import multiprocessing as mp
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"#,1"
import tensorflow as tf
import env
import a3c
import load_trace
import re
import shutil
import math

S_INFO = 6  # bit_rate, buffer_size, next_chunk_size, bandwidth_measurement(throughput and time), chunk_til_video_end
S_LEN = 8  # take how many frames in the past
A_DIM = 6
ACTOR_LR_RATE = 0.0001
CRITIC_LR_RATE = 0.001
NUM_AGENTS = 8
TRAIN_SEQ_LEN = 100  # take as a train batch
MODEL_SAVE_INTERVAL = 100
VIDEO_BIT_RATE = [300,750,1200,1850,2850,4300]  # Kbps
HD_REWARD = [1, 2, 3, 12, 15, 20]
BUFFER_NORM_FACTOR = 10.0
CHUNK_TIL_VIDEO_END_CAP = 48.0
M_IN_K = 1000.0
REBUF_PENALTY = 4.3  # 1 sec rebuffering -> 3 Mbps
SMOOTH_PENALTY = 1
DEFAULT_QUALITY = 1  # default video quality without agent
RANDOM_SEED = 42
RAND_RANGE = 1000

SUMMARY_DIR = "./results/summary"
LOG_FILE = "./results/log/log"
TEST_LOG_FOLDER = './test_results/'
TRAIN_TRACES = './cooked_traces/'
RESULTS_DIR = './results/' # + curr_time = individual run
# NN_MODEL = './results/pretrain_linear_reward.ckpt'
# NN_MODEL = '/home/peter/abr-project/custom_pensieve/sim/results/20240627-112625 synth 200k/nn_model_ep_200000.ckpt'
NN_MODEL = './results/summary/nn_model_ep_108700.ckpt'
# NN_MODEL = None

# Parameters for agent and actor/critic network
MAX_EPOCHS = 200000
CHECKPOINT_SAVE_INTERVAL = 25000 # automatically write checkpoint to result dir
STARTING_ENTROPY_WEIGHT = 4 # set from agent instead of a3c
FINAL_ENTROPY_WEIGHT = 0.1
NO_ENTROPY_DECAY = 100000 # Epoch to start decaying entropy weight
FINISH_ENTROPY_DECAY = 150000 # Epoch to reach final entropy weight
ENTROPY_UPDATE_INTERVAL = 5000

LINEAR_DECAY = 'linear'
EXPONENTIAL_DECAY = 'exponential'
DECAY_TYPE = LINEAR_DECAY

# Function to extract epoch from checkpoint filename
def extract_epoch_from_filename(filename):
    match = re.search(r'_ep_(\d+)', filename)
    if match:
        return int(match.group(1))
    else:
        return 0
    
# Write program parameters to a text file in results dir - easier to keep track of changes between runs
# Taken from tf_agent
def save_parameters(results_dir):
    os.makedirs(results_dir, exist_ok=True)
    param_file_path = os.path.join(results_dir, 'run_parameters.log')
    
    with open(param_file_path, 'w') as f:
        f.write("Program Parameters multi_agent.py:\n\n")
        f.write(f"ACTOR_LR_RATE: {ACTOR_LR_RATE}\n")
        f.write(f"CRITIC_LR_RATE: {CRITIC_LR_RATE}\n")
        f.write(f"TRAIN_SEQ_LEN: {TRAIN_SEQ_LEN}\n")
        f.write(f"MODEL_SAVE_INTERVAL: {MODEL_SAVE_INTERVAL}\n")
        f.write(f"RANDOM_SEED: {RANDOM_SEED}\n")
        f.write(f"NN_MODEL: {NN_MODEL}\n\n")
        # f.write(f"CONTINUE_TRAINING: {CONTINUE_TRAINING}\n")
        # f.write(f"PRETRAINED_ACTOR_WEIGHT: {PRETRAINED_ACTOR_WEIGHT}\n")
        # f.write(f"PRETRAINED_CRITIC_WEIGHT: {PRETRAINED_CRITIC_WEIGHT}\n")
        f.write(f"MAX_EPOCHS: {MAX_EPOCHS}\n")
        f.write(f"CHECKPOINT_SAVE_INTERVAL: {CHECKPOINT_SAVE_INTERVAL}\n")
        f.write(f"STARTING_ENTROPY_WEIGHT: {STARTING_ENTROPY_WEIGHT}\n")
        f.write(f"FINAL_ENTROPY_WEIGHT: {FINAL_ENTROPY_WEIGHT}\n")
        f.write(f"NO_ENTROPY_DECAY: {NO_ENTROPY_DECAY}\n")
        f.write(f"FINISH_ENTROPY_DECAY: {FINISH_ENTROPY_DECAY}\n")
        f.write(f"ENTROPY_UPDATE_INTERVAL: {ENTROPY_UPDATE_INTERVAL}\n")
        f.write(f"DECAY_TYPE: {DECAY_TYPE}\n")
        f.write(f"\nComments:")

    print(f"Parameters saved to {param_file_path}")
    
# Code reused a few times, better to have in new function for ease of changes
# Decays entropy weight from initial to final between start_decay and finish_decay epochs
def set_entropy_weight(epoch, initial=STARTING_ENTROPY_WEIGHT, final=FINAL_ENTROPY_WEIGHT, 
                       start_decay=NO_ENTROPY_DECAY, finish_decay=FINISH_ENTROPY_DECAY, decay_type=DECAY_TYPE):
    if epoch <= start_decay:
        return initial
    elif start_decay < epoch < finish_decay:
        # Gradual decay per epoch
        if decay_type == LINEAR_DECAY:
            progress = (epoch - start_decay) / (finish_decay - start_decay)
            return initial - (initial - final) * progress
        # Starts fast and evens out
        elif decay_type == EXPONENTIAL_DECAY:
            decay_rate = -math.log(final / initial) / (finish_decay - start_decay)
            return initial * math.exp(-decay_rate * (epoch - start_decay))
        else:
            raise ValueError(f"Unknown decay type: {decay_type}")
    else:
        return final

def testing(epoch, nn_model, log_file):
    # clean up the test results folder
    os.system('rm -r ' + TEST_LOG_FOLDER)
    if not os.path.exists(TEST_LOG_FOLDER):
        os.makedirs(TEST_LOG_FOLDER)
    # run test script
    # os.system('python rl_test.py ' + nn_model)
    entropy_weight = set_entropy_weight(epoch)
    os.system(f"python rl_test.py {nn_model} {entropy_weight}")
    
    # append test performance to the log
    rewards, entropies = [], []
    test_log_files = os.listdir(TEST_LOG_FOLDER)
    for test_log_file in test_log_files:
        reward, entropy = [], []
        with open(TEST_LOG_FOLDER + test_log_file, 'r') as f:
            for line in f:
                parse = line.split()
                try:
                    entropy.append(float(parse[-2]))
                    reward.append(float(parse[-1]))
                except IndexError:
                    break
        rewards.append(np.sum(reward[1:]))
        entropies.append(np.mean(entropy[1:]))

    rewards = np.array(rewards)
    rewards_min = np.min(rewards)
    rewards_5per = np.percentile(rewards, 5)
    rewards_mean = np.mean(rewards)
    rewards_median = np.percentile(rewards, 50)
    rewards_95per = np.percentile(rewards, 95)
    rewards_max = np.max(rewards)

    log_file.write(str(epoch) + '\t' +
                   str(rewards_min) + '\t' +
                   str(rewards_5per) + '\t' +
                   str(rewards_mean) + '\t' +
                   str(rewards_median) + '\t' +
                   str(rewards_95per) + '\t' +
                   str(rewards_max) + '\n')
    log_file.flush()

    return rewards_mean, np.mean(entropies)


def central_agent(net_params_queues, exp_queues):

    assert len(net_params_queues) == NUM_AGENTS
    assert len(exp_queues) == NUM_AGENTS

    logging.basicConfig(filename=LOG_FILE + '_central',
                        filemode='w',
                        level=logging.INFO)

    with tf.compat.v1.Session() as sess, open(LOG_FILE + '_test', 'w') as test_log_file:

        actor = a3c.ActorNetwork(sess,
                                 state_dim=[S_INFO, S_LEN], action_dim=A_DIM,
                                 learning_rate=ACTOR_LR_RATE,
                                 entropy_weight=STARTING_ENTROPY_WEIGHT)
        critic = a3c.CriticNetwork(sess,
                                   state_dim=[S_INFO, S_LEN],
                                   learning_rate=CRITIC_LR_RATE)

        summary_ops, summary_vars = a3c.build_summaries()

        sess.run(tf.compat.v1.global_variables_initializer())
        curr_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        results_dir = RESULTS_DIR + curr_time
        save_parameters(results_dir) # Write system parameters to log file - much easier to remember training run details this way
        TRAIN_SUMMARY_DIR = results_dir + '/train'
        TEST_SUMMARY_DIR = results_dir + '/test'
        writer = tf.compat.v1.summary.FileWriter(TRAIN_SUMMARY_DIR, sess.graph)  # training monitor
        test_writer = tf.compat.v1.summary.FileWriter(TEST_SUMMARY_DIR, sess.graph)  # training monitor
        saver = tf.compat.v1.train.Saver()  # save neural net parameters

        # restore neural net parameters
        nn_model = NN_MODEL
        if nn_model is not None:  # nn_model is the path to file
            saver.restore(sess, nn_model)
            # Extract the epoch from the filename
            epoch = extract_epoch_from_filename(nn_model)
            print(f"Model restored: epoch {epoch}.")
        else:
            epoch = 0
            
        test_avg_reward, test_avg_entropy, test_avg_td_loss = 0, 0.5, 0

        # assemble experiences from agents, compute the gradients
        max_epochs = MAX_EPOCHS # code from tf_agent to automate entropy changes in 1 script
        current_entropy_weight = STARTING_ENTROPY_WEIGHT
        last_entropy_update_epoch = 0
        while True:

            # Only want to update entropy weight after at least ENTROPY_UPDATE_INTERVAL epochs
            # Updating every epoch caused instability during update period
            if epoch - last_entropy_update_epoch >= ENTROPY_UPDATE_INTERVAL:
                if NO_ENTROPY_DECAY <= epoch <= FINISH_ENTROPY_DECAY:
                    current_entropy_weight = set_entropy_weight(epoch)
                    # actor.set_entropy_weight(current_entropy_weight)
                    actor.update_entropy_weight(current_entropy_weight) # redefines objective function with new entropy_weight
                last_entropy_update_epoch = epoch
                logging.info(f"Epoch {epoch}: Updated entropy weight to {current_entropy_weight}")
            
            # synchronize the network parameters of work agent
            actor_net_params = actor.get_network_params()
            critic_net_params = critic.get_network_params()
            for i in range(NUM_AGENTS):
                net_params_queues[i].put([actor_net_params, critic_net_params])
                # Note: this is synchronous version of the parallel training,
                # which is easier to understand and probe. The framework can be
                # fairly easily modified to support asynchronous training.
                # Some practices of asynchronous training (lock-free SGD at
                # its core) are nicely explained in the following two papers:
                # https://arxiv.org/abs/1602.01783
                # https://arxiv.org/abs/1106.5730

            # record average reward and td loss change
            # in the experiences from the agents
            total_batch_len = 0.0
            total_reward = 0.0
            total_td_loss = 0.0
            total_entropy = 0.0
            total_agents = 0.0 

            # assemble experiences from the agents
            actor_gradient_batch = []
            critic_gradient_batch = []

            for i in range(NUM_AGENTS):
                s_batch, a_batch, r_batch, terminal, info = exp_queues[i].get()

                actor_gradient, critic_gradient, td_batch = \
                    a3c.compute_gradients(
                        s_batch=np.stack(s_batch, axis=0),
                        a_batch=np.vstack(a_batch),
                        r_batch=np.vstack(r_batch),
                        terminal=terminal, actor=actor, critic=critic)

                actor_gradient_batch.append(actor_gradient)
                critic_gradient_batch.append(critic_gradient)

                total_reward += np.sum(r_batch)
                total_td_loss += np.sum(td_batch)
                total_batch_len += len(r_batch)
                total_agents += 1.0
                total_entropy += np.sum(info['entropy'])

            # compute aggregated gradient
            assert NUM_AGENTS == len(actor_gradient_batch)
            assert len(actor_gradient_batch) == len(critic_gradient_batch)
            for i in range(len(actor_gradient_batch)):
                actor.apply_gradients(actor_gradient_batch[i])
                critic.apply_gradients(critic_gradient_batch[i])

            # log training information
            epoch += 1
            avg_reward = total_reward  / total_agents
            avg_td_loss = total_td_loss / total_batch_len
            avg_entropy = total_entropy / total_batch_len

            logging.info('Epoch: ' + str(epoch) +
                         ' TD_loss: ' + str(avg_td_loss) +
                         ' Avg_reward: ' + str(avg_reward) +
                         ' Avg_entropy: ' + str(avg_entropy))

            #Training summary
            summary_str = sess.run(summary_ops, feed_dict={
                summary_vars[0]: avg_td_loss,
                summary_vars[1]: avg_reward,
                summary_vars[2]: avg_entropy
            })
            writer.add_summary(summary_str, epoch)
            writer.flush()
            # Testing summary
            summary_str = sess.run(summary_ops, feed_dict={
                summary_vars[0]: test_avg_td_loss,
                summary_vars[1]: test_avg_reward,
                summary_vars[2]: test_avg_entropy
            })

            test_writer.add_summary(summary_str, epoch)
            test_writer.flush()

            if epoch % MODEL_SAVE_INTERVAL == 0:
                # Save the neural net parameters to disk (in SUMMARY_DIR).
                save_path = saver.save(sess, os.path.join(SUMMARY_DIR, f"nn_model_ep_{epoch}.ckpt"))
                logging.info(f"Model saved in SUMMARY_DIR: {save_path}")
                logging.info(f"Current entropy weight: {current_entropy_weight}")

                # Run testing
                test_avg_reward, test_avg_entropy = testing(epoch, save_path, test_log_file)
                
                # Update test averages for summary
                test_avg_td_loss = avg_td_loss  # Might want to compute this separately for test data
                
                # Log test results
                logging.info(f"Testing - Avg_reward: {test_avg_reward}, Avg_entropy: {test_avg_entropy}")

                # At bigger intervals, also save checkpoint to results_dir
                if epoch % CHECKPOINT_SAVE_INTERVAL == 0:
                    # os.makedirs(results_dir_path, exist_ok=True)
                    
                    # Copy .ckpt files directly
                    for ext in [".index", ".data-00000-of-00001", ".meta"]:
                        src = save_path + ext
                        dst = os.path.join(results_dir, f"nn_model_ep_{epoch}.ckpt" + ext)
                        try:
                            shutil.copy2(src, dst)
                            logging.info(f"Copied {src} to {dst}")
                            
                            # Verify file content
                            with open(src, 'rb') as f_src, open(dst, 'rb') as f_dst:
                                if f_src.read() == f_dst.read():
                                    logging.info(f"Confirmed: Checkpoint file created and verified at {dst}")
                                else:
                                    logging.error(f"Checkpoint file at {dst} does not match source file")
                        except Exception as e:
                            logging.error(f"Error copying {src} to {dst}: {e}")
                                        
            if epoch >= max_epochs:
                break # break out of training loop after specified max epochs
    logging.info('Training completed after {} epochs'.format(epoch))
    return 0

def agent(agent_id, all_cooked_time, all_cooked_bw, net_params_queue, exp_queue, current_epoch=0):

    net_env = env.Environment(all_cooked_time=all_cooked_time,
                              all_cooked_bw=all_cooked_bw,
                              random_seed=agent_id)

    with tf.compat.v1.Session() as sess, open(LOG_FILE + '_agent_' + str(agent_id), 'w') as log_file:
        current_entropy_weight = set_entropy_weight(current_epoch)
        
        actor = a3c.ActorNetwork(sess,
                                 state_dim=[S_INFO, S_LEN], action_dim=A_DIM,
                                 learning_rate=ACTOR_LR_RATE,
                                 entropy_weight=current_entropy_weight)
        critic = a3c.CriticNetwork(sess,
                                   state_dim=[S_INFO, S_LEN],
                                   learning_rate=CRITIC_LR_RATE)

        # initial synchronization of the network parameters from the coordinator
        actor_net_params, critic_net_params = net_params_queue.get()
        actor.set_network_params(actor_net_params)
        critic.set_network_params(critic_net_params)

        last_bit_rate = DEFAULT_QUALITY
        bit_rate = DEFAULT_QUALITY

        action_vec = np.zeros(A_DIM)
        action_vec[bit_rate] = 1

        s_batch = [np.zeros((S_INFO, S_LEN))]
        a_batch = [action_vec]
        r_batch = []
        entropy_record = []

        time_stamp = 0
        while True:  # experience video streaming forever

            # the action is from the last decision
            # this is to make the framework similar to the real
            delay, sleep_time, buffer_size, rebuf, \
            video_chunk_size, next_video_chunk_sizes, \
            end_of_video, video_chunk_remain = \
                net_env.get_video_chunk(bit_rate)

            time_stamp += delay  # in ms
            time_stamp += sleep_time  # in ms

            # -- linear reward --
            # reward is video quality - rebuffer penalty - smoothness
            reward = VIDEO_BIT_RATE[bit_rate] / M_IN_K \
                     - REBUF_PENALTY * rebuf \
                     - SMOOTH_PENALTY * np.abs(VIDEO_BIT_RATE[bit_rate] -
                                               VIDEO_BIT_RATE[last_bit_rate]) / M_IN_K

            # -- log scale reward --
            # log_bit_rate = np.log(VIDEO_BIT_RATE[bit_rate] / float(VIDEO_BIT_RATE[-1]))
            # log_last_bit_rate = np.log(VIDEO_BIT_RATE[last_bit_rate] / float(VIDEO_BIT_RATE[-1]))

            # reward = log_bit_rate \
            #          - REBUF_PENALTY * rebuf \
            #          - SMOOTH_PENALTY * np.abs(log_bit_rate - log_last_bit_rate)

            # -- HD reward --
            # reward = HD_REWARD[bit_rate] \
            #          - REBUF_PENALTY * rebuf \
            #          - SMOOTH_PENALTY * np.abs(HD_REWARD[bit_rate] - HD_REWARD[last_bit_rate])

            r_batch.append(reward)

            last_bit_rate = bit_rate

            # retrieve previous state
            if len(s_batch) == 0:
                state = np.zeros((S_INFO, S_LEN))
            else:
                state = np.array(s_batch[-1], copy=True)

            # dequeue history record
            state = np.roll(state, -1, axis=1)

            # this should be S_INFO number of terms
            state[0, -1] = VIDEO_BIT_RATE[bit_rate] / float(np.max(VIDEO_BIT_RATE))  # last quality
            state[1, -1] = buffer_size / BUFFER_NORM_FACTOR  # 10 sec
            state[2, -1] = float(video_chunk_size) / float(delay) / M_IN_K  # kilo byte / ms
            state[3, -1] = float(delay) / M_IN_K / BUFFER_NORM_FACTOR  # 10 sec
            state[4, :A_DIM] = np.array(next_video_chunk_sizes) / M_IN_K / M_IN_K  # mega byte
            state[5, -1] = np.minimum(video_chunk_remain, CHUNK_TIL_VIDEO_END_CAP) / float(CHUNK_TIL_VIDEO_END_CAP)

            # compute action probability vector
            action_prob = actor.predict(np.reshape(state, (1, S_INFO, S_LEN)))
            action_cumsum = np.cumsum(action_prob)
            bit_rate = (action_cumsum > np.random.randint(1, RAND_RANGE) / float(RAND_RANGE)).argmax()
            # Note: we need to discretize the probability into 1/RAND_RANGE steps,
            # because there is an intrinsic discrepancy in passing single state and batch states

            entropy_record.append(a3c.compute_entropy(action_prob[0]))

            # log time_stamp, bit_rate, buffer_size, reward
            log_file.write(str(time_stamp) + '\t' +
                           str(VIDEO_BIT_RATE[bit_rate]) + '\t' +
                           str(buffer_size) + '\t' +
                           str(rebuf) + '\t' +
                           str(video_chunk_size) + '\t' +
                           str(delay) + '\t' +
                           str(reward) + '\n')
            log_file.flush()

            # report experience to the coordinator
            if len(r_batch) >= TRAIN_SEQ_LEN or end_of_video:
                exp_queue.put([s_batch[1:],  # ignore the first chuck
                               a_batch[1:],  # since we don't have the
                               r_batch[1:],  # control over it
                               end_of_video,
                               {'entropy': entropy_record}])

                # synchronize the network parameters from the coordinator
                actor_net_params, critic_net_params = net_params_queue.get()
                actor.set_network_params(actor_net_params)
                critic.set_network_params(critic_net_params)

                del s_batch[:]
                del a_batch[:]
                del r_batch[:]
                del entropy_record[:]

                log_file.write('\n')  # so that in the log we know where video ends

            # store the state and action into batches
            if end_of_video:
                last_bit_rate = DEFAULT_QUALITY
                bit_rate = DEFAULT_QUALITY  # use the default action here

                action_vec = np.zeros(A_DIM)
                action_vec[bit_rate] = 1

                s_batch.append(np.zeros((S_INFO, S_LEN)))
                a_batch.append(action_vec)

            else:
                s_batch.append(state)

                action_vec = np.zeros(A_DIM)
                action_vec[bit_rate] = 1
                a_batch.append(action_vec)


def main():

    np.random.seed(RANDOM_SEED)
    assert len(VIDEO_BIT_RATE) == A_DIM

    # create result directory
    if not os.path.exists(SUMMARY_DIR):
        os.makedirs(SUMMARY_DIR)

    # inter-process communication queues
    net_params_queues = []
    exp_queues = []
    for i in range(NUM_AGENTS):
        net_params_queues.append(mp.Queue(1))
        exp_queues.append(mp.Queue(1))

    # create a coordinator and multiple agent processes
    # (note: threading is not desirable due to python GIL)
    coordinator = mp.Process(target=central_agent,
                             args=(net_params_queues, exp_queues))
    coordinator.start()

    all_cooked_time, all_cooked_bw, _ = load_trace.load_trace(TRAIN_TRACES)
    agents = []
    
    # Pass epoch # to agent when restoring so that can set weight
    if NN_MODEL is not None:
        initial_epoch = extract_epoch_from_filename(NN_MODEL)
    else:
        initial_epoch = 0
        
    for i in range(NUM_AGENTS):
        agents.append(mp.Process(target=agent,
                                 args=(i, all_cooked_time, all_cooked_bw,
                                       net_params_queues[i],
                                       exp_queues[i], initial_epoch)))
    for i in range(NUM_AGENTS):
        agents[i].start()

    # wait unit training is done
    coordinator.join()


if __name__ == '__main__':
    main()