import tensorflow as tf
import numpy as np
import os

MODEL_DIR = './model'
os.makedirs(MODEL_DIR, exist_ok=True)

S_INFO = 6  # bit_rate, buffer_size, next_chunk_size, bandwidth_measurement(throughput and time), chunk_til_video_end
S_LEN = 8  # take how many frames in the past
A_DIM = 6
ACTOR_LR_RATE = 0.0001
CRITIC_LR_RATE = 0.001

# Define the paths to the checkpoint file and the output .npy files
checkpoint_path = './model/pretrain_linear_reward.ckpt'
actor_weights_path = os.path.join(MODEL_DIR, 'pretrained_actor_weights.npy')
critic_weights_path = os.path.join(MODEL_DIR, 'pretrained_critic_weights.npy')

# import tf_a3c as a3c
import a3c

# Loads weights, separate function so can load in tf_agent
def load_weights_from_checkpoint(checkpoint_path, actor_frozen=0, critic_frozen=0):
    print(f"Loading checkpoint: {checkpoint_path}")
    if os.path.exists(checkpoint_path):
        print(f"Checkpoint file not found: {checkpoint_path}")
        return None, None
    try:
        with tf.compat.v1.Session() as sess:
            # Initialize the actor and critic networks
            actor = a3c.ActorNetwork(sess, state_dim=[S_INFO, S_LEN], action_dim=A_DIM, learning_rate=ACTOR_LR_RATE)
            critic = a3c.CriticNetwork(sess, state_dim=[S_INFO, S_LEN], learning_rate=CRITIC_LR_RATE)
            # actor = a3c.ActorNetwork(sess,
            #                     state_dim=[S_INFO, S_LEN], action_dim=A_DIM,
            #                     learning_rate=ACTOR_LR_RATE,
            #                     num_frozen_layers=actor_frozen)
            # critic = a3c.CriticNetwork(sess,
            #                     state_dim=[S_INFO, S_LEN],
            #                     learning_rate=CRITIC_LR_RATE,
            #                     num_frozen_layers=critic_frozen)
            # Create a saver object to restore the model from the checkpoint
            saver = tf.compat.v1.train.Saver()

            # Restore the model from the checkpoint
            saver.restore(sess, checkpoint_path)

            # Extract the network parameters
            actor_params = actor.get_network_params()
            critic_params = critic.get_network_params()

            print("Checkpoint loaded successfully")
            # Return weights
            return actor_params, critic_params
    except tf.errors.NotFoundError as e:
        print(f"Error loading checkpoint: {e}")
        return None, None
    except Exception as e:
        print(f"Unexpected error loading checkpoint: {e}")
        return None, None
    finally:
        print("Closing restore TensorFlow session...")
        sess.close()
        print("Exiting load_weights_from_checkpoint function")

# Saves to file     
def save_weights_from_checkpoint(checkpoint_path, actor_weights_path, critic_weights_path):
    actor_params, critic_params = load_weights_from_checkpoint(checkpoint_path)

    # Save the parameters to .npy files
    np.save(actor_weights_path, actor_params)
    np.save(critic_weights_path, critic_params)

def main():
    # Call the function to save the weights
    save_weights_from_checkpoint(checkpoint_path, actor_weights_path, critic_weights_path)

if __name__ == '__main__':
    main()