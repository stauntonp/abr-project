#### Preparing Directories

Make sure actual video files are stored in `video_server/video[1-6]`, then run

```
python get_video_sizes
```

Put training data in `./sim/cooked_traces` and testing data in `./sim/cooked_test_traces` (need to create dirs). The trace format for simulation is `[time_stamp (sec), throughput (Mbit/sec)]`. Traces can be found in `../../datasets/{dataset}/cooked_traces` and `cooked_test_traces`

#### Reinforcement Learning (multi_agent.py)

To train a RL/Pensieve model, run:

```
python multi_agent.py
```

- This implementation is mostly the same, but in python 3 and the Actor network `entropy_weight` parameter is automatically updated from the `multi_agent`.
  - Automating the weight update does cause a performance hit but allows the entire training sequence to finish uninterrupted.
- A `run_parameters.log` file is created which lists the hyperparameters used in training.
  - This is primarily useful for transfer learning but is helpful for identifying models later.

#### Transfer Learning (tf_agent.py)

- The transfer learning model uses a modified `multi_agent` called `tf_agent.py` and a custom Actor-Critic Network script called `tf_a3c.py`.
- As with RL, the entropy weight parameter is updated automatically and a `run_parameters.log` file is created.
- `ACTOR_LAYERS_FROZEN` and `CRITIC_LAYERS_FROZEN` allow layers of the neural network to be frozen and unable to be trained.
  - In practice, this resulted in a 10-15% improvement in training speed over RL/unfrozen network.
  - Model performance was significantly inferior relative to all layers being trainable.
  - Performing initial training with frozen layers before later unfreezing them may potentially yield superior results but is not possible within the limitations of Tensforflow 1.x which creates static graphs at creation which cannot be modified later.
- `PRETRAINED_ACTOR_WEIGHT` and `PRETRAINED_CRITIC_WEIGHT` point to pretrained weights which are used for transfer learning.

  - These can be generated from by running

  ```
  python tf_get_weights.py
  ```

  - This script will extract the weights from a saved RL model and output them as numpy files.

- The `STARTING_ENTROPY_WEIGHT`, `FINAL_ENTROPY_WEIGHT` and associated variables control the entropy weight decay schedule for the agent.
  - In practice, no decay performs very poorly while starting too high results in much of the preloaded knowledge to be lost

#### Training Curve / Results using tensorboard

TD Loss: <img src="./TD_loss.svg">
Total Reward: <img src="./Eps_total_reward.svg">
Avg Entropy: <img src="./Avg_entropy.svg">

The progress/results of the model training can be viewed in Tensorboard with the following command:

```
python -m tensorflow.tensorboard --logdir=./results/
(py3.7) peter@peter-1-0:~/abr-project/custom_pensieve/sim$ tensorboard --logdir=./results
```

- The plot can be viewed at `localhost:6006` from a browser.

Trained model will be saved in `sim/results/summary`. A sample pretrained model with linear QoE as the reward signal was provided by the Pensieve authors. It can be loaded by setting `NN_MODEL = './model/pretrain_linear_reward.ckpt'`. It is also used as the starting model weights for transfer learning.
