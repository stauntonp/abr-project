import os
import shutil
import random

def split_directory(input_dir, train_dir, test_dir, split_ratio=0.5):
    # Create output directories if they don't exist
    os.makedirs(train_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)

    # Get list of files in the input directory
    files = os.listdir(input_dir)
    files = [file for file in files if os.path.isfile(os.path.join(input_dir, file))]

    # Shuffle files to ensure random split
    random.shuffle(files)

    # Calculate split index
    split_index = int(len(files) * split_ratio)

    # Split files into training and testing sets
    train_files = files[:split_index]
    test_files = files[split_index:]

    # Move files to the respective directories
    for file in train_files:
        shutil.move(os.path.join(input_dir, file), os.path.join(train_dir, file))
    
    for file in test_files:
        shutil.move(os.path.join(input_dir, file), os.path.join(test_dir, file))
    
    print(f"Moved {len(train_files)} files to {train_dir}")
    print(f"Moved {len(test_files)} files to {test_dir}")

# Example usage
input_dir = './cooked'
train_dir = './cooked_traces'
test_dir = './cooked_test_traces'
split_directory(input_dir, train_dir, test_dir)
