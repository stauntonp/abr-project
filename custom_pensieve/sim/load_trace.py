import os


COOKED_TRACE_FOLDER = './cooked_traces/'


def load_trace(cooked_trace_folder=COOKED_TRACE_FOLDER):
    cooked_files = os.listdir(cooked_trace_folder)
    all_cooked_time = []
    all_cooked_bw = []
    all_file_names = []
    for cooked_file in cooked_files:
        # file_path = cooked_trace_folder + cooked_file
        file_path = os.path.join(cooked_trace_folder, cooked_file)
        cooked_time = []
        cooked_bw = []
        # print file_path
        # with open(file_path, 'r') as f:
        #     for line in f:
        #         parse = line.split()
        #         cooked_time.append(float(parse[0]))
        #         cooked_bw.append(float(parse[1]))
        with open(file_path, 'r') as f:
            for line in f:
                parse = line.split()
                # Check if line is properly formatted
                if len(parse) != 2:
                    print(f"Warning: Skipping malformed line in {file_path}: {line.strip()}")
                    continue
                try:
                    cooked_time.append(float(parse[0]))
                    cooked_bw.append(float(parse[1]))
                except ValueError as e:
                    print(f"Warning: Skipping line with invalid values in {file_path}: {line.strip()}. Error: {e}")
                    continue
        all_cooked_time.append(cooked_time)
        all_cooked_bw.append(cooked_bw)
        all_file_names.append(cooked_file)

    return all_cooked_time, all_cooked_bw, all_file_names
