import os
import sys
import signal
import subprocess
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException, WebDriverException
from pyvirtualdisplay import Display
from time import sleep

# TO RUN: download https://pypi.python.org/packages/source/s/selenium/selenium-2.39.0.tar.gz
# run sudo apt-get install python-setuptools
# run sudo apt-get install xvfb
# after untar, run sudo python setup.py install
# follow directions here: https://pypi.python.org/pypi/PyVirtualDisplay to install pyvirtualdisplay

# For chrome, need chrome driver: https://code.google.com/p/selenium/wiki/ChromeDriver
# chromedriver variable should be path to the chromedriver
# the default location for firefox is /usr/bin/firefox and chrome binary is /usr/bin/google-chrome
# if they are at those locations, don't need to specify


def timeout_handler(signum, frame):
    raise Exception("Timeout")

abr_algo = sys.argv[1]
run_time = int(sys.argv[2])
exp_id = sys.argv[3]

print("run_video.py executing.")

# url = f'http://localhost:8333/myindex_{abr_algo}.html'
url = f'http://localhost/myindex_{abr_algo}.html'
print(f"Generated URL: {url}")

# timeout signal
signal.signal(signal.SIGALRM, timeout_handler)
signal.alarm(run_time + 30)
    
try:
    # Set up directories
    default_chrome_user_dir = '../abr_browser_dir/chrome_data_dir'
    chrome_user_dir = '/tmp/chrome_user_dir_real_exp_' + abr_algo
    os.system(f'rm -rf {chrome_user_dir}')
    os.system(f'cp -r {default_chrome_user_dir} {chrome_user_dir}')
    
    # Start ABR algorithm server
    if abr_algo == 'RL':
        command = f'exec python ../rl_server/rl_server_no_training.py {exp_id}'
    elif abr_algo == 'fastMPC':
        command = f'exec python ../rl_server/mpc_server.py {exp_id}'
    elif abr_algo == 'robustMPC':
        command = f'exec python ../rl_server/robust_mpc_server.py {exp_id}'
    else:
        command =f'exec python ../rl_server/simple_server.py {abr_algo} {exp_id}'

    print(f'Running command: {command}')
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    sleep(2)
        
    # Initialize virtual display
    display = Display(visible=0, size=(800, 600))
    display.start()
    
    # Initialize Chrome WebDriver
    options = Options()
    chrome_driver = '../abr_browser_dir/chromedriver'
    
    # Check if ChromeDriver exists and is executable
    if not os.path.exists(chrome_driver):
        raise FileNotFoundError(f"ChromeDriver not found at {chrome_driver}")
    
    options.add_argument('--user-data-dir=' + chrome_user_dir)
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--headless')
    
    service = Service(executable_path=chrome_driver)
    driver = webdriver.Chrome(service=service, options=options)
    print("ChromeDriver initialized.")
    
	# run chrome
    try:
        driver.set_page_load_timeout(10)  # Increased timeout
        driver.get(url)
        print(f"URL {url} loaded in ChromeDriver.")
    except TimeoutException as e:
        print(f"TimeoutException: {e}")
    except WebDriverException as e:
        print(f"WebDriverException: {e}")

    # Run for the specified duration
    sleep(run_time)
    
    # Clean up
    driver.quit()
    display.stop()
    
    # Stop the ABR algorithm server
    proc.send_signal(signal.SIGINT)
    
    print('done')
    
except Exception as e:
    try: 
        display.stop()
    except:
        pass
    try:
        driver.quit()
    except:
        pass
    try:
        proc.send_signal(signal.SIGINT)
    except:
        pass
    
    print(f"Error: {e}")