import numpy as np
import subprocess

RUN_SCRIPT = 'run_video.py'
RANDOM_SEED = 42
RUN_TIME = 30  # sec
ABR_ALGO = ['fastMPC', 'robustMPC', 'BOLA', 'RL']
REPEAT_TIME = 10
MAX_RETRY = 5

def main():
    np.random.seed(RANDOM_SEED)

    with open('./chrome_retry_log', 'w') as log:
        log.write('chrome retry log\n')
        log.flush()

        for rt in range(REPEAT_TIME):
            np.random.shuffle(ABR_ALGO)
            for abr_algo in ABR_ALGO:
                retry_count = 0
                while retry_count < MAX_RETRY:
                    script = f'python {RUN_SCRIPT} {abr_algo} {RUN_TIME} {rt}'
                    
                    proc = subprocess.Popen(script, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                    out, err = proc.communicate()

                    out_str = out.decode('utf-8')
                    err_str = err.decode('utf-8')

                    if 'done' in out_str:
                        break
                    else:
                        log.write(f'Attempt {retry_count + 1} for {abr_algo}_{rt}\n')
                        log.write(f'Stdout: {out_str}\n')
                        log.write(f'Stderr: {err_str}\n')
                        log.flush()
                        retry_count += 1

                if retry_count == MAX_RETRY:
                    log.write(f'Max retries reached for {abr_algo}_{rt}\n')
                    log.flush()

if __name__ == '__main__':
    main()
