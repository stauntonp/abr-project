This set of code runs experiments over Mahimahi. It loads the traces from `cooked_traces/` and invokes ABR servers from `rl_servers/`. Selenium on top of a PyVirtualDisplay is used and the actual graphics is disabled.

Trained RL model needs to be saved in `rl_server/results/`. We provided a sample pretrained model with linear QoE as the reward signal. It can be loaded by setting `NN_MODEL = '../rl_server/results/pretrain_linear_reward.ckpt'` in `rl_server/rl_server_no_training.py`.

Traces need to be placed in `cooked_traces/` (in custom_pensieve home directory), in Mahimahi format. The format details can be found if `datasets/*/mahimahi/` or `datasets/\*/mahimahi_sliced/`

The ABR algorithms are implemented in `rl_server/`. Transfer learning (`tf_server_no_training`), RL/Pensieve (`rl_server_no_training`), Oboe (`oboe_mpc_server`), fast MPC (`mpc_server`) and robust MPC (`robust_mpc_server`) are implemented directly, while other schemes (BB, RB, FESTIVE, BOLA and FIXED) are natively supported by Dash.js and are called from `simple_server`.

- The natively supported algorithms are much slower to run_experiments than the ones with specific implementations

The a3c, tf_a3c and oboe files are symbolic links to the files in the `sim/` and `test/` dirs. If any changes are made, the links may need to be recreated or copied.

To conduct the experiment, run

```
python run_all_traces.py
```

- `run_all_traces.py` creates a subprocess for each selected ABR algorithm
- Note: in `run_traces.py` it's necessary to update the virtual environment (conda py3.7) to the appropriate value as the scripts rely on installed packages
- Each subprocess `run_traces.py` iterates through the `../cooked_traces/` directory and does the following:
  - creates mahimahi shell to emulate the network conditions in the provided trace
  - runs `run_video.py` inside of the mahimahi shell
  - `run_video.py` starts the relevant server from `rl_server/` as a subprocess and then attempts to load a video over http
  - the server subprocess creates a log file in the `results/` dir recording the reward and bitrate data used to server the video
- `check_results.py` can be used to check the contents of the `./results` dir, listing the number of files, any files with less than LINE_COUNT lines as well as the most recent modification time
  - this was used while testing to quickly view progress without having to read individual files

To view the results, modify `SCHEMES` in `plot_results.py` (it checks the file name of the log and matches to the corresponding ABR algorithm), then run

```
python plot_results.py
```
