import os
import sys
import signal
import logging
import subprocess
import psutil
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
# from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, WebDriverException
from pyvirtualdisplay import Display
from time import sleep
import time

# TO RUN: download https://pypi.python.org/packages/source/s/selenium/selenium-2.39.0.tar.gz
# run sudo apt-get install python-setuptools
# run sudo apt-get install xvfb
# after untar, run sudo python setup.py install
# follow directions here: https://pypi.python.org/pypi/PyVirtualDisplay to install pyvirtualdisplay

# For chrome, need chrome driver: https://code.google.com/p/selenium/wiki/ChromeDriver
# chromedriver variable should be path to the chromedriver
# the default location for firefox is /usr/bin/firefox and chrome binary is /usr/bin/google-chrome
# if they are at those locations, don't need to specify

# IP = "127.0.0.1" # use loopback ip when running server locally
IP = "192.168.0.225" # need external ip when running inside of mahimahi shell
ip = sys.argv[1]

abr_algo = sys.argv[2]
run_time = int(sys.argv[3])
process_id = sys.argv[4]
trace_file = sys.argv[5]
sleep_time = sys.argv[6]
RESULTS_DIR = './results/'

def timeout_handler(signum, frame):
    raise Exception("Timeout")

# Clean up at end of execution or during unhandled exception
def cleanup_and_exit():
    global driver, display, proc
    
    if driver:
        try:
            driver.quit()
        except Exception as e:
            logger.error(f"Error quitting driver: {e}")

    if display:
        try:
            display.stop()
        except Exception as e:
            logger.error(f"Error stopping display: {e}")

    if proc:
        try:
            logger.info(f"Terminating process group for PID: {proc.pid}")
            # Send SIGTERM to the entire process group
            os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
            
            # Wait for a short time to allow for graceful termination
            try:
                proc.wait(timeout=5)
            except subprocess.TimeoutExpired:
                logger.warning("Timeout expired. Forcefully killing the process group.")
                os.killpg(os.getpgid(proc.pid), signal.SIGKILL)
            
            # Double-check if the process is still running
            if psutil.pid_exists(proc.pid):
                logger.warning(f"Process {proc.pid} still exists. Attempting to terminate it directly.")
                os.kill(proc.pid, signal.SIGKILL)
            
            # Check for any remaining child processes
            parent = psutil.Process(proc.pid)
            for child in parent.children(recursive=True):
                logger.warning(f"Child process {child.pid} still running. Terminating it.")
                child.terminate()
                try:
                    child.wait(timeout=3)
                except psutil.TimeoutExpired:
                    child.kill()
                    
        except psutil.NoSuchProcess:
            logger.info(f"Process {proc.pid} no longer exists.")
        except Exception as e:
            logger.error(f"Error during process termination: {e}")

    sys.exit(0)

def signal_handler(signum, frame):
    logger.info(f"Received signal {signum}. Cleaning up and exiting.")
    cleanup_and_exit()

signal.signal(signal.SIGTERM, signal_handler)
signal.signal(signal.SIGINT, signal_handler)

# Script runs inside of a subprocess and not piping output upstream so need logging for debugging
def setup_logging():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    
    # Console handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    console_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    console_handler.setFormatter(console_formatter)
    
    # File handler
    file_handler = logging.FileHandler('run_video_log.log')
    file_handler.setLevel(logging.INFO)
    file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(file_formatter)
    
    # Add both handlers to the logger
    # logger.addHandler(console_handler)
    logger.addHandler(file_handler) # Was used to debug - unreadable when multiple processes writing to it
    
    return logger

logger = setup_logging()

logger.info("run_video.py executing.")
logger.info(f"Sleeping for {sleep_time} seconds.")
sleep(int(sleep_time))

# Script was interrupting before enough lines written to the log
# Adding timeout logic to kill dead processes but wait for processing to finish also
def wait_for_log_file(log_file_path, timeout=run_time+180, proc=None):
    MIN_LOG_LINES = 12 # will accept this result at timeout
    MAX_LOG_LINES = 14
    CHECK_INTERVAL = 30  # Sleep for 30 seconds between checks
    
    start_time = time.time()
    last_modified_time = 0
    last_line_count = 0

    # Loop until MAX_LOG_LINES reached or timeout without changes
    while True:
        current_time = time.time()
        
        # Was experiencing crashes (different issue): No point waiting if the server is dead
        if proc and not psutil.pid_exists(proc.pid):
            logger.error("[{abr_algo}|{trace_file}] Server process is no longer running.")
            return False
        
        if os.path.exists(log_file_path):
            current_modified_time = os.path.getmtime(log_file_path) # Time file last modified
            
            with open(log_file_path, 'r') as f:
                line_count = sum(1 for _ in f)
            
            # logger.info(f"Log file has {line_count} lines.")
            logger.info(f"Log file {abr_algo}|{trace_file} has {line_count} lines.")
            
            if line_count >= MAX_LOG_LINES:
                logger.info(f"Log file reached {MAX_LOG_LINES} lines. Interrupting.")
                return True
            
            # Continue while changes have been made
            if current_modified_time > last_modified_time or line_count > last_line_count:
                # File has been modified, reset the timer
                start_time = current_time
                last_modified_time = current_modified_time
                last_line_count = line_count
        else:
            logger.info(f"Log file does not exist yet. Checking: {log_file_path}")
            # List files in the directory
            files = os.listdir(os.path.dirname(log_file_path))
            logger.info(f"Files in directory: {files}")
        
        # Check for timeout since the last modification
        if current_time - start_time > timeout:
            # No need to restart at 12+ lines
            if line_count >= MIN_LOG_LINES:
                logger.warning(f"Timeout after {timeout} seconds, {line_count} lines reached and continuing to next trace.")
                return True
            else:
                logger.warning(f"Timeout after {timeout} seconds since last modification.")
                return False
        
        logger.info(f"[{abr_algo}|{trace_file}]Sleeping for {CHECK_INTERVAL} seconds before next check.")
        sleep(CHECK_INTERVAL) # otherwise writes to log way too much


def main():
    # ip = IP    
    url = f'http://{ip}/myindex_{abr_algo}.html'

    logger.info(f"Generated URL: {url}")

    # timeout signal
    signal.signal(signal.SIGALRM, timeout_handler)
    # signal.alarm(run_time + 30) # default 30 seems to have failed frequently

    try:
        # copy over the chrome user dir
        default_chrome_user_dir = '../abr_browser_dir/chrome_data_dir'
        chrome_user_dir = f'/tmp/chrome_user_dir_id_{abr_algo}'
        os.system('rm -r ' + chrome_user_dir)
        os.system('cp -r ' + default_chrome_user_dir + ' ' + chrome_user_dir)
        
        # start abr algorithm server
        if abr_algo == 'RL':
            command = f'python ../rl_server/rl_server_no_training.py {trace_file}.log'
        elif abr_algo == 'fastMPC':
            command = f'/usr/bin/python ../rl_server/mpc_server.py {trace_file}.log'
        elif abr_algo == 'robustMPC':
            # command = f'/usr/bin/python ../rl_server/robust_mpc_server.py {trace_file}_{process_id}.log'
            command = f'python ../rl_server/robust_mpc_server.py {trace_file}.log'
        elif abr_algo in ['TF_25k', 'TF_50k', 'TF_full']:
            # tf_type = abr_algo.split('_')[1]  # This will be '25', '50', or 'full'
            url = f'http://{ip}/myindex_RL.html' # URL works, TF not server by the apache server
            command = f'python ../rl_server/tf_server_no_training.py {abr_algo} {trace_file}.log'
        elif abr_algo == 'oboeMPC':
            url = f'http://{ip}/myindex_robustMPC.html'
            command = f'python ../rl_server/oboe_mpc_server.py {trace_file}.log'
        else:
            command = f'python ../rl_server/simple_server.py {abr_algo} {trace_file}.log'
        
        logger.info(f'Running command: {command}')
        # proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn=os.setpgrp) # setpgrp keeps subprocess inside of the same session group
        logger.info("Waiting for server to initialize...")
        sleep(5)  # Increased initial sleep time
        
        display = Display(visible=0, size=(800, 600))
        display.start()

        options = Options()
        chrome_driver = '../abr_browser_dir/chromedriver'

        if not os.path.exists(chrome_driver):
            raise FileNotFoundError(f"ChromeDriver not found at {chrome_driver}")

        options.add_argument('--user-data-dir=' + chrome_user_dir)
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('--no-sandbox')

        service = Service(executable_path=chrome_driver)
        driver = webdriver.Chrome(service=service, options=options)
        logger.info("ChromeDriver initialized.")
        
        try:
            driver.set_page_load_timeout(15)
            logger.info(f"Attempting to load URL: {url}")
            driver.get(url) # GET request to server to load web page (video))
            logger.info(f"URL {url} loaded in ChromeDriver.")
        except TimeoutException as e:
            logger.error(f"TimeoutException: {e}")
            sys.exit(1) # no point continuing loop
        except WebDriverException as e:
            logger.error(f"WebDriverException: {e}")
            sys.exit(1)
            
        # Wait for log file to reach MAX_LOG_LINES or timeout
        log_file_name = f'log_{abr_algo}_{os.path.basename(trace_file)}.log'
        log_file_path = os.path.join(RESULTS_DIR, log_file_name)
        logger.info(f"Waiting for log file: {log_file_path}")
        log_file_completed = wait_for_log_file(log_file_path, proc=proc)

        if not log_file_completed:
            logger.warning("Log file did not reach MAX_LOG_LINES before timeout.")

        # Check final log file status
        if os.path.exists(log_file_path):
            with open(log_file_path, 'r') as f:
                final_line_count = sum(1 for _ in f)
            logger.info(f"Final log file line count: {final_line_count}")
        else:
            logger.warning("Log file does not exist at the end of execution.")
  
        
        logger.info("Waiting additional time to ensure log writing completes...")
        sleep(10)
        
        # Check final log file status
        if os.path.exists(log_file_path):
            with open(log_file_path, 'r') as f:
                final_line_count = sum(1 for _ in f)
            logger.info(f"Final log file line count after additional wait: {final_line_count}")
        else:
            logger.warning(f"Log file {log_file_path} does not exist.")

        # driver_logs = driver.get_log('browser')
        # for entry in driver_logs:
        #     logger.info(f"Browser log entry: {entry}")
        
    except Exception as e:
        logger.error(f"An error occurred: {e}")
        try:
            display.stop()
        except Exception as e:
            logger.error(f"Error stopping display: {e}")
        try:
            driver.quit()
        except Exception as e:
            logger.error(f"Error quitting driver: {e}")
        try:
            proc.send_signal(signal.SIGINT)
        except Exception as e:
            logger.error(f"Error sending signal to process: {e}")
            
    finally:
        # if timeout, return fail to run_traces
        if not log_file_completed:
            print("fail")
        else:
            print("done")
                
if __name__ == '__main__':
    try:
        main()
    finally:
        cleanup_and_exit() # Ensures all memory freed up after main()