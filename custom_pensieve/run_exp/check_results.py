import os
import psutil
import time

LINE_COUNT = 14

# Had issues with failing to terminate subprocesses and running out of memory
# Not very precise as counts all python processes but should quickly identify if dozens running
# Number shouldn't be increasing in subsequent runs
def count_python_processes():
    python_count = 0
    for process in psutil.process_iter(['name']):
        try:
            if process.info['name'] == 'python' or process.info['name'] == 'python3':
                python_count += 1
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return python_count

# Checks that all files are at least 14 lines long as that is the length for plotting results
def check_files(directory='./results/'):
    # Variable to store the most recent modification time
    most_recent_mod_time = 0
    most_recent_file = ""
    
    # Print the number of Python processes
    print(f"Current number of Python processes: {count_python_processes()}")
    
    # Ensure the directory path ends with a slash
    if not directory.endswith('/'):
        directory += '/'

    # Check if the directory exists
    if not os.path.exists(directory):
        print(f"The directory {directory} does not exist.")
        return

    traces_dir = os.listdir('../cooked_traces/')
    print(f"Total number of files in traces dir: {len(traces_dir)}")

    # List to store files with fewer than 14 lines
    short_files = []

    all_files = os.listdir(directory)    
    print(f"Total number of files in results dir: {len(all_files)}")
    # Iterate through all files in the directory
    for filename in all_files:
        filepath = os.path.join(directory, filename)
        
        # Check if it's a file (not a subdirectory)
        if os.path.isfile(filepath):
            # Get the modification time of the file
            mod_time = os.path.getmtime(filepath)
            
            # Update most recent modification time if this file is more recent
            if mod_time > most_recent_mod_time:
                most_recent_mod_time = mod_time
                most_recent_file = filename
                
            with open(filepath, 'r') as file:
                # Count the number of lines
                line_count = sum(1 for line in file)
                
                # If the file has fewer than 14 lines, add it to the list
                if line_count < LINE_COUNT:
                    short_files.append((filename, line_count))

    # Print the results
    if short_files:
        print(f"Files with fewer than {LINE_COUNT} lines: {len(short_files)}")
        for filename, line_count in short_files:
            print(f"{filename}: {line_count} lines")
    else:
        print(f"No files with fewer than {LINE_COUNT} lines were found.")
        
    # Print the most recently modified file
    if most_recent_file:
        print(f"\nMost recently modified file:")
        print(f"{most_recent_file}: last modified at {time.ctime(most_recent_mod_time)}")
    else:
        print("\nNo files found in the directory.")

# Run the function
check_files()