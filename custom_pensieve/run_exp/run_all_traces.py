import os
import time
import json
from urllib.request import urlopen
import subprocess

# There's an issue when running many traces (20+) at once or 5+ algorithms at once
# Subprocesses crashing + not handled correctly
# Added timeout to individual servers and improved termination logic in run_videos - seems fine and much faster
TRACE_PATH = '../cooked_traces/'
VIDEO_PROGRESS_LOG = 'run_video_log.log' # Shows subprocess progress

# Clear log from parent process as multiple subprocesses run_video.py write to the same log file
if os.path.exists(VIDEO_PROGRESS_LOG):
    try:
        os.remove(VIDEO_PROGRESS_LOG)
        print(f"Deleted existing {VIDEO_PROGRESS_LOG}")
    except Exception as e:
        print(f"Error deleting {VIDEO_PROGRESS_LOG}: {e}")
        
os.makedirs('./results', exist_ok=True)

# with open('./chrome_retry_log', 'w') as f:
# 	f.write('chrome retry log\n')

# Need ip forwarding for mahimahi to work
os.system('sudo sysctl -w net.ipv4.ip_forward=1')

# ip_data = json.loads(urlopen("http://ip.jsontest.com/").read())
# ip = str(ip_data['ip'])
ip = "192.168.0.225" # private ip used with mahimahi, loopback 127.0.0.1 used when server running outside of shell

# Replacing original ABR_ALGO=X PROCESS_ID=Y command=Z hardcoded for each as awkward to modify/extend
# Original algorithms from Pensieve paper
# Skipping RB/FIXED/BB/fastMPC - very slow due to implementation compared to others
algorithms = [
    # ('BB', 0),
    # ('RB', 1),
    # ('FIXED', 2),
    # ('FESTIVE', 3),
    ('BOLA', 4),
    # ('fastMPC', 5),
    ('robustMPC', 6),
    ('RL', 7)
    ]
# New algorithms
new_algorithms = [
    # ('TF_25k', 8),
    ('TF_50k', 9),
    ('TF_full', 10),
    ('oboeMPC', 11)
]
# Combine all algorithms
all_algorithms = algorithms  + new_algorithms

# Create commands for all algorithms
commands = {}
for abr_algo, process_id in all_algorithms:
    command = f'python run_traces.py {TRACE_PATH} {abr_algo} {process_id} {ip}'
    commands[abr_algo] = command


# Start processes for all algorithms
processes = {}
for abr_algo, _ in all_algorithms:
    processes[abr_algo] = subprocess.Popen(commands[abr_algo], stdout=subprocess.PIPE, shell=True)
    time.sleep(1)

# Wait for all processes to complete
for process in processes.values():
    process.wait()