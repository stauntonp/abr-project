import sys
import os
import subprocess
import numpy as np


RUN_SCRIPT = 'run_video.py'
RANDOM_SEED = 42
RUN_TIME = 60  # sec
MM_DELAY = 50   # millisec
MAX_TRIES = 7 # Was getting stuck in endless loop of retries. Also dont want to quit after 1 failure


def main():
    trace_path = sys.argv[1]
    abr_algo = sys.argv[2]
    process_id = sys.argv[3]
    ip = sys.argv[4]
    # ip = "127.0.0.1"

    sleep_vec = list(range(1, 15))  # random sleep second

    files = os.listdir(trace_path)
    for f in files:
        tries = 1
        while True and tries <= MAX_TRIES:
            print(f"Attempt {tries}/{MAX_TRIES}")
            tries += 1
            np.random.shuffle(sleep_vec)
            sleep_time = sleep_vec[int(process_id)]
            print(f"Sleep time = {sleep_time}")
            file_path = os.path.join(trace_path, f)
            
            
            UPLINK_TRACE = '12mbps' # mahimahi upload trace
            downlink_trace = file_path # mahimahi download trace
            
            # Separating commands for clarity
            # run mm-link to enable mm emulation
            mm_command = f"mm-delay {MM_DELAY} mm-link {UPLINK_TRACE} {downlink_trace}"        
            python_command = f"python {RUN_SCRIPT} {ip} {abr_algo} {RUN_TIME} {process_id} {file_path} {sleep_time}"
            # start_conda = f"conda init && conda activate py3.7"
            # Need to start the virtual environment inside of the mahimahi shell also or no packages accessible
            full_command = f"bash -c 'source ~/anaconda3/etc/profile.d/conda.sh && conda activate py3.7 && {python_command}'"
            final_command = f"{mm_command} -- {full_command}" # start mm shell > activate venv > run script

            print(f"Starting mahimahi shell: {mm_command}")
            print(f"Running script: {python_command}")
            proc = subprocess.Popen(final_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            
            out, err = proc.communicate()
            out_str = out.decode()
            err_str = err.decode()
            
            print("Output of subprocess:\n", out_str)
            print("Errors:\n", err_str)

            if "done" in out_str:
                break
            # if run_video fails, should return fail and retry up to 5x
            # if "fail" in out_str:
            #     continue
            # No need for this retry_log as writing directly to run_video_log as they execute.
            # else:
            #     with open('./chrome_retry_log', 'ab') as log:
            #         log.write((abr_algo + '_' + f + '\n').encode('utf-8'))
            #         log.write(out_str.encode('utf-8') + b'\n')
            #         log.write(err_str.encode('utf-8') + b'\n')
            #         log.flush()

if __name__ == '__main__':
    main()