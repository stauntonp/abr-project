import os
import subprocess
import signal
import psutil
import requests
import logging
from time import sleep

def setup_logging():
    logging.basicConfig(level=logging.INFO)
    return logging.getLogger(__name__)

logger = setup_logging()

def start_server(abr_algo, trace_file, process_id):
    command = ""
    if abr_algo == 'RL':
        command = f'/usr/bin/python ../rl_server/rl_server_no_training.py {trace_file}_{process_id}.log'
    elif abr_algo == 'fastMPC':
        command = f'/usr/bin/python ../rl_server/mpc_server.py {trace_file}_{process_id}.log'
    elif abr_algo == 'robustMPC':
        command = f'/usr/bin/python ../rl_server/robust_mpc_server.py {trace_file}_{process_id}.log'
    else:
        command = f'/usr/bin/python ../rl_server/simple_server.py {abr_algo} {trace_file}_{process_id}.log'
    
    logger.info(f'Running command: {command}')
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
    sleep(10)
    return proc

def check_server(url):
    try:
        response = requests.get(url, timeout=10)
        if response.status_code == 200:
            logger.info(f"Server is up and accessible at {url}")
            return True
        else:
            logger.error(f"Server returned status code: {response.status_code}")
            return False
    except requests.ConnectionError as e:
        logger.error(f"Connection error: {e}")
        return False
    except requests.Timeout as e:
        logger.error(f"Timeout error: {e}")
        return False
    except Exception as e:
        logger.error(f"An error occurred: {e}")
        return False

def stop_server(proc):
    if proc:
        try:
            logger.info(f"Terminating process group for PID: {proc.pid}")
            parent = psutil.Process(proc.pid)
            for child in parent.children(recursive=True):
                child.terminate()
            parent.terminate()
            parent.wait(timeout=10)
        except Exception as e:
            logger.error(f"Error terminating process: {e}")

if __name__ == "__main__":
    # ip = "127.0.0.1"  # You can change this if needed
    # ip = 'localhost'
    ip = '192.168.0.225'
    abr_algo = "robustMPC"  # Example algorithm, change as needed
    process_id = "1"  # Example process ID, change as needed
    trace_file = "../cooked_traces/report.2011-04-21_1135CEST.log"
    # url = f'http://{ip}:8333/myindex_{abr_algo}.html'
    url = f'http://{ip}/myindex_{abr_algo}.html'

    logger.info("Starting server...")
    proc = start_server(abr_algo, trace_file, process_id)

    logger.info("Checking if server is accessible...")
    server_up = check_server(url)

    if server_up:
        logger.info("Server setup and check completed successfully.")
    else:
        logger.error("Server setup or check failed.")

    stop_server(proc)
    logger.info("Server stopped.")
