import os
import shutil
import random

def select_and_copy_files(source_dir, destination_dir, num_files=50, min_size_kb=1, max_size_kb=50):
    # Ensure the destination directory exists
    os.makedirs(destination_dir, exist_ok=True)
    
    # List all files in the source directory
    # all_files = [f for f in os.listdir(source_dir) if os.path.isfile(os.path.join(source_dir, f))]
    # List all files in the source directory that match the size constraints
    all_files = [
        f for f in os.listdir(source_dir) 
        if os.path.isfile(os.path.join(source_dir, f)) 
        # and min_size_kb * 1024 <= os.path.getsize(os.path.join(source_dir, f)) <= max_size_kb * 1024
    ]
    
    # If there are fewer than num_files, adjust num_files
    num_files = min(num_files, len(all_files))
    
    # Randomly select num_files files from the list
    selected_files = random.sample(all_files, num_files)
    
    # Move each selected file to the destination directory
    for file in selected_files:
        shutil.move(os.path.join(source_dir, file), os.path.join(destination_dir, file))
    
    print(f"Moved {len(selected_files)} files from {source_dir} to {destination_dir}.")


source_directory = './mahimahi/'
destination_directory1 = './mahimahi_subset/'
destination_directory2 = './cooked_test_traces/'
select_and_copy_files(source_directory, destination_directory1, num_files=80)

# select_and_copy_files(source_directory, destination_directory2, num_files=100)
