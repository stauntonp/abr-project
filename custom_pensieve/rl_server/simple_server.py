#!/usr/bin/env python3
from http.server import BaseHTTPRequestHandler, HTTPServer
import socketserver
import base64
import urllib
import sys
import os
import logging
import json

from collections import deque
import numpy as np
import time


VIDEO_BIT_RATE = [300, 750, 1200, 1850, 2850, 4300]  # Kbps
BITRATE_REWARD = [1, 2, 3, 12, 15, 20]
BITRATE_REWARD_MAP = {0: 0, 300: 1, 750: 2, 1200: 3, 1850: 12, 2850: 15, 4300: 20}
M_IN_K = 1000.0
DEFAULT_QUALITY = 0  # default video quality without agent
REBUF_PENALTY = 4.3  # 1 sec rebuffering -> this number of Mbps
SMOOTH_PENALTY = 1
TOTAL_VIDEO_CHUNKS = 48
SUMMARY_DIR = './results'
LOG_FILE = 'log'
# in format of time_stamp bit_rate buffer_size rebuffer_time video_chunk_size download_time reward

# Servers not reliably terminating process from parent, want to add a timeout
class TimeoutHTTPServer(socketserver.TCPServer):
    def __init__(self, server_address, RequestHandlerClass, timeout=5, max_idle_time=300):
        self.timeout = timeout
        self.max_idle_time = max_idle_time
        self.last_request_time = time.time()
        self.should_shutdown = False
        socketserver.TCPServer.__init__(self, server_address, RequestHandlerClass)

    def handle_timeout(self):
        current_time = time.time()
        idle_time = current_time - self.last_request_time
        print(f"Idle for {idle_time:.2f} seconds")
        if idle_time > self.max_idle_time:
            print(f"No requests received for {self.max_idle_time} seconds. Initiating shutdown.")
            self.should_shutdown = True

    def process_request(self, request, client_address):
        self.last_request_time = time.time()
        print(f"Request received. Updating last_request_time to {self.last_request_time}")
        return socketserver.TCPServer.process_request(self, request, client_address)

def make_request_handler(input_dict):

    class Request_Handler(BaseHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
            self.input_dict = input_dict
            self.log_file = input_dict['log_file']
            super().__init__(*args, **kwargs)

        def do_POST(self):
            try:
                content_length = int(self.headers['Content-Length'])
                post_data = json.loads(self.rfile.read(content_length))

                print(post_data)
                send_data = ""

                if 'lastquality' in post_data:
                    rebuffer_time = float(post_data['RebufferTime'] - self.input_dict['last_total_rebuf'])
                    reward = (
                        VIDEO_BIT_RATE[post_data['lastquality']] / M_IN_K
                        - REBUF_PENALTY * rebuffer_time / M_IN_K
                        - SMOOTH_PENALTY * np.abs(VIDEO_BIT_RATE[post_data['lastquality']] -
                                                  self.input_dict['last_bit_rate']) / M_IN_K
                    )

                    video_chunk_fetch_time = post_data['lastChunkFinishTime'] - post_data['lastChunkStartTime']
                    video_chunk_size = post_data['lastChunkSize']

                    # log wall_time, bit_rate, buffer_size, rebuffer_time, video_chunk_size, download_time, reward
                    self.log_file.write((str(time.time()) + '\t' +
                                         str(VIDEO_BIT_RATE[post_data['lastquality']]) + '\t' +
                                         str(post_data['buffer']) + '\t' +
                                         str(rebuffer_time / M_IN_K) + '\t' +
                                         str(video_chunk_size) + '\t' +
                                         str(video_chunk_fetch_time) + '\t' +
                                         str(reward) + '\n').encode())
                    self.log_file.flush()

                    self.input_dict['last_total_rebuf'] = post_data['RebufferTime']
                    self.input_dict['last_bit_rate'] = VIDEO_BIT_RATE[post_data['lastquality']]

                    if post_data['lastRequest'] == TOTAL_VIDEO_CHUNKS:
                        send_data = "REFRESH"
                        self.input_dict['last_total_rebuf'] = 0
                        self.input_dict['last_bit_rate'] = DEFAULT_QUALITY
                        self.log_file.write('\n'.encode())  # so that in the log we know where video ends

                self.send_response(200)
                self.send_header('Content-Type', 'text/plain')
                self.send_header('Content-Length', len(send_data))
                self.send_header('Access-Control-Allow-Origin', "*")
                self.end_headers()
                self.wfile.write(send_data.encode())

            except Exception as e:
                logging.error("Error handling POST request: %s", str(e))

        def do_GET(self):
            try:
                print('GOT REQ', file=sys.stderr)
                self.send_response(200)
                # self.send_header('Cache-Control', 'Cache-Control: no-cache, no-store, must-revalidate max-age=0')
                self.send_header('Cache-Control', 'max-age=3000')
                self.send_header('Content-Length', 20)
                self.end_headers()
                self.wfile.write("console.log('here');".encode())
            except Exception as e:
                logging.error("Error handling GET request: %s", str(e))

        def log_message(self, format, *args):
            return

    return Request_Handler


def run(server_class=TimeoutHTTPServer, port=8333, log_file_path=LOG_FILE, max_idle_time=300):

    if not os.path.exists(SUMMARY_DIR):
        os.makedirs(SUMMARY_DIR)
    # Ensure the log file directory exists
    # log_dir = os.path.dirname(log_file_path)
    # if not os.path.exists(log_dir):
    #     os.makedirs(log_dir)

    log_file_path = os.path.join(SUMMARY_DIR, log_file_path)
    with open(log_file_path, 'wb') as log_file:

        last_bit_rate = DEFAULT_QUALITY
        last_total_rebuf = 0
        input_dict = {'log_file': log_file,
                      'last_bit_rate': last_bit_rate,
                      'last_total_rebuf': last_total_rebuf}

        handler_class = make_request_handler(input_dict=input_dict)

        server_address = ('localhost', port)
        httpd = server_class(server_address, handler_class, timeout=5, max_idle_time=max_idle_time)
        print(f'Listening on port {port}. Server will shut down after {max_idle_time} seconds of inactivity.')
        
        try:
            while not httpd.should_shutdown:
                httpd.handle_request()
            print("Server idle timeout reached. Shutting down.")
        except KeyboardInterrupt:
            print("Keyboard interrupt received. Shutting down.")
        finally:
            httpd.server_close()
            print("Server shut down.")


def main():   
    max_idle_time = 600  # 10 minutes as BOLA not finishing before server gone a few times
    
    logging.basicConfig(level=logging.DEBUG)
    if len(sys.argv) == 3:
        abr_algo = sys.argv[1]
        trace_file = sys.argv[2]
        run(log_file_path=LOG_FILE + '_' + abr_algo + '_' + os.path.basename(trace_file),  max_idle_time=max_idle_time)
    else:
        try:
            run(max_idle_time=max_idle_time)
        except KeyboardInterrupt:
            print("Keyboard interrupted. Exiting gracefully.")

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logging.debug("Keyboard interrupted.")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
