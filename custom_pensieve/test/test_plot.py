import logging
import numpy as np

# Ensure logging is configured
logging.basicConfig(
    filename='test_log_file.log',
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s',
    filemode='w'
)

# Example initialization (adjust according to your actual initialization logic)
SCHEMES = ['scheme1', 'scheme2', 'scheme3']
VIDEO_LEN = 3
time_all = {
    'scheme1': {'trace1': [0, 1, 2], 'trace2': [0, 1, 2]},
    'scheme2': {'trace1': [0, 1, 2], 'trace2': [0, 1, 2]},
    'scheme3': {'trace1': [0, 1, 2], 'trace2': [0, 1, 2]}
}
logging.info(f"time_all: {time_all}")
raw_reward_all = {
    'scheme1': {'trace1': np.random.rand(15), 'trace2': np.random.rand(15)},
    'scheme2': {'trace1': np.random.rand(15), 'trace2': np.random.rand(15)},
    'scheme3': {'trace1': np.random.rand(15), 'trace2': np.random.rand(15)}
}

reward_all = {scheme: [] for scheme in SCHEMES}  # Initialize reward_all as a dictionary of lists
log_file_all = []

# Process logic
for l in time_all[SCHEMES[0]]:
    schemes_check = True
    logging.debug(f"Processing trace {l}")
    for scheme in SCHEMES:
        if l not in time_all[scheme]:
            logging.debug(f"Trace {l} not found for scheme {scheme}")
            schemes_check = False
            break
        if len(time_all[scheme][l]) < VIDEO_LEN:
            logging.debug(f"Skipping trace {l} for scheme {scheme}: length of data {len(time_all[scheme][l])} is less than VIDEO_LEN {VIDEO_LEN}")
            schemes_check = False
            break
    if schemes_check:
        log_file_all.append(l)
        logging.debug(f"Trace {l} is valid for all schemes, appending to log_file_all")
        for scheme in SCHEMES:
            reward_sum = np.sum(raw_reward_all[scheme][l][1:VIDEO_LEN])
            reward_all[scheme].append(reward_sum)
            logging.debug(f"Appending reward {reward_sum} for scheme {scheme} and trace {l}")

# Verify the results
logging.debug(f"Final reward_all: {reward_all}")
logging.debug(f"Final log_file_all: {log_file_all}")
