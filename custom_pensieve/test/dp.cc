// Compile: g++ -std=c++11 -O3 dp.cc -o dp.exe 
// C++ standard library headers
#include <cassert>
#include <cmath>
#include <cstdint> // fixed width integer types
#include <fstream>
#include <iostream>
#include <limits> // numericlimits
#include <mutex> // Used in logging for thread safety
#include <numeric> // numeric operations eg. std::acumulate
#include <sstream>
#include <stdexcept> // exceptions
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
// C system headers
#include <cstring> // strerror() for error message handling
#include <dirent.h> // directory operations
#include <errno.h> // err number definitions
#include <sys/stat.h> // file status/directory creation, mkdir() and mode_t
#include <sys/types.h> // system types definitions
#include <sys/wait.h> // process control
#include <csignal> // Improves abilitiy to handle signals/identify problematic part of code
#include <cstdlib>

namespace abr {

	float MILLISECONDS_IN_SECOND = 1000;
	float B_IN_MB = 1000000;
	float M_IN_K = 1000;
	float BITS_IN_BYTE = 8;
	int RANDOM_SEED = 42;
	float VIDEO_CHUNCK_LEN = 4000;  // (ms), every time add this amount to buffer
	unsigned int BITRATE_LEVELS = 6;
	unsigned int TOTAL_VIDEO_CHUNCK = 48;
	float PACKET_PAYLOAD_PORTION = 0.95;
	unsigned int LINK_RTT = 80;  // millisec
	unsigned int PACKET_SIZE = 1500;  // bytes
	// Breaks with larger files - ideally need a larger buffer size which would involve large re-write
	// Additionally, dynamically adjusting the DT did not work out
	float DT = 0.10;  // time granularity
	float BUFFER_THRESH = 60;  // sec, max buffer limit
	static const int VIDEO_BIT_RATE[] = {300, 750, 1200, 1850, 2850, 4300};  // Kbps
	bool use_hd_reward = false; // use linear reward as default
	static const int BITRATE_REWARD[] = {1, 2, 3, 12, 15, 20}; // HD reward instead
	unsigned int DEFAULT_QUALITY = 1;
	unsigned int MAX_QUALITY = 5;
	const float REBUF_PENALTY = 4.3;  // 1 sec rebuffering -> 3 Mbps
	int SMOOTH_PENALTY = 1;
	float INVALID_DOWNLOAD_TIME = -1;

	const std::string COOKED_TRACE_FOLDER = "./cooked_test_traces/";
	const std::string OUTPUT_DIR = "./dp/";
	const std::string OUTPUT_FILE_PATH = "./dp/log_sim_dp";
	const std::string VIDEO_SIZE_FILE = "./video_size_";

	// Bad memory allocation exception
	class insufficient_memory_error : public std::bad_alloc {
	public:
		const char* what() const noexcept override {
			return "Insufficient memory to continue processing";
		}
	};

	// Malloc which will throw inufficient memory exception
	template <class T>
	struct throwing_allocator {
		using value_type = T;
		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;
		using propagate_on_container_move_assignment = std::true_type;

		throwing_allocator() noexcept = default;
		template <class U> throwing_allocator(const throwing_allocator<U>&) noexcept {}

		T* allocate(std::size_t n) {
			if (n > std::numeric_limits<std::size_t>::max() / sizeof(T))
				throw insufficient_memory_error();
			
			if (auto p = static_cast<T*>(std::malloc(n * sizeof(T)))) {
				return p;
			}

			throw insufficient_memory_error();
		}

		void deallocate(T* p, std::size_t) noexcept { std::free(p); }
		
		// Rebind struct allows allocator to be used with different types
		template <class U>
		struct rebind {
			using other = throwing_allocator<U>;
		};
	};
	
	// Comparison operators required for some container operations
	template <class T, class U>
	bool operator==(const throwing_allocator<T>&, const throwing_allocator<U>&) { return true; }

	template <class T, class U>
	bool operator!=(const throwing_allocator<T>&, const throwing_allocator<U>&) { return false; }

	// Signal handler for memory-related errors
	// Should catch sudden memory exhaustion to terminate process
	void memory_error_handler(int signum) {
		std::cerr << "Memory-related error occurred. Signal: " << signum << std::endl;
		std::exit(1);
	}

		// Adds more clarity of stage that caused timeout
	enum class ProcessingStage {
		LOADING,
		QUANTIZATION,
		CAPPING,
		PRECOMPUTATION,
		DYNAMIC_PROGRAMMING,
		FINAL_PROCESSING
	};

	ProcessingStage current_stage = ProcessingStage::LOADING; // for timeout signalling messaging

	// Set/clear alarm for timeouts
	void set_alarm(unsigned int seconds, ProcessingStage stage) {
		current_stage = stage;
		alarm(seconds);
	}

	void clear_alarm() {
		alarm(0);
	}

	// Handles timeouts
	void timeout_handler(int signum) {
		std::cerr << "Process timed out during ";
		switch(current_stage) {
			case ProcessingStage::LOADING:
				std::cerr << "loading and preprocessing";
				break;
			case ProcessingStage::QUANTIZATION:
				std::cerr << "time and bandwidth quantization";
				break;
			case ProcessingStage::CAPPING:
				std::cerr << "max time and buffer capping";
				break;
			case ProcessingStage::PRECOMPUTATION:
				std::cerr << "download time pre-computation";
				break;
			case ProcessingStage::DYNAMIC_PROGRAMMING:
				std::cerr << "dynamic programming";
				break;
			case ProcessingStage::FINAL_PROCESSING:
				std::cerr << "final processing and result writing";
				break;
		}
		std::cerr << std::endl;
		std::exit(1);
	}

	// Replaces standard vector with bad malloc version
	template <typename T>
	using Vector = std::vector<T, throwing_allocator<T>>;

	// Replaces standard map with bad malloc version
	template <typename K, typename V>
	using UnorderedMap = std::unordered_map<K, V, std::hash<K>, std::equal_to<K>, throwing_allocator<std::pair<const K, V>>>;

	struct ALL_COOKED_TIME_BW {
		Vector<Vector<float> > all_cooked_time;
		Vector<Vector<float> > all_cooked_bw;
		Vector<std::string> all_file_names;
	};

	struct DP_PT {
		unsigned int chunk_idx;
		unsigned int time_idx;
		unsigned int buffer_idx;
		unsigned int bit_rate;
	};

	uint64_t combine(uint64_t a, uint64_t b, uint64_t c, uint64_t d) {
		// assume a, b, c, d < 2^16 = 65536
		assert(a < 65536);
		assert(b < 65536);
		assert(c < 65536);
		assert(d < 65536);
		return (a << 48) | (b << 32) | (c << 16) | d;
	}

	void split(const std::string &s, char delim, Vector<std::string> &elems) {
		std::stringstream ss;
		ss.str(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			elems.push_back(item);
		}
	}

	Vector<std::string> split(const std::string &s, char delim) {
		Vector<std::string> elems;
		split(s, delim, elems);
		return elems;
	}

	Vector<Vector<unsigned int>> get_video_sizes(const std::string& path) {
		Vector<Vector<unsigned int>> video_sizes;
		for (int bit_rate = 0; bit_rate < BITRATE_LEVELS; bit_rate++) {
			std::ifstream in_file(path + std::to_string(bit_rate));
			std::string line;
			Vector<unsigned int> video_size;
			if (in_file.is_open()) {
				while (getline(in_file, line)) {
					video_size.push_back(std::stoi(line));
				}
				in_file.close();
			}
			video_sizes.push_back(video_size);
		}
		return video_sizes;
	}

	float restore_or_compute_download_time(
		UnorderedMap<uint64_t, float>& download_time_map,
		unsigned int video_chunk,
		unsigned int quan_t_idx,
		unsigned int bit_rate,
		Vector<float>& quan_time,
		Vector<float>& quan_bw,
		float dt,
		const Vector<unsigned int>& video_size) {

		uint64_t key = combine(video_chunk, quan_t_idx, bit_rate, 0);
		auto it = download_time_map.find(key);
		if (it != download_time_map.end()) {
			return it->second;
		} else {
			unsigned int chunk_size = video_size[video_chunk];
			float downloaded = 0;
			float time_spent = 0;
			unsigned int quan_idx = quan_t_idx;

			while (true) {
				if (quan_idx >= quan_bw.size()) {
					// Expand bandwidth data if needed when go out of bounds
					quan_time.push_back(quan_time.back() + DT);
					quan_bw.push_back(quan_bw.back());  // Repeat the last known bandwidth
				}
				float curr_bw = quan_bw[quan_idx];  // in Mbit/sec
				downloaded += curr_bw * dt / BITS_IN_BYTE * B_IN_MB * PACKET_PAYLOAD_PORTION;
				quan_idx++;
				if (downloaded >= chunk_size) {
					break;
				}
				time_spent += dt;  // lower bound the time spent
			}
			download_time_map[key] = time_spent;
			return time_spent;
		}
	}

	template <typename T>
	T must_retrieve(
		UnorderedMap<uint64_t, T>& dict_map,
		unsigned int chunk_idx,
		unsigned int time_idx,
		unsigned int buffer_idx,
		unsigned int bit_rate) {
		uint64_t key = combine(chunk_idx, time_idx, buffer_idx, bit_rate);
		auto it = dict_map.find(key);
		assert(it != dict_map.end());
		return it->second;
	}

	template <typename T>
	void insert_or_update(
		UnorderedMap<uint64_t, T>& dict_map,
		unsigned int chunk_idx,
		unsigned int time_idx,
		unsigned int buffer_idx,
		unsigned int bit_rate,
		T value) {
		uint64_t key = combine(chunk_idx, time_idx, buffer_idx, bit_rate);
		auto it = dict_map.find(key);
		if (it == dict_map.end()) {
			dict_map.insert({key, value});
		} else {
			it->second = value;
		}
	}

	template <typename T>
	bool insert_or_compare_and_update(
		UnorderedMap<uint64_t, T>& dict_map,
		unsigned int chunk_idx,
		unsigned int time_idx,
		unsigned int buffer_idx,
		unsigned int bit_rate,
		T value) {
		uint64_t key = combine(chunk_idx, time_idx, buffer_idx, bit_rate);
		auto it = dict_map.find(key);
		if (it == dict_map.end()) {
			dict_map.insert({key, value});
			return true;
		} else {
			if (value > it->second) {
				it->second = value;
				return true;
			}
		}
		return false;
	}

	template <typename T>
	bool found_in(
		UnorderedMap<uint64_t, T>& dict_map,
		unsigned int chunk_idx,
		unsigned int time_idx,
		unsigned int buffer_idx,
		unsigned int bit_rate) {
		uint64_t key = combine(chunk_idx, time_idx, buffer_idx, bit_rate);
		auto it = dict_map.find(key);
		return it != dict_map.end();
	}

	// List files in directory to check if already exist
	Vector<std::string> get_directory_files(const std::string& path) {
		Vector<std::string> files;
		DIR* dir = opendir(path.c_str());
		if (dir != NULL) {
			struct dirent* entry;
			while ((entry = readdir(dir)) != NULL) {
				if (entry->d_name[0] != '.') {
					files.push_back(entry->d_name);
				}
			}
			closedir(dir);
		}
		return files;
	}

	/* Returns list of file names to process individually
	* Original design loaded all at once and caused system crash
	* Check output directory for files already processed to save time */
	Vector<std::string> get_all_file_names(const std::string& input_path, const std::string& output_path=OUTPUT_DIR) {
		// Get list of files in output_path
		Vector<std::string> output_files = get_directory_files(output_path);
		std::unordered_set<std::string> output_files_set(output_files.begin(), output_files.end());

		// Get list of files in input_path
		Vector<std::string> input_files = get_directory_files(input_path);

		// Debugging: comparison error was due to file prefix
		/*
		std::cout << "Files to process:" << std::endl;
		for (const auto& file : input_files) {
			std::cout << file << std::endl;
		}
		std::cout << "Files already processed:" << std::endl;
		for (const auto& file : output_files) {
			std::cout << file << std::endl;
		} */

		// Filter input files
		Vector<std::string> file_names;
		for (const auto& file : input_files) {
			std::string temp = "log_sim_dp_" + file; // forgot prefix on filename
			if (output_files_set.find(temp) == output_files_set.end()) {
				file_names.push_back(file);
				std::cout << "New file added: " << file << std::endl; // New
			} else {
				std::cout << "File already exists: " << file << std::endl; // Skip
			}
		}

		std::cout << "Number of files to process: " << file_names.size() << std::endl;
		return file_names;
	}

	// New functions to load data for a single file, uses custom vector template
	// std::vector<float> load_cooked_time(const std::string& file_name) {
	Vector<float> load_cooked_time(const std::string& file_name) {
		Vector<float> cooked_time;
		std::ifstream in_file(COOKED_TRACE_FOLDER + file_name);
		std::string line;
		if (in_file.is_open()) {
			while (getline(in_file, line)) {
				Vector<std::string> parse = split(line, '\t');
				cooked_time.push_back(std::stof(parse[0]));
			}
			in_file.close();
		}
		return cooked_time;
	}

	// New functions to load data for a single file, uses custom vector template
	// std::vector<float> load_cooked_bw(const std::string& file_name) {
	Vector<float> load_cooked_bw(const std::string& file_name) {
		Vector<float> cooked_bw;
		std::ifstream in_file(COOKED_TRACE_FOLDER + file_name);
		std::string line;
		if (in_file.is_open()) {
			while (getline(in_file, line)) {
				Vector<std::string> parse = split(line, '\t');
				cooked_bw.push_back(std::stof(parse[1]));
			}
			in_file.close();
		}
		return cooked_bw;
	}

	void log_zero_bandwidth(const std::string& file_name, unsigned int time_index) {
		std::ofstream zero_bw_log("dp_logs/zero_bandwidth_log.txt", std::ios::app);
		if (zero_bw_log.is_open()) {
			zero_bw_log << "Zero bandwidth detected in file: " << file_name 
						<< " at time index: " << time_index << std::endl;
			zero_bw_log.close();
		}
	}

	// Logging to try and capture reason for out of bounds breaking lte/belgium logs - works on norway (lower bw??)
	void log_detailed_info(const std::string& file_name, unsigned int n, unsigned int t, unsigned int b, unsigned int m, 
						float download_time, float buffer_size, float rebuf, float reward, 
						unsigned int time_idx, unsigned int buffer_idx, unsigned int t_max_idx) {
		static int log_count = 0;
		static const int LOG_FREQUENCY = 1000; // Log every 1000th call, reduce volume of data
		static std::mutex log_mutex; // locks access for thread safety
		static bool file_initialized = false;

		if (++log_count % LOG_FREQUENCY == 0 || time_idx >= t_max_idx) { // Log periodically or when time_idx exceeds t_max_idx
			std::lock_guard<std::mutex> lock(log_mutex);
			
			std::ofstream log_file;
			
			if (!file_initialized) {
				// Open the file in truncation mode to clear its contents
				log_file.open("dp_logs/compact_detailed_log.csv", std::ios::trunc);
				log_file << "file,chunk,t,b,m,download_time,buffer_size,rebuf,reward,time_idx,buffer_idx,exceeds_t_max\n";
				file_initialized = true;
			} else {
				// Open in append mode for subsequent writes
				log_file.open("dp_logs/compact_detailed_log.csv", std::ios::app);
			}

			if (log_file.is_open()) {
				log_file << file_name << ","
						<< n << ","
						<< t << ","
						<< b << ","
						<< m << ","
						<< download_time << ","
						<< buffer_size << ","
						<< rebuf << ","
						<< reward << ","
						<< time_idx << ","
						<< buffer_idx << ","
						<< (time_idx >= t_max_idx ? "1" : "0") << "\n";
				log_file.close();
			}
		}
	}

	void log_time_index_expansion(const std::string& file_name, unsigned int chunk_index, unsigned int original_time_idx, unsigned int new_t_max_idx, unsigned int expansion_count) {
		std::ofstream expansion_log("dp_logs/time_index_expansion_log.txt", std::ios::app);
		if (expansion_log.is_open()) {
			expansion_log << "File: " << file_name 
						<< ", Chunk: " << chunk_index
						<< ", Original time_idx: " << original_time_idx 
						<< ", New t_max_idx: " << new_t_max_idx 
						<< ", Expansion count: " << expansion_count << std::endl;
			expansion_log.close();
		}
	}

	void log_time_index_capping(const std::string& file_name, unsigned int chunk_index, unsigned int original_time_idx, unsigned int capped_time_idx) {
		std::ofstream cap_log("dp_logs/time_index_capping_log.txt", std::ios::app);
		if (cap_log.is_open()) {
			cap_log << "File: " << file_name 
					<< ", Chunk: " << chunk_index
					<< ", Original time_idx: " << original_time_idx 
					<< ", Capped to: " << capped_time_idx << std::endl;
			cap_log.close();
		}
	}

	// Main processing function for a single file - logic is the same as original main()
	void process_single_file(const std::string& file_name, const Vector<Vector<unsigned int>>& video_sizes) {
		signal(SIGALRM, timeout_handler); // timeout

		const unsigned int MAX_TIME_EXPANSIONS = 5; // no more unbounded expansion of t_max_idx
		unsigned int time_expansions_count = 0;

		set_alarm(60, ProcessingStage::LOADING);  // Setting alarms to timeout if individual stages take too long
		Vector<float> cooked_time = load_cooked_time(file_name);
		Vector<float> cooked_bw = load_cooked_bw(file_name);
		clear_alarm();

		// Catch bw=0 and optionally log to file
		for (unsigned int i = 0; i < cooked_bw.size(); i++) {
			if (cooked_bw[i] <= 0) {
				// log_zero_bandwidth(file_name, i);
				cooked_bw[i] = std::numeric_limits<float>::min();  // Set to a very small positive value
			}
		}

		// Step 1: Quantize the time and bandwidth
		set_alarm(60, ProcessingStage::QUANTIZATION);
		unsigned int total_time_pt = ceil(cooked_time.back() / DT);
		Vector<float> quan_time(total_time_pt + 1);
		Vector<float> quan_bw(total_time_pt + 1);
		
		float d_t = floor(cooked_time[0]);
		for (unsigned int i = 0; i <= total_time_pt; i++) {
			quan_time[i] = d_t;
			d_t += DT;
		}

		unsigned int curr_time_idx = 0;
		for (unsigned int i = 0; i < quan_bw.size(); i++) {
			while (curr_time_idx < cooked_time.size() - 1 && cooked_time[curr_time_idx] < quan_time[i]) {
				curr_time_idx++;
			}
			quan_bw[i] = cooked_bw[curr_time_idx];
		}

		clear_alarm();

		// Step 2: Cap the max time and max buffer
		set_alarm(60, ProcessingStage::CAPPING);
		unsigned int max_video_contents = std::accumulate(video_sizes.back().begin(), video_sizes.back().end(), 0);
		float total_bw = std::accumulate(quan_bw.begin(), quan_bw.end(), 0.0f) * DT;  // in Mbit
		float t_portion = max_video_contents / (total_bw * B_IN_MB * PACKET_PAYLOAD_PORTION / BITS_IN_BYTE);
		unsigned int t_max = std::ceil(cooked_time.back() * t_portion * 4);  // Increasing cap to try and handle out of bounds error
		unsigned int t_max_idx = std::ceil(t_max / DT);
		unsigned int b_max_idx = t_max_idx;

		for (unsigned int i = 1; i < ceil(t_portion); i++) {
			float last_quan_time = quan_time.back();
			for (unsigned int j = 1; j <= total_time_pt; j++) {
				quan_time.push_back(quan_time[j] + last_quan_time);
				quan_bw.push_back(quan_bw[j]);
			}
		}

		while (quan_time.back() < t_max) { // quan_time extended instead of failing assert check
			// expand quan_time has 1 idx less (0 idx is 0 time)
			// precaution step
			float last_quan_time = quan_time.back();
			for (unsigned int j = 1; j <= total_time_pt; j++) {
				quan_time.push_back(quan_time[j] + last_quan_time);
				quan_bw.push_back(quan_bw[j]);
			}
		}
		// assert(quan_time.back() >= t_max);

		clear_alarm();

		// Step 3: Pre-compute the download time of chunks download_time(chunk_idx, quan_time, bit_rate)
		set_alarm(240, ProcessingStage::PRECOMPUTATION);
		UnorderedMap<uint64_t, float> download_time_map;

		for (unsigned int i = 0; i < TOTAL_VIDEO_CHUNCK; i++) {
			for (unsigned int j = 0; j < t_max_idx; j++) {
				for (unsigned int k = 0; k < BITRATE_LEVELS; k++) {
					restore_or_compute_download_time(download_time_map, i, j, k, quan_time, quan_bw, DT, video_sizes[k]);
				}
			}
		}

		clear_alarm();

		// Step 4: Dynamic programming
		set_alarm(240, ProcessingStage::DYNAMIC_PROGRAMMING);
		Vector<UnorderedMap<uint64_t, float>> total_reward(TOTAL_VIDEO_CHUNCK);
		UnorderedMap<uint64_t, DP_PT> last_dp_pt;

		float download_time = restore_or_compute_download_time(download_time_map, 0, 0, DEFAULT_QUALITY, quan_time, quan_bw, DT, video_sizes[DEFAULT_QUALITY]);
		float chunk_finish_time = download_time + LINK_RTT / M_IN_K;
		unsigned int time_idx = floor(chunk_finish_time / DT);
		unsigned int buffer_idx = int(VIDEO_CHUNCK_LEN / M_IN_K / DT);

		float reward;
		if (use_hd_reward) {
			// HD reward
			reward = BITRATE_REWARD[DEFAULT_QUALITY] - 8 * chunk_finish_time;
		} else {
			// Linear reward
			reward = VIDEO_BIT_RATE[DEFAULT_QUALITY] / M_IN_K - REBUF_PENALTY * chunk_finish_time;
		}


		insert_or_update(total_reward[0], 0, time_idx, buffer_idx, DEFAULT_QUALITY, reward);
		DP_PT dp_pt = {0, 0, 0, 0};
		insert_or_update(last_dp_pt, 0, time_idx, buffer_idx, DEFAULT_QUALITY, dp_pt);

		for (unsigned int n = 1; n < TOTAL_VIDEO_CHUNCK; n++) {
			std::cout << n << " " << TOTAL_VIDEO_CHUNCK << '\n';
			float max_reward_up_to_n = -INFINITY;
			float max_reward_remaining_after_n = (TOTAL_VIDEO_CHUNCK - n - 1) * VIDEO_BIT_RATE[MAX_QUALITY] / M_IN_K;

			for (const auto& it : total_reward[n - 1]) {
				uint64_t key = it.first;
				uint64_t mask = 65536 - 1;
				unsigned int m = key & mask;
				unsigned int b = (key >> 16) & mask;
				unsigned int t = (key >> 32) & mask;
				float boot_strap_reward = it.second;
				//unsigned int chunk_num = (key >> 48) & mask;
				//assert (chunk_num == n - 1);

				for (unsigned int nm = 0; nm < BITRATE_LEVELS; nm++) {
					if (t >= t_max_idx) {
						std::cerr << "Error: time index out of bounds: t = " << t << ", t_max_idx = " << t_max_idx << std::endl;
						continue;
					}
					if (nm >= BITRATE_LEVELS) {
						std::cerr << "Error: bitrate level index out of bounds: nm = " << nm << ", BITRATE_LEVELS = " << BITRATE_LEVELS << std::endl;
						continue;
					}

					download_time = restore_or_compute_download_time(download_time_map, n, t, nm, quan_time, quan_bw, DT, video_sizes[nm]);
					if (download_time == std::numeric_limits<float>::max() || download_time > BUFFER_THRESH) {
						continue; // Skip this option if download time is infinite (bw = 0) or buffer full
					}

					float buffer_size = quan_time[b];
					float rebuf;
					if (use_hd_reward) {
						// HD reward
						rebuf = std::max(0.0f, download_time - buffer_size);
						reward = BITRATE_REWARD[nm] 
								- 8 * rebuf 
								- SMOOTH_PENALTY * std::abs(BITRATE_REWARD[nm] - BITRATE_REWARD[m]);
					} else {
						// Linear reward (default)
						rebuf = download_time - buffer_size;
						if (rebuf < 0) rebuf = 0;
						reward = VIDEO_BIT_RATE[nm] / M_IN_K 
								- REBUF_PENALTY * rebuf 
								- SMOOTH_PENALTY * std::abs(VIDEO_BIT_RATE[nm] - VIDEO_BIT_RATE[m]) / M_IN_K;
					}

					buffer_size = buffer_size - download_time - LINK_RTT / M_IN_K;
					if (buffer_size < 0) buffer_size = 0;
					buffer_size += VIDEO_CHUNCK_LEN / M_IN_K;

					float drain_buffer_time = 0;
					if (buffer_size > BUFFER_THRESH) {
						drain_buffer_time = buffer_size - BUFFER_THRESH;
						buffer_size = BUFFER_THRESH;
					}

					buffer_idx = ceil(buffer_size / DT);
					chunk_finish_time = quan_time[t] + download_time + drain_buffer_time + LINK_RTT / M_IN_K;
					time_idx = floor(chunk_finish_time / DT);

					log_detailed_info(file_name, n, t, b, m, download_time, buffer_size, rebuf, reward, time_idx, buffer_idx, t_max_idx);
					if (time_idx >= t_max_idx) {
						std::cerr << "Warning: Computed time index out of bounds: time_idx = " << time_idx << ", t_max_idx = " << t_max_idx << std::endl;
						
						// No more infinite expansion
						if (time_expansions_count < MAX_TIME_EXPANSIONS) {
							// Expand time range
							unsigned int new_t_max_idx = time_idx + 1;
							while (t_max_idx < new_t_max_idx) {
								quan_time.push_back(quan_time.back() + DT);
								quan_bw.push_back(quan_bw.back());  // Repeat the last known bandwidth
								t_max_idx++;
							}
							b_max_idx = t_max_idx;  // Update b_max_idx as well
							time_expansions_count++;

							log_time_index_expansion(file_name, n, time_idx, t_max_idx, time_expansions_count);
						} else {
							// Cap time_idx at t_max_idx
							log_time_index_capping(file_name, n, time_idx, t_max_idx);
							time_idx = t_max_idx - 1;
							
							// Adjust reward calculation for capped time
							float capped_time_penalty = (chunk_finish_time - quan_time[time_idx]) * REBUF_PENALTY;
							reward -= capped_time_penalty;
						}
					}

					reward += boot_strap_reward;
					max_reward_up_to_n = (reward > max_reward_up_to_n) ? reward : max_reward_up_to_n;

					if (reward + max_reward_remaining_after_n >= max_reward_up_to_n - 0.01) {
						bool updated = insert_or_compare_and_update(total_reward[n], n, time_idx, buffer_idx, nm, reward);
						if (updated) {
							dp_pt = {n - 1, t, b, m};
							insert_or_update(last_dp_pt, n, time_idx, buffer_idx, nm, dp_pt);
						}
					}
				}
			}
		}

		unsigned int total_keys = 0;
		for (const auto& tr : total_reward) {
			total_keys += tr.size();
		}

		std::cout << "total keys = " << total_keys << std::endl;
		std::cout << "max keys = " << TOTAL_VIDEO_CHUNCK * t_max_idx * b_max_idx * BITRATE_LEVELS << std::endl;

		clear_alarm();

		// Step 5: Get the max total reward
		set_alarm(60, ProcessingStage::FINAL_PROCESSING); // final processing and write to file
		float optimal_total_reward = -std::numeric_limits<float>::max();
		dp_pt = {0, 0, 0, 0};

		unsigned int last_time_idx = 0;
		unsigned int last_buff_idx = 0;
		unsigned int last_bit_rate = 0;

		for (const auto& it : total_reward[TOTAL_VIDEO_CHUNCK - 1]) {
			uint64_t key = it.first;
			uint64_t mask = 65536 - 1;
			unsigned int m = key & mask;
			unsigned int b = (key >> 16) & mask;
			unsigned int t = (key >> 32) & mask;
			float tmp_total_reward = it.second;
			if (tmp_total_reward > optimal_total_reward) {
				optimal_total_reward = tmp_total_reward;
				dp_pt = must_retrieve(last_dp_pt, TOTAL_VIDEO_CHUNCK - 1, t, b, m);
				last_time_idx = t;
				last_buff_idx = b;
				last_bit_rate = m;
			}
		}

		// Write results to file
		std::ofstream log_file(OUTPUT_FILE_PATH + "_" + file_name);

		// Hopefully catch seg faults/errors
		if (!log_file.is_open()) {
			throw std::runtime_error("Unable to open output file for writing");
		}

		log_file << optimal_total_reward << '\n';
		log_file << TOTAL_VIDEO_CHUNCK - 1 << '\t'
			<< last_time_idx << '\t'
			<< last_buff_idx << '\t'
			<< quan_time[last_time_idx] << '\t'
			<< quan_time[last_buff_idx] << '\t'
			<< quan_bw[last_time_idx] << '\t'
			<< last_bit_rate << '\n';

		while (dp_pt.chunk_idx != 0 || dp_pt.time_idx != 0 ||
			dp_pt.buffer_idx != 0 || dp_pt.bit_rate != 0) {
			log_file << dp_pt.chunk_idx << '\t'
				<< dp_pt.time_idx << '\t'
				<< dp_pt.buffer_idx << '\t'
				<< quan_time[dp_pt.time_idx] << '\t'
				<< quan_time[dp_pt.buffer_idx] << '\t'
				<< quan_bw[dp_pt.time_idx] << '\t'
				<< dp_pt.bit_rate << '\n';
			dp_pt = must_retrieve(last_dp_pt, dp_pt.chunk_idx, dp_pt.time_idx, dp_pt.buffer_idx, dp_pt.bit_rate);
		}
		log_file << '\n';

		log_file.close();

		// Clear large data structures
		download_time_map.clear();
		total_reward.clear();
		last_dp_pt.clear();

		clear_alarm(); // finished single file
	}

	// Create log file for errors
	void log_error(const std::string& file_name, const std::string& error_message) {
		std::ofstream error_log("dp_error_log.log", std::ios::app);
		if (error_log.is_open()) {
			error_log << "Error processing file: " << file_name << " - " << error_message << std::endl;
			error_log.close();
		}
	}

	// Wrapper function handles exception in each child process
	// Original signal approach to errors with jumps had undefined behaviour
	void process_file_wrapper(const std::string& file_name, const Vector<Vector<unsigned int>>& video_sizes) {
		signal(SIGSEGV, memory_error_handler);
		signal(SIGABRT, memory_error_handler);

		try {
			process_single_file(file_name, video_sizes);
			exit(0);  // Success
		} catch (const insufficient_memory_error& e) {
			std::cerr << "Insufficient memory to process file: " << file_name << " - " << e.what() << std::endl;
			exit(1);  // Failure due to insufficient memory
		} catch (const std::exception& e) {
			std::cerr << "Exception occurred while processing file: " << file_name << " - " << e.what() << std::endl;
			exit(1);  // Failure
		} catch (...) {
			std::cerr << "Unknown exception occurred while processing file: " << file_name << std::endl;
			exit(2);  // Unknown failure
		}
	}

	// Should be same as os.makedirs(OUTPUT_DIR, exists_ok=True)
	bool mkdir_p(const std::string& path, mode_t mode = 0755) {
		// Create a mutable copy of the path
		std::string current_path = path;
		std::string dir;
		size_t pos = 0;

		// Iterate through each directory in the path
		while ((pos = current_path.find_first_of('/', pos)) != std::string::npos) {
			dir = current_path.substr(0, pos++);
			if (dir.empty()) continue; // Skip empty directories (e.g., if path starts with '/')

			// Try to create the directory
			if (mkdir(dir.c_str(), mode) != 0 && errno != EEXIST) {
				return false; // Failed to create directory and it doesn't already exist
			}
		}

		// Create the final directory
		if (mkdir(current_path.c_str(), mode) != 0 && errno != EEXIST) {
			return false;
		}

		return true;
	}

} // end of namespace abr

using namespace abr; // don't want to type abr::foo() in main()

int main(int argc, char* argv[]) {
	// Makes dir if it doesn't already exist
    if (!mkdir_p(OUTPUT_DIR)) {
        std::cerr << "Failed to create output directory: " << OUTPUT_DIR << std::endl;
        return 1;
    }

	// Select reward type with terminal input instead of having to comment/uncomment code
	if (argc > 1) {
		std::string arg = argv[1];
		if (arg == "hd" || arg == "HD") {
			use_hd_reward = true;
            std::cout << "Using HD reward formula" << std::endl;
        } else if (arg == "linear" || arg == "LINEAR") {
            use_hd_reward = false;
            std::cout << "Using linear reward formula" << std::endl;
        } else {
            std::cerr << "Invalid argument. Using default linear reward formula." << std::endl;
        }
    } else {
        std::cout << "No reward type specified. Using default linear reward formula." << std::endl;
    }

    Vector<Vector<unsigned int>> video_sizes = get_video_sizes(VIDEO_SIZE_FILE);
    Vector<std::string> file_names = get_all_file_names(COOKED_TRACE_FOLDER);

	// Uses subprocesses to handle each file, should continue after failure
	for (const auto& file_name : file_names) {
			std::cout << "Processing file: " << file_name << std::endl;
			
			pid_t pid = fork(); // Create subprocesses for each file
			
			if (pid == -1) {
				// Fork failed
				std::cerr << "Fork failed for file: " << file_name << std::endl;
				log_error(file_name, "Fork failed");
				continue;
			} else if (pid == 0) {
				// Child process
				process_file_wrapper(file_name, video_sizes);
				// The child process will exit in process_file_wrapper
			} else {
				// Parent process
				int status;
				waitpid(pid, &status, 0);
				
				if (WIFEXITED(status)) {
					if (WEXITSTATUS(status) == 0) {
						std::cout << "Successfully processed file: " << file_name << std::endl;
					} else {
						std::cerr << "Failed to process file: " << file_name << " (exit status: " << WEXITSTATUS(status) << ")" << std::endl;
						log_error(file_name, "Process exited with non-zero status");
					}
				} else if (WIFSIGNALED(status)) {
					std::cerr << "Process for file " << file_name << " terminated by signal " << WTERMSIG(status) << std::endl;
					log_error(file_name, "Process terminated by signal");
				}
			}
		}

    return 0;
} // end of main