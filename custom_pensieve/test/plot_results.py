import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import logging
import argparse

# interactive graph for individual traces
from matplotlib.widgets import Slider

RESULTS_FOLDER = './results/'
NUM_BINS = 100
BITS_IN_BYTE = 8.0
MILLISEC_IN_SEC = 1000.0
M_IN_B = 1000000.0
VIDEO_LEN = 48
VIDEO_BIT_RATE = [300, 750, 1200, 1850, 2850, 4300]
K_IN_M = 1000.0
REBUF_P = 4.3
SMOOTH_P = 1
COLOR_MAP = plt.cm.jet #nipy_spectral, Set1,Paired 
SIM_DP = 'sim_dp' # 'sim_dp'
DATASET = "Norway HSDPA"

# SCHEMES = [SIM_DP, 'sim_rl']
SCHEMES = ['sim_rl', 'sim_bb', 'sim_mpc', 'sim_oboempc', 'sim_tf_50k', 'sim_tf_full']#, SIM_DP]

# Setup logging
logging.basicConfig(filename='plot_summary_log.log', level=logging.INFO, format='%(asctime)s %(message)s', filemode='w')

# Add cli argument to skip plotting charts
def parse_args():
    parser = argparse.ArgumentParser(description="Plot and print results")
    parser.add_argument('--nochart', '--n', '--no', '-0', action='store_true', help="Skip generating charts")
    return parser.parse_args()

# Prints out results for each SCHEMES entry to text - so can be recorded elsewhere
def print_results(reward_all, mean_rewards, median_rewards, avg_bitrate_all):
    print("\n--- Results Summary ---")
    for scheme in SCHEMES:
        print(f"\nScheme: {scheme}")
        
        if reward_all[scheme]:
            print(f"Min Reward: {min(reward_all[scheme]):.2f}")
            print(f"Max Reward: {max(reward_all[scheme]):.2f}")
            print(f"Median Reward: {median_rewards[scheme]:.2f}")
        else:
            print("No reward data available")
        print(f"Mean Reward: {mean_rewards[scheme]:.2f}")
        
        if avg_bitrate_all[scheme]:
            avg_bitrates = list(avg_bitrate_all[scheme].values())
            print(f"Min Bitrate: {min(avg_bitrates):.2f} kbps")
            print(f"Max Bitrate: {max(avg_bitrates):.2f} kbps")
            print(f"Median Bitrate: {np.median(avg_bitrates):.2f} kbps")
            print(f"Average Bitrate: {np.mean(avg_bitrates):.2f} kbps")
        else:
            print("No bitrate data available")
    print("\n--- End of Results Summary ---")
    
# Debugging: Summarises array details
def print_summary(array, name):
    array_np = np.array(array)
    if array_np.size > 0:
        logging.info(f"{name} Summary: Length = {len(array)}, Mean = {np.mean(array_np)}, Min = {np.min(array_np)}, Max = {np.max(array_np)}")
    else:
        logging.info(f"{name} Summary: Length = 0 (empty array)")

# Separated plotting for each chart for make main() function more readable
def plot_total_reward(reward_all, SCHEMES, mean_rewards, colors, dataset_name):
    fig, ax = plt.subplots(figsize=(12, 6))

    for idx, scheme in enumerate(SCHEMES):
        if reward_all[scheme]:
            # Replaced sim_dp with offline optimal
            label = f"offline optimal: {mean_rewards[scheme]:.2f}" if scheme == 'sim_dp' else f"{scheme}: {mean_rewards[scheme]:.2f}"
            ax.plot(reward_all[scheme], color=colors[idx], label=label)

    ax.set_ylabel('Total Reward', fontsize=12, fontweight='bold')
    ax.set_xlabel('Trace Index', fontsize=12, fontweight='bold')
    ax.set_title(f'Total Reward per Scheme ({dataset_name})', fontsize=14, fontweight='bold')
    ax.grid(True, linestyle='--', alpha=0.7)
    ax.legend(loc='best', fontsize=10) # automatic positioning to minimize overlap

    plt.tight_layout()
    plt.show()

# Separated plotting for each chart for make main() function more readable
def plot_cdf(reward_all, SCHEMES, NUM_BINS, colors, dataset_name):
    fig, ax = plt.subplots(figsize=(12, 6))

    for idx, scheme in enumerate(SCHEMES):
        if reward_all[scheme]:
            values, base = np.histogram(reward_all[scheme], bins=NUM_BINS)
            cumulative = np.cumsum(values) / np.sum(values)
            ax.plot(base[:-1], cumulative, color=colors[idx], label=scheme)

    ax.set_ylabel('CDF', fontsize=12, fontweight='bold')
    ax.set_xlabel('Total Reward', fontsize=12, fontweight='bold')
    ax.set_title(f'Cumulative Distribution Function of Total Reward ({dataset_name})', fontsize=14, fontweight='bold')
    ax.grid(True, linestyle='--', alpha=0.7)
    ax.legend(loc='best', fontsize=10) # automatic positioning to minimize overlap

    plt.tight_layout()
    plt.show()
    
# Separated plotting for each chart for make main() function more readable
def plot_average_bitrate(avg_bitrate_all, SCHEMES, colors, dataset_name):
    fig, ax = plt.subplots(figsize=(14, 7))  # Increased figure size

    max_y = 0
    for idx, scheme in enumerate(SCHEMES):
        if avg_bitrate_all[scheme]:
            trace_ids = sorted(avg_bitrate_all[scheme].keys())
            avg_bitrates = [avg_bitrate_all[scheme][trace_id] for trace_id in trace_ids]
            overall_avg = np.mean(avg_bitrates)
            
            # Modify the label for 'sim_dp'
            label = f"offline optimal: {overall_avg:.2f} kbps" if scheme == 'sim_dp' else f"{scheme}: {overall_avg:.2f} kbps"
            ax.plot(range(len(trace_ids)), avg_bitrates, color=colors[idx], label=label)
            
            max_y = max(max_y, max(avg_bitrates))

    ax.set_ylabel('Average Bitrate (kbps)', fontsize=12, fontweight='bold')
    ax.set_xlabel('Trace Index', fontsize=12, fontweight='bold')
    ax.set_title(f'Average Bitrate per Trace Index ({dataset_name})', fontsize=14, fontweight='bold')
    ax.grid(True, linestyle='--', alpha=0.7)

    # Adjust the plot area to make room for the legend
    plt.subplots_adjust(right=0.75)

    # Place the legend in the bottom right corner
    ax.legend(loc='best', fontsize=10) # automatic positioning to minimize overlap

    # Set y-axis limit with some headroom
    ax.set_ylim(0, max_y * 1.1)

    plt.tight_layout()
    plt.show()
    
# Should be interactive and able to select individual trace files from chart - more complex as running via cli instead of just as a jupyter widget
def interactive_trace_plot(time_all, bit_rate_all, buff_all, bw_all, raw_reward_all, SCHEMES, colors, dataset_name):
    traces = list(time_all[SCHEMES[0]].keys())
    
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(12, 15))
    plt.subplots_adjust(left=0.1, bottom=0.25, right=0.9, top=0.9, hspace=0.5)

    lines1, lines2, lines3 = [], [], []
    for i, scheme in enumerate(SCHEMES):
        l1, = ax1.plot([], [], label=scheme, color=colors[i])
        l2, = ax2.plot([], [], label=scheme, color=colors[i])
        l3, = ax3.plot([], [], label=scheme, color=colors[i])
        lines1.append(l1)
        lines2.append(l2)
        lines3.append(l3)

    ax1.set_ylabel('Bit Rate (kbps)')
    ax2.set_ylabel('Buffer Size (sec)')
    ax3.set_ylabel('Bandwidth (mbps)')
    ax3.set_xlabel('Time (sec)')

    ax1.legend()
    ax2.legend()
    ax3.legend()

    def update(val):
        trace = traces[int(val)]
        ax1.set_title(f'Trace: {trace}')
        for i, scheme in enumerate(SCHEMES):
            if trace in time_all[scheme]:
                t = time_all[scheme][trace][:VIDEO_LEN]
                lines1[i].set_data(t, bit_rate_all[scheme][trace][:VIDEO_LEN])
                lines2[i].set_data(t, buff_all[scheme][trace][:VIDEO_LEN])
                lines3[i].set_data(t, bw_all[scheme][trace][:VIDEO_LEN])
        
        for ax in (ax1, ax2, ax3):
            ax.relim()
            ax.autoscale_view()

        reward_text = "Rewards: " + ", ".join([f"{scheme}: {np.sum(raw_reward_all[scheme][trace][1:VIDEO_LEN]):.2f}" for scheme in SCHEMES])
        fig.suptitle(reward_text, fontsize=10)

        fig.canvas.draw_idle()

    ax_slider = plt.axes([0.1, 0.1, 0.8, 0.03])
    slider = Slider(ax_slider, 'Trace Index', 0, len(traces) - 1, valinit=0, valstep=1)
    slider.on_changed(update)
    
    update(0)
    plt.show()

def main(dataset_name=DATASET):    
    time_all = {}
    bit_rate_all = {}
    buff_all = {}
    bw_all = {}
    raw_reward_all = {}
    avg_bitrate_all = {scheme: {} for scheme in SCHEMES}

    for scheme in SCHEMES:
        time_all[scheme] = {}
        raw_reward_all[scheme] = {}
        bit_rate_all[scheme] = {}
        buff_all[scheme] = {}
        bw_all[scheme] = {}

    log_files = os.listdir(RESULTS_FOLDER)
    for log_file in log_files:
        if not log_file.startswith('log_'):
            continue

        time_ms = []
        bit_rate = []
        buff = []
        bw = []
        reward = []

        logging.info(f"Processing file: {log_file}")

        with open(RESULTS_FOLDER + log_file, 'r') as f:
            if SIM_DP in log_file:
                last_t = 0
                last_b = 0
                last_q = 1
                lines = []
                for line in f:
                    lines.append(line)
                    parse = line.split()
                    if len(parse) >= 6:
                        time_ms.append(float(parse[3]))
                        bit_rate.append(VIDEO_BIT_RATE[int(parse[6])])
                        buff.append(float(parse[4]))
                        bw.append(float(parse[5]))

                for line in reversed(lines):
                    parse = line.split()
                    r = 0
                    if len(parse) > 1:
                        t = float(parse[3])
                        b = float(parse[4])
                        q = int(parse[6])
                        if b == 4:
                            rebuff = (t - last_t) - last_b
                            assert rebuff >= -1e-4
                            r -= REBUF_P * rebuff

                        r += VIDEO_BIT_RATE[q] / K_IN_M
                        r -= SMOOTH_P * np.abs(VIDEO_BIT_RATE[q] - VIDEO_BIT_RATE[last_q]) / K_IN_M
                        reward.append(r)

                        last_t = t
                        last_b = b
                        last_q = q
            else:
                for line in f:
                    parse = line.split()
                    if len(parse) <= 1:
                        break
                    time_ms.append(float(parse[0]))
                    bit_rate.append(int(parse[1]))
                    buff.append(float(parse[2]))
                    bw.append(float(parse[4]) / float(parse[5]) * BITS_IN_BYTE * MILLISEC_IN_SEC / M_IN_B)
                    reward.append(float(parse[6]))

                if not time_ms:
                    logging.warning(f"Warning: No data parsed from {log_file}")
                    continue

        if SIM_DP in log_file:
            time_ms = time_ms[::-1]
            bit_rate = bit_rate[::-1]
            buff = buff[::-1]
            bw = bw[::-1]
            
            # print(f"Log file SIM_DP: {time_ms} {bit_rate} {buff} {bw}")
            
        time_ms = np.array(time_ms)
        time_ms -= time_ms[0]

        for scheme in SCHEMES:
            if scheme in log_file:
                trace_id = log_file[len('log_' + str(scheme) + '_'):]
                time_all[scheme][trace_id] = time_ms
                bit_rate_all[scheme][trace_id] = bit_rate
                buff_all[scheme][trace_id] = buff
                bw_all[scheme][trace_id] = bw
                raw_reward_all[scheme][trace_id] = reward
                
                # Calculate average bitrate
                avg_bitrate = np.mean(bit_rate[:VIDEO_LEN])
                avg_bitrate_all[scheme][trace_id] = avg_bitrate
                
                logging.debug(f"Processed data for scheme {scheme}, trace {trace_id}: time_ms={time_ms}, bit_rate={bit_rate}, buff={buff}, bw={bw}, reward={reward}")
                break

    # Reward records
    log_file_all = []
    reward_all = {scheme: [] for scheme in SCHEMES}  # Initialize reward_all as a dictionary of lists

    log_file_all = []
    reward_all = {}
    for scheme in SCHEMES:
        reward_all[scheme] = []

    for l in time_all[SCHEMES[0]]:
        schemes_check = True
        for scheme in SCHEMES:
            if l not in time_all[scheme] or len(time_all[scheme][l]) < VIDEO_LEN:
                schemes_check = False
                break
        if schemes_check:
            log_file_all.append(l)
            for scheme in SCHEMES:
                reward_all[scheme].append(np.sum(raw_reward_all[scheme][l][1:VIDEO_LEN]))

    mean_rewards = {}
    median_rewards = {}
    for scheme in SCHEMES:
        if reward_all[scheme]:
            mean_rewards[scheme] = np.mean(reward_all[scheme])
            median_rewards[scheme] = np.median(reward_all[scheme])
        else:
            mean_rewards[scheme] = float('nan')
            median_rewards[scheme] = float('nan')
    
    # Print results to terminal
    print_results(reward_all, mean_rewards, median_rewards, avg_bitrate_all)
    
    args = parse_args()
    
    # Skipping plotting if nocharts specified
    if not args.nochart:
        # Set the style
        plt.style.use('seaborn')
        sns.set_palette("deep")
        # Custom color palette
        colors = sns.color_palette("deep", n_colors=len(SCHEMES))
        # Font settings
        plt.rcParams['font.family'] = 'serif'
        plt.rcParams['font.serif'] = ['Times New Roman'] + plt.rcParams['font.serif']
        
        # Plot charts
        plot_total_reward(reward_all, SCHEMES, mean_rewards, colors, DATASET)
        plot_cdf(reward_all, SCHEMES, NUM_BINS, colors, DATASET)
        plot_average_bitrate(avg_bitrate_all, SCHEMES, colors, DATASET)
        # interactive_trace_plot(time_all, bit_rate_all, buff_all, bw_all, raw_reward_all, SCHEMES, colors, DATASET) # Creates interactive chart for all traces
    else:
        print("\nSkipping charts as requested.")
     
                
if __name__ == '__main__':
    main()
