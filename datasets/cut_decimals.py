import os

# Function to round the second column to 6 decimal places
def round_column_data(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()

    rounded_lines = []
    for line in lines:
        parts = line.split()
        if len(parts) == 2:
            first_col = round(float(parts[0]), 2)
            second_col = round(float(parts[1]), 6)
            second_col_str = ('{:.6f}'.format(second_col)).rstrip('0').rstrip('.') # Strip trailing zeroes
            rounded_lines.append(f"{first_col}\t{second_col_str}\n")

    # Replace the original file with the rounded data
    with open(file_path, 'w') as file:
        file.writelines(rounded_lines)

# List of directories to check
directories = ['./cooked_test_traces/', './cooked_traces/']

# Process each .log file in the specified directories
for directory_path in directories:
    if os.path.exists(directory_path) and os.path.isdir(directory_path):
        for filename in os.listdir(directory_path):
            if filename.endswith('.log'):
                file_path = os.path.join(directory_path, filename)
                round_column_data(file_path)