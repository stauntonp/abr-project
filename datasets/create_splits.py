import os
import shutil
import random
import logging
import sys

random.seed(42) # ensures reproducibility
INPUT_DIR = './cooked'
TRAIN_DIR = './cooked_traces'
TEST_DIR = './cooked_test_traces'
LOG_FILE = 'split_progress.log'
MAX_FILES = 300
# Remove the existing log file if it exists
if os.path.exists(LOG_FILE):
    os.remove(LOG_FILE)
# Set up logging - valid_file check not working. Too many files to read print output
logging.basicConfig(filename=LOG_FILE, level=logging.INFO, 
                    format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

# Checks file is acceptable format before being added to list of training/testing files
# Testing broke on faulty log files for other datasets
def is_valid_file(file_path):
    # Forgot that not all files are log files
    # if not file_path.endswith('.log'):
    #     logging.info(f"Rejecting {file_path}: Not a .log file")
    #     return False
    
    # policies files have extremely low throughput, suspect they are ruining data
    # if file_path.endswith('-policies-'):
    # Filter if matched term appears in file_path, even before _0, _60, _120 chunk affixes
    if '-policies-' in file_path or '-help-' in file_path:
        logging.info(f"Rejecting {file_path}: as it is a -policies- or -help- trace file.")
        return False
    # if file_path.endswith('_0'):
    #     logging.info(f"Rejecting {file_path}: first chunk from custom chunks.")

    try:
        with open(file_path, 'r') as f:
            lines = f.readlines()
            
            # Check if file is empty
            if not lines:
                logging.info(f"Rejecting {file_path}: Empty file")
                return False
            if len(lines) < 48:
                logging.info(f"Rejecting {file_path}: log too short.")
                return False
            
            # Check if timestamps are positive and in ascending order
            prev_timestamp = float('-inf')
            all_zero_throughput = True # If every value is 0, never set to false -> return False
            
            for line in lines:
                parts = line.strip().split()
                if len(parts) != 2: # should only contain timestamp + throughput
                    logging.info(f"Rejecting {file_path}: Line does not have 2 parts")
                    return False
                
                try:
                    timestamp = float(parts[0])
                    throughput = float(parts[1])
                except ValueError:
                    logging.info(f"Rejecting {file_path}: Cannot convert to float")
                    return False
                
                if timestamp <= prev_timestamp: # ascending order
                    logging.info(f"Rejecting {file_path}: Timestamps not in ascending order")
                    return False
                prev_timestamp = timestamp
                
                if throughput != 0:
                    all_zero_throughput = False
            
            # Skipped if any nonzero throughput value
            if all_zero_throughput:
                logging.info(f"Rejecting {file_path}: All throughput values are zero")
                return False
            
            logging.info(f"Accepting {file_path}: File valid.")
            return True # file okay
    except Exception as e:
        logging.info(f"Rejecting {file_path}: Exception occurred - {str(e)}")
        return False

# Either clear directory contents or make directory if it doesn't exist
def clear_directory(directory):
    if os.path.exists(directory):
        for filename in os.listdir(directory):
            file_path = os.path.join(directory, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                logging.error(f'Failed to delete {file_path}. Reason: {e}')
    else:
        os.makedirs(directory)


def split_directory(input_dir, train_dir, test_dir, split_ratio=0.5, max_files=MAX_FILES):
    # Clear and recreate output directories
    clear_directory(train_dir)
    clear_directory(test_dir)

    # Get list of files in the input directory
    all_files = [f for f in os.listdir(input_dir) if os.path.isfile(os.path.join(input_dir, f))]
    print(f"Total files in input directory: {len(all_files)}")

    valid_files = []
    for f in all_files:
        full_path = os.path.join(input_dir, f)
        logging.info(f"Checking file: {full_path}")
        if is_valid_file(full_path):
            valid_files.append(f)
    print(f"Length of valid files = {len(valid_files)}")

    # Select a maximum of 300 random files if there are more than 300 valid files
    if len(valid_files) > max_files:
        valid_files = random.sample(valid_files, max_files)

    # Shuffle files to ensure random split
    random.shuffle(valid_files)

    # Calculate split index
    split_index = int(len(valid_files) * split_ratio)

    # Split files into training and testing sets
    train_files = valid_files[:split_index]
    test_files = valid_files[split_index:]

    # Copies files to the respective directories, don't want to move or lose files later
    for file in train_files:
        shutil.copy2(os.path.join(input_dir, file), os.path.join(train_dir, file))

    for file in test_files:
        shutil.copy2(os.path.join(input_dir, file), os.path.join(test_dir, file))

    print(f"Moved {len(train_files)} files to {train_dir}")
    print(f"Moved {len(test_files)} files to {test_dir}")
  
def main():
    # Check if a command-line argument is provided
    if len(sys.argv) > 1:
        trace_location = sys.argv[1]
        print(f"Using custom input directory: {trace_location}")
    else:
        print(f"Using default input directory: {INPUT_DIR}")
        trace_location = INPUT_DIR
        
    # Ensure the input directory exists
    if not os.path.isdir(trace_location):
        print(f"Error: The directory {trace_location} does not exist.")
        sys.exit(1)

    split_directory(trace_location, TRAIN_DIR, TEST_DIR)
    
if __name__ == '__main__':
    main()