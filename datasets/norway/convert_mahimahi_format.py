import os
import numpy as np

# Set paths relative to the script's directory
script_dir = os.path.dirname(os.path.abspath(__file__))
DATA_PATH = os.path.join(script_dir, 'traces')
OUTPUT_PATH = os.path.join(script_dir, 'processed')
os.makedirs(OUTPUT_PATH, exist_ok=True)
BYTES_PER_PKT = 1500.0
MILLISEC_IN_SEC = 1000.0
BITS_IN_BYTE = 8.0

def main():
    files = os.listdir(DATA_PATH)

    for f in files:
        file_path = os.path.join(DATA_PATH, f)
        output_path = os.path.join(OUTPUT_PATH, f)

        print("Current file: " + file_path)

        with open(file_path, 'r') as f, open(output_path, 'w') as mf:
            time_ms = []
            bytes_recv = []
            recv_time = []
            for line in f:
                parse = line.split()
                try:
                    time_ms_value = float(parse[0])
                    bytes_recv_value = float(parse[1])
                    recv_time_value = float(parse[2])
                except ValueError:
                    # Skip this line if it contains non-numeric values
                    continue

                if len(time_ms) > 0 and time_ms_value < time_ms[-1]:  # trace error, time not monotonically increasing
                    break
                time_ms.append(time_ms_value)
                bytes_recv.append(bytes_recv_value)
                recv_time.append(recv_time_value)

            time_ms = np.array(time_ms)
            bytes_recv = np.array(bytes_recv)
            recv_time = np.array(recv_time)
            throughput_all = bytes_recv / recv_time

            millisec_time = 0
            mf.write(str(millisec_time) + '\n')

            for i in range(len(throughput_all)):
                throughput = throughput_all[i]
                pkt_per_millisec = throughput / BYTES_PER_PKT 

                millisec_count = 0
                pkt_count = 0

                while True:
                    millisec_count += 1
                    millisec_time += 1
                    to_send = (millisec_count * pkt_per_millisec) - pkt_count
                    to_send = np.floor(to_send)

                    for j in range(int(to_send)):
                        mf.write(str(millisec_time) + '\n')

                    pkt_count += to_send

                    if millisec_count >= recv_time[i]:
                        break

if __name__ == '__main__':
    main()
