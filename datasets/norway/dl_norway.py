import requests
from bs4 import BeautifulSoup
import os

# Function to download files, added error handling instead of creating junk .log file
def download_file(url, filepath):
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an HTTPError for bad responses
        with open(filepath, 'wb') as f:
            f.write(response.content)
        print(f'Downloaded {filepath}')
    except requests.exceptions.HTTPError as err:
        print(f'HTTP error occurred: {err}')  # Print HTTP error messages
    except Exception as err:
        print(f'Other error occurred: {err}')  # Print other errors

# Base URL of the page
base_url = 'https://skulddata.cs.umass.edu/traces/mmsys/2013/pathbandwidth/'
# Directory to save the files
# save_directory = "/home/peter/abr-project/datasets/norway hsdpa/raw"
# Set paths relative to the script's directory
script_dir = os.path.dirname(os.path.abspath(__file__))
save_directory = os.path.join(script_dir, 'raw')
# Create the directory if it does not exist
os.makedirs(save_directory, exist_ok=True)

# Send a GET request to the base URL
response = requests.get(base_url)
response.raise_for_status()  # Ensure we notice bad responses

# Parse the HTML content
soup = BeautifulSoup(response.text, 'html.parser')
# Find all 'a' tags, which are hyperlinks
links = soup.find_all('a')

# List to store all logs directory links
logs_directory_links = []

# First loop to gather all logs directory links
for link in links:
    href = link.get('href')
    link_text = link.text.strip().upper()

    # Check if the link text contains 'LOGS'
    if 'LOGS' in link_text and href:
        logs_directory_link = base_url + href
        logs_directory_links.append(logs_directory_link)

# Iterate through each logs directory link to find .log files and download them
for logs_directory_link in logs_directory_links:
    # Send a GET request to the logs directory page
    logs_directory_response = requests.get(logs_directory_link)
    logs_directory_response.raise_for_status()
    
    # Parse the HTML content of the logs directory page
    logs_directory_soup = BeautifulSoup(logs_directory_response.text, 'html.parser')
    # Find all 'a' tags on the logs directory page
    log_links = logs_directory_soup.find_all('a')
    
    # Iterate through the found links to download .log files
    for log_link in log_links:
        log_href = log_link.get('href')
        if log_href and log_href.endswith('.log'):
            # Construct the full URL for the .log file
            log_file_url = os.path.join(logs_directory_link, log_href) #.replace('\\', '/') # .replace() only on windows
            # Construct the file path to save the .log file
            log_file_path = os.path.join(save_directory, log_href) #.replace('\\', '/') # .replace() only on windows
            # Download the .log file
            download_file(log_file_url, log_file_path)
