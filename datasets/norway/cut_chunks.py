import os
import numpy as np
import matplotlib.pyplot as plt
import shutil

#DATA_PATH = './mahimahi/'
#OUTPUT_PATH = './mahimahi_chunks/'
# Set paths relative to the script's directory
DATA_PATH = './cooked/'
OUTPUT_PATH = './chunks/'
BYTES_PER_PKT = 1500.0
MILLISEC_IN_SEC = 1000.0
BITS_IN_BYTE = 8.0

CHUNK_DURATION = 480.0  # duration in seconds
CHUNK_JUMP = 60.0  # shift in seconds


def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx


def main():
    # Clear the contents of the chunks directory
    if os.path.exists(OUTPUT_PATH):
        shutil.rmtree(OUTPUT_PATH)
    os.makedirs(OUTPUT_PATH)
    
    files = os.listdir(DATA_PATH)
    print(f"Found {len(files)} files in {DATA_PATH}")
    
    os.makedirs(OUTPUT_PATH, exist_ok=True)
    
    for file in files:
        file_path = os.path.join(DATA_PATH, file)
        output_path = os.path.join(OUTPUT_PATH, file)
        print(f"Processing file: {file_path}")
        
        timestamps = []
        throughput_data = []
        with open(file_path, 'r') as f:  # Changed to 'r' mode
            for line in f:
                parts = line.split()
                if len(parts) >= 2:
                    timestamps.append(float(parts[0]))  # Only append the timestamp
                    throughput_data.append(float(parts[1]))

        timestamps = np.array(timestamps)
        throughput_data = np.array(throughput_data)
        print(f"Read {len(timestamps)} data points")
        
        chunk = 0
        start_time = 0
        while True:
            end_time = start_time + CHUNK_DURATION
            if end_time > np.max(timestamps):
                break
            print(f"Processing chunk {chunk}: start_time = {start_time}")
            
            start_ptr = find_nearest(timestamps, start_time)
            end_ptr = find_nearest(timestamps, end_time)
            
            chunk_file_path = output_path + '_' + str(int(start_time))
            print(f"Writing chunk to {chunk_file_path}")
            with open(chunk_file_path, 'w') as f:
                for i in range(start_ptr, end_ptr + 1):
                    time_offset = timestamps[i] - timestamps[start_ptr]
                    f.write(f"{time_offset:.3f} {throughput_data[i]}\n")  # Write both timestamp and difference
            
            start_time += CHUNK_JUMP
            chunk += 1
        
        print(f"Finished processing {file}, created {chunk} chunks")

    print("Script execution completed")
            
if __name__ == '__main__':
	main()
