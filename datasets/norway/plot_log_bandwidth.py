""" 
Original pensieve code to process raw Norway HSDPA data
Made some modifications to work with py3 and work on local machine
Selects random file in directory instead of manually entering file name
@author: Hongzi Mao
"""
import os
import numpy as np
import random
import matplotlib.pyplot as plt


PACKET_SIZE = 1500.0  # bytes
TIME_INTERVAL = 5.0
BITS_IN_BYTE = 8.0
MBITS_IN_BITS = 1000000.0
MILLISECONDS_IN_SECONDS = 1000.0
N = 100

#LINK_FILE = './cooked_data/bus.ljansbakken-oslo-report.2010-09-28_1407CEST.log'
script_dir = os.path.dirname(os.path.abspath(__file__))
raw_dir = os.path.join(script_dir, 'raw')
# filename = 'raw/report.2010-09-28_1407CEST.log'
# LINK_FILE = os.path.join(script_dir, filename)

# List all .log files in the 'raw/' directory
log_files = [f for f in os.listdir(raw_dir) if f.endswith('.log')]

# Select a random .log file
if not log_files:
    raise FileNotFoundError("No .log files found in the 'raw/' directory.")
random_file = random.choice(log_files)
LINK_FILE = os.path.join(raw_dir, random_file)

print("FILE PATH: " + LINK_FILE)

with open(LINK_FILE, 'rb') as f:
    time_ms = []
    bytes_recv = []
    recv_time = []
    for line in f:
        parse = line.split()
        if len(parse) < 3:
            print(f"Skipping line: {parse}")
            continue
        time_ms.append(float(parse[0]))
        bytes_recv.append(float(parse[1]))
        recv_time.append(float(parse[2]))

time_ms = np.array(time_ms)
bytes_recv = np.array(bytes_recv)
recv_time = np.array(recv_time)
throughput_all = bytes_recv / recv_time

time_ms = time_ms - time_ms[0]
time_ms = time_ms / MILLISECONDS_IN_SECONDS
throughput_all = throughput_all * BITS_IN_BYTE / MBITS_IN_BITS * MILLISECONDS_IN_SECONDS

plt.plot(time_ms, throughput_all)
plt.xlabel('Time (second)')
plt.ylabel('Throughput (Mbit/sec)')
plt.title(f"Throughput for {random_file}")
plt.show()
