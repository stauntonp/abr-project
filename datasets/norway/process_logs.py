import os
import pandas as pd

INPUT_DIR = "./traces"
OUTPUT_DIR = "./cooked"

KB_IN_MB = BYTES_IN_KB = MILLISECONDS_IN_SECOND= 1000
BITS_IN_BYTE = 8
# MILLISECONDS_IN_SECOND = 1000

def convert_timestamp(start_time, current_time):
    return (float(current_time) - float(start_time)) / MILLISECONDS_IN_SECOND

def process_log_file(file_path, output_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        
        lines.sort() # one log has the descending times - causes crash for offline optimal

    start_time = float(lines[0].split()[1])/MILLISECONDS_IN_SECOND
    
    processed_lines = []
    for line in lines:
        columns = line.split()
        timestamp = abs(float(columns[1])/MILLISECONDS_IN_SECOND - start_time) # seconds since start of experiment
        column_5 = float(columns[4]) # bytes received since last
        column_6 = float(columns[5]) # milliseconds since last
        
        # milliseconds = convert_timestamp(start_time, timestamp)
        ratio = column_5 / column_6 * MILLISECONDS_IN_SECOND / BYTES_IN_KB / KB_IN_MB # Megabytes per second
        ratio = ratio * BITS_IN_BYTE # megabits per second
        
        processed_line = f"{timestamp:.3f}\t{ratio:.11f}\n"
        processed_lines.append(processed_line)
    
    with open(output_path, 'w') as output_file, open(output_path, 'w') as output_file:
        output_file.writelines(processed_lines)

def process_all_logs(input_dir, output_dir):    
    os.makedirs(output_dir, exist_ok=True) # Create output directory if it doesn't exist
    
    for filename in os.listdir(input_dir):
        if filename.endswith(".log"):
            input_path = os.path.join(input_dir, filename)
            output_path = os.path.join(output_dir, filename)
            process_log_file(input_path, output_path)
            print(f"Processed {filename}")

# Run the processing
process_all_logs(INPUT_DIR, OUTPUT_DIR)