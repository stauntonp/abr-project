import csv
import sys
import os
import glob

INPUT_PATH = '/home/peter/abr-project/datasets/lte_2018/pedestrian_cooked'
OUTPUT_PATH = './lte_logs/'

def csv_to_log(csv_file_path, log_file_path):
    os.makedirs(OUTPUT_PATH, exist_ok=True) # if not exist
    try:
        # Open the CSV file
        with open(csv_file_path, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file)
            # Get the headers
            headers = next(csv_reader)
            
            # Open the log file
            with open(log_file_path, mode='w') as log_file:
                # Write the headers to the log file
                # log_file.write(" | ".join(headers) + "\n")
                
                # Write the CSV data to the log file
                for row in csv_reader:
                    log_file.write("\t".join(row) + "\n")
                    
        print(f"CSV file '{csv_file_path}' has been successfully converted to log file '{log_file_path}'")
    
    except Exception as e:
        print(f"An error occurred: {e}")

def convert_all_csv(input_dir, output_dir):
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)
    
    # Find all CSV files in the input directory
    csv_files = glob.glob(os.path.join(input_dir, "*.csv"))
    
    for csv_file in csv_files:
        # Create corresponding log file path
        base_name = os.path.basename(csv_file)
        log_file_name = os.path.splitext(base_name)[0] + '.log'
        log_file_path = os.path.join(output_dir, log_file_name)
        
        # Convert CSV to log
        csv_to_log(csv_file, log_file_path)

def main():
    if len(sys.argv) == 1:  # No arguments, use default paths
        convert_all_csv(INPUT_PATH, OUTPUT_PATH)
    elif len(sys.argv) == 2:  # One argument, use provided CSV path and default log path
        input_dir = sys.argv[1]
        convert_all_csv(input_dir, OUTPUT_PATH)
    elif len(sys.argv) == 3:  # Two arguments, use provided paths
        input_dir = sys.argv[1]
        output_dir = sys.argv[2]
        convert_all_csv(input_dir, output_dir)
    else:  # Incorrect number of arguments
        print("Usage: python csv_to_log.py [<csv_file_path> <log_file_path>]")
        sys.exit(1)

if __name__ == "__main__":
    main()
