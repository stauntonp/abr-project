import os
import numpy as np
import heapq # selecting 50 highest average throughputs for Norway traces
import random # or select 50 random

DATA_PATH = './cooked_test_traces' 
OUTPUT_PATH = './mahimahi_sliced'
os.makedirs(OUTPUT_PATH, exist_ok=True)

BYTES_PER_PKT = 1500.0 # 1 MTU
BITS_IN_BYTE = 8
MILLISEC_IN_SEC = 1000.0
MEGA = 10**6 # 1 million bits = 1 megabit

GBR = 5 # Guaranteed bitrate (Mbps) for HD video streamining
# GBR = 2 # Guaranteed bitrate (Mbps) for SD video streaming (Should be 3-4 Mbps but most traces has throughput under 3)
MBR = 25 # Maximum bitrate (Mbps) for 4K Ultra HD video streaming
NUM_FILES = 50 # Number of output files to process

def megabits_to_bytes(megabits):
    return (megabits * MEGA) / BITS_IN_BYTE

# Clip throughput to the GBR-MBR range
# Most Norway traces have much lower throughput so lower clipping value required
def apply_gbr_mbr(throughput):
    return np.clip(throughput, GBR, MBR)

# Calculates average throughput in file to only select the top 50 averages for processing
# Lower averages will mostly be clipped upwards so that every log is identical
def calculate_average_throughput(file_path):
    with open(file_path, 'r') as f:
        throughputs = [float(line.strip().split()[1]) for line in f]
    return np.mean(throughputs)

# Reads traces processed for simulations [Timestamp(s), Throughput(Mbps)]
# Converts into mahimahi format: Each line contains the timestamp in milliseconds for 1x MTU packet to be consumed
def process_log(input_file, output_file):
    with open(input_file, 'r') as f, open(output_file, 'w') as mf:
        timestamps = []
        throughputs = []

        # Read and parse the log file
        for line in f:
            timestamp, throughput = map(float, line.strip().split())
            timestamps.append(timestamp)
            throughputs.append(throughput)

        # Convert to numpy arrays - better for processing
        timestamps = np.array(timestamps)
        throughputs = np.array(throughputs)
        # Apply GBR and MBR constraints for network slice
        throughputs = apply_gbr_mbr(throughputs)

        # Process each throughput value
        millisec_time = 0
        mf.write(str(millisec_time) + '\n')

        for i in range(len(throughputs)):     
            throughput = megabits_to_bytes(throughputs[i]) # Read megabit/s throughput and convert to byte/s
            pkt_per_millisec = throughput / (BYTES_PER_PKT * MILLISEC_IN_SEC) 
            millisec_count = 0
            pkt_count = 0

            while True:
                millisec_count += 1
                millisec_time += 1
                to_send = (millisec_count * pkt_per_millisec) - pkt_count
                to_send = np.floor(to_send)

                for _ in range(int(to_send)):
                    # Each line = Timestamp in milliseconds for 1 1500 Byte MTU
                    mf.write(str(millisec_time) + '\n')

                pkt_count += to_send

                # Use the difference between consecutive timestamps as recv_time
                if i < len(timestamps) - 1:
                    recv_time = timestamps[i+1] - timestamps[i]
                else:
                    recv_time = 1  # Assume 1 ms for the last entry

                if millisec_count >= recv_time * MILLISEC_IN_SEC:
                    break

def main():
    random.seed(42)
    files = os.listdir(DATA_PATH)
    print(f"The guaranteed bitrate is {GBR} mbps and the maximum bitrate is {MBR} for the network slice.")
    
    if GBR<=3:
        # Calculate average throughput for each file
        file_throughputs = []
        for f in files:
            input_file = os.path.join(DATA_PATH, f)
            avg_throughput = calculate_average_throughput(input_file)
            file_throughputs.append((-avg_throughput, f))  # Negative for max-heap behavior
        
        # Select top NUM_FILES with highest average throughput
        selected_files = [f for _, f in heapq.nsmallest(NUM_FILES, file_throughputs)]
    # No need to only take higher average traces when the GBR is higher (for 4G/LTE/broadband)
    # Almost every trace datapoint will already be above the GBR
    else:
        # Select NUM_FILES random traces from files
        selected_files = random.sample(files, min(NUM_FILES, len(files))) # min() used to allow <50 files
    
    # Process selection of files
    for f in selected_files:
        input_file = os.path.join(DATA_PATH, f)
        output_file = os.path.join(OUTPUT_PATH, f)
        print("Processing file: " + input_file)
        process_log(input_file, output_file)

if __name__ == '__main__':
    main()