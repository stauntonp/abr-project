import numpy as np
import datetime
import os

FILE_PATH = './curr_httpgetmt.csv'
OUTPUT_PATH = './all_traces/'
os.makedirs(OUTPUT_PATH, exist_ok=True)
NUM_LINES = np.inf
TIME_ORIGIN = datetime.datetime.utcfromtimestamp(0)

BYTES_IN_MB = 1000000

bw_measurements = {}
line_indices = {}

def main():
    line_counter = 0
    with open(FILE_PATH, 'r') as f:
        for idx, line in enumerate(f):
            if idx == 0:  # Skip the header line
                continue
            parse = line.split(',')
            
            if len(parse) < 7:  # Ensure there are enough elements
                print(f"Skipping malformed line {line_counter + 1}: {line.strip()}")
                continue

            uid = parse[0]
            # dtime = (datetime.datetime.strptime(parse[1],'%Y-%m-%d %H:%M:%S') 
            #     - TIME_ORIGIN).total_seconds()
            target = parse[2]
            address = parse[3]
            throughput = float(parse[6]) / BYTES_IN_MB  # bytes per second
            # throughput = float(parse[6])/1000 # bytes per millisecond: same format as other traces

            k = (uid, target)
            
            if k not in line_indices:
                line_indices[k] = 1  # Start indexing from 1

            relative_time = line_indices[k]
            line_indices[k] += 1  # Increment the index for the next line
            
            if k in bw_measurements:
                bw_measurements[k].append((relative_time, throughput))
            else:
                bw_measurements[k] = [(relative_time, throughput)]

            line_counter += 1
            if line_counter >= NUM_LINES:
                break

    for k in bw_measurements:
        out_file = 'trace_' + '_'.join(k)
        out_file = out_file.replace(':', '-')
        out_file = out_file.replace('/', '-')
        out_file = OUTPUT_PATH + out_file
        with open(out_file, 'w') as f:
            # for i in bw_measurements[k]:
                # f.write(str(i) + '\n')
            for timestamp, throughput in bw_measurements[k]:
                f.write(f"{timestamp}\t{throughput}\n")

if __name__ == '__main__':
    main()

