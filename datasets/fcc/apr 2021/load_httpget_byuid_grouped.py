import pandas as pd
import numpy as np
import os
import shutil

FILE_PATH = './curr_httpget.csv'
OUTPUT_PATH = './cooked/'
os.makedirs(OUTPUT_PATH, exist_ok=True)
NUM_LINES = np.inf
BYTES_IN_MB = 1000000

def clear_output_directory(directory):
    if os.path.exists(directory):
        shutil.rmtree(directory)
    os.makedirs(directory)
    
def calculate_average_throughput(data):
    throughputs = [float(line.strip()) for line in data]
    return np.mean(throughputs)

def get_trace_length(file_path):
    with open(file_path, 'r') as f:
        return len(f.readlines())

def group_traces(sorted_traces, target_length=360):
    groups = []
    current_group = []
    current_length = 0

    for trace in sorted_traces:
        trace_length = len(trace['data'])
        current_group.append(trace)
        current_length += trace_length

        # If we've reached or exceeded the target length, finalize this group
        if current_length >= target_length:
            groups.append(current_group)
            current_group = []
            current_length = 0

    # Add any remaining traces to the last group
    if current_group:
        groups.append(current_group)

    return groups
    
def main():
    # Clear and recreate the output directory
    clear_output_directory(OUTPUT_PATH)
    # Read the CSV file, specifying data types and handling errors
    df = pd.read_csv(FILE_PATH,
                     nrows=NUM_LINES if NUM_LINES != np.inf else None,
                     dtype={
                         'unit_id': str,
                         'dtime': str,
                         'target': str,
                         'address': str,
                         'bytes_sec': float
                     },
                     on_bad_lines='warn')

    # Clean the 'bytes_sec' column if it contains commas
    df['bytes_sec'] = df['bytes_sec'].replace({',': ''}, regex=True).astype(float)
    df['throughput'] = df['bytes_sec'] / BYTES_IN_MB  # Convert bytes_sec to MB/s

    # Convert dtime to datetime
    df['dtime'] = pd.to_datetime(df['dtime'], format='%Y-%m-%d %H:%M:%S')

    # Group by unit_id
    grouped = df.groupby('unit_id')

    trace_info = []
    for unit_id, group in grouped:
        # Calculate seconds since the start of the first record for this unit_id
        start_time = group['dtime'].min()
        group['seconds'] = ((group['dtime'] - start_time).dt.total_seconds()).astype(int)
        group = group.sort_values('seconds')

        # Aggregate throughput when there are multiple rows with the same timestamp
        aggregated = group.groupby('seconds')['throughput'].sum().reset_index()

        # Create trace data
        trace_data = [f"{row['throughput']:.6f}\n" for _, row in aggregated.iterrows()]
        
        # Calculate average throughput
        avg_throughput = calculate_average_throughput(trace_data)

        # Only include traces with non-zero average throughput
        # if avg_throughput > 0:
        # Appending all traces then discarding top/bottom 15% outliers - overall performance *should* improve at expense of worse performance on outlier traces
        trace_info.append({
            'unit_id': unit_id, 
            'data': trace_data, 
            'avg_throughput': avg_throughput,
        })

    # Sort traces by average throughput
    sorted_traces = sorted(trace_info, key=lambda x: x['avg_throughput'], reverse=True)
    # Calculate the number of traces to discard (15% from each end)
    num_traces = len(sorted_traces)
    num_discard = int(num_traces * 0.15)

    # Select only the middle 70% of traces
    selected_traces = sorted_traces[num_discard:-num_discard]
    # Group the selected traces
    grouped_traces = group_traces(selected_traces)

    # Write grouped traces to log files
    for i, group in enumerate(grouped_traces):
        out_file = os.path.join(OUTPUT_PATH, f'group_{i+1}.log')
        with open(out_file, 'w') as f:
            index = 0
            for trace in group:
                for line in trace['data']:
                    f.write(f"{index}\t{line}")
                    index += 1
        # Print group info
        print(f"Group {i+1}:")
        total_length = sum(len(trace['data']) for trace in group)
        print(f"  Total length: {total_length}")
        for trace in group:
            print(f"  - unit_id: {trace['unit_id']}, Avg Throughput = {trace['avg_throughput']:.6f}")
        print()

if __name__ == '__main__':
    main()