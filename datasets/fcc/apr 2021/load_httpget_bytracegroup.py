import pandas as pd
import numpy as np
import os
import shutil

FILE_PATH = './curr_httpget.csv'
OUTPUT_PATH = './cooked/'
NUM_LINES = np.inf
BYTES_IN_MB = 1000000
GROUP_SIZE = 4

def clear_output_directory(directory):
    if os.path.exists(directory):
        shutil.rmtree(directory)
    os.makedirs(directory)

def main():
    # Clear and recreate the output directory
    clear_output_directory(OUTPUT_PATH)

    # Read the CSV file, specifying data types and handling errors
    df = pd.read_csv(FILE_PATH,
                     nrows=NUM_LINES if NUM_LINES != np.inf else None,
                     dtype={
                         'unit_id': str,
                         'dtime': str,
                         'target': str,
                         'address': str,
                         'bytes_sec': float
                     },
                     on_bad_lines='warn')

    # Clean the 'bytes_sec' column if it contains commas
    df['bytes_sec'] = df['bytes_sec'].replace({',': ''}, regex=True).astype(float)
    df['throughput'] = df['bytes_sec'] / BYTES_IN_MB  # Convert bytes_sec to MB/s

    # Convert dtime to datetime
    df['dtime'] = pd.to_datetime(df['dtime'], format='%Y-%m-%d %H:%M:%S')

    # Sort unit_ids and group them
    sorted_unit_ids = sorted(df['unit_id'].unique())
    grouped_unit_ids = [sorted_unit_ids[i:i+GROUP_SIZE] for i in range(0, len(sorted_unit_ids), GROUP_SIZE)]

    for group_index, unit_id_group in enumerate(grouped_unit_ids):
        group = df[df['unit_id'].isin(unit_id_group)].copy()  # Create a copy here - not modifying original dataframe

        # Create a unique filename for each group
        out_file = f'trace_group_{group_index+1}.log'
        out_file = os.path.join(OUTPUT_PATH, out_file)

        # Calculate seconds since the start of the first record for this group
        start_time = group['dtime'].min()
        group['seconds'] = ((group['dtime'] - start_time).dt.total_seconds()).astype(int)
        group = group.sort_values('seconds')

        # Aggregate throughput when there are multiple rows with the same timestamp
        aggregated = group.groupby('seconds')['throughput'].sum().reset_index()

        # Write to file
        with open(out_file, 'w') as f:
            for _, row in aggregated.iterrows():
                f.write(f"{row['seconds']}\t{row['throughput']:.6f}\n")

if __name__ == '__main__':
    main()