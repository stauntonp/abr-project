import pandas as pd
import numpy as np
import os
import shutil

FILE_PATH = './curr_httpgetmt.csv'
OUTPUT_PATH = './cooked/'
os.makedirs(OUTPUT_PATH, exist_ok=True)
NUM_LINES = np.inf
BYTES_IN_MB = 1000000

def clear_output_directory(directory):
    if os.path.exists(directory):
        shutil.rmtree(directory)
    os.makedirs(directory)

def main():
    # Clear and recreate the output directory
    clear_output_directory(OUTPUT_PATH)
    # Read the CSV file, specifying data types and handling errors
    # Pandas instead of csv module as multiple ips inside same entry breaks csv library
    df = pd.read_csv(FILE_PATH, 
                     nrows=NUM_LINES if NUM_LINES != np.inf else None,
                     dtype={
                         'unit_id': str,
                         'dtime': str,
                         'target': str,
                         'address': str,
                         'bytes_sec': float
                     },
                     on_bad_lines='warn')  # This replaces error_bad_lines and warn_bad_lines
    
    # Clean the 'bytes_sec' column if it contains commas
    df['bytes_sec'] = df['bytes_sec'].replace({',': ''}, regex=True).astype(float)
    df['throughput'] = df['bytes_sec'] / BYTES_IN_MB # Convert bytes_sec to MB/s
    
    # Group by uid only, each trace gets 1 log instead of 1 per website
    # grouped = df.groupby('unit_id')
    
    # Convert dtime to datetime
    df['dtime'] = pd.to_datetime(df['dtime'], format='%Y-%m-%d %H:%M:%S')
    df['hour'] = df['dtime'].dt.floor('H') # creates an hour column - will split logs up by all recorded traces per h on each day
    grouped = df.groupby('hour')
    
    for hour, group in grouped:
        # Create a unique filename for each hour
        out_file = f'trace_{hour.strftime("%Y-%m-%d_%H")}H.log'
        out_file = out_file.replace(':', '-').replace('/', '-')
        out_file = os.path.join(OUTPUT_PATH, out_file)

        # Calculate seconds since start of the hour (as integers)
        group['seconds'] = ((group['dtime'] - hour).dt.total_seconds()).astype(int)
        group = group.sort_values('seconds')
        
        # Return sum of throughputs to different targets when there are multiple rows with the same timestamp
        aggregated = group.groupby('seconds')['throughput'].sum().reset_index()

        # Write to file
        with open(out_file, 'w') as f:
            for _, row in aggregated.iterrows():
                f.write(f"{row['seconds']}\t{row['throughput']:.6f}\n")

if __name__ == '__main__':
    main()