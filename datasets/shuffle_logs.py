import os
import random

OUTPUT_DIR = './synthetic_logs'

def read_files_in_directory(directory):
    files_data = []
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)
        if os.path.isfile(filepath):
            with open(filepath, 'r') as file:
                for line in file:
                    parts = line.split()
                    if len(parts) == 2:
                        try:
                            value = float(parts[1])
                            files_data.append(value)
                        except ValueError:
                            pass
    return files_data

def chunk_data(data, chunk_size=5):
    chunks = []
    for i in range(0, len(data), chunk_size):
        chunk = data[i:i + chunk_size]
        if len(chunk) < chunk_size:
            while len(chunk) < chunk_size:
                chunk.append(random.choice(data))
        chunks.append(chunk)
    return chunks

def save_unshuffled_chunks(chunks, filename='shuffle.log'):
    with open(filename, 'w') as file:
        for chunk in chunks:
            file.write("\t".join(map(str, chunk)) + "\n")

def shuffle_and_create_logs(chunks, output_directory=OUTPUT_DIR, chunks_per_log=30):
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    
    random.shuffle(chunks)
    
    log_count = 1
    for i in range(0, len(chunks), chunks_per_log):
        log_chunks = chunks[i:i + chunks_per_log]
        if len(log_chunks) < chunks_per_log:
            break
        output_filepath = os.path.join(output_directory, f'synthetic_trace_{log_count}.txt')
        with open(output_filepath, 'w') as output_file:
            index = 1
            for chunk in log_chunks:
                for value in chunk:
                    # output_file.write(f'{index}\t{value}\n')
                    # Adding noise to make data separate but similar to original
                    noise = value * 0.05
                    noisy_value = value + random.uniform(-noise, noise)
                    output_file.write(f'{index}\t{noisy_value}\n')
                    index += 1
        log_count += 1

def main(directory):
    os.makedirs(OUTPUT_DIR, exist_ok=True)
    
    data = read_files_in_directory(directory) 
    chunks = chunk_data(data) # All original data is extracted into chunks/segments of 5 timestamps
    save_unshuffled_chunks(chunks, 'shuffle.log') # Output for debugging
    shuffle_and_create_logs(chunks) # shuffle chunks, add noise then create new log files of 30-chunk length

### Input dataset: LTE_2018 and 2x 4G Belgium
if __name__ == "__main__":
    directory = './all_logs/' 
    main(directory)
