import os

def convert_bytes_to_megabytes(file_path):
    # Read the content of the file
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    # Process each line to convert bytes to megabytes
    converted_lines = []
    for line in lines:
        parts = line.split()
        if len(parts) == 2:
            first_num = parts[0]
            second_num = float(parts[1]) / 1000000  # Convert bytes to megabytes
            converted_lines.append(f"{first_num}\t{second_num:.6f}\n")
    
    # Write the converted content back to the file or to a new file
    with open(file_path, 'w') as file:
        file.writelines(converted_lines)
    
    print(f"Processed file: {file_path}")

def process_directory(directory_path):
    # List all files in the directory
    files = [f for f in os.listdir(directory_path) if os.path.isfile(os.path.join(directory_path, f))]
    
    # Process each file
    for file_name in files:
        file_path = os.path.join(directory_path, file_name)
        convert_bytes_to_megabytes(file_path)

# Directory containing the log files
log_directory = './cooked_traces/'

# Process all log files in the directory
process_directory(log_directory)
