import os
import random
import shutil

PROCESSED_DIR = './cooked'
TRAIN_DIR = './cooked_traces'
TEST_DIR = './cooked_test_traces'

def split_data(processed_dir, train_dir, test_dir, train_ratio=0.6):
    # Create output directories if they don't exist
    os.makedirs(train_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)

    # Get list of all processed log files
    files = [f for f in os.listdir(processed_dir) if f.endswith('.log')]

    # Shuffle the files
    random.shuffle(files)

    # Calculate the split point
    split_point = int(len(files) * train_ratio)

    # Split the files into training and testing sets
    train_files = files[:split_point]
    test_files = files[split_point:]

    # Move files to respective directories
    for file in train_files:
        shutil.move(os.path.join(processed_dir, file), os.path.join(train_dir, file))
        print(f"Moved {file} to {train_dir}")

    for file in test_files:
        shutil.move(os.path.join(processed_dir, file), os.path.join(test_dir, file))
        print(f"Moved {file} to {test_dir}")

# Run the data splitting
split_data(PROCESSED_DIR, TRAIN_DIR, TEST_DIR)
