import os
import sys

sys.path.append('../')
from create_splits import split_directory

INPUT_DIR = "./processed"
TRAIN_DIR = "./cooked_traces"
TEST_DIR = "./cooked_test_traces"

# Runs create_splits on {subdirectory}
def process_subdirectory(subdirectory):
    subdirectory_path = os.path.join(INPUT_DIR, subdirectory)
    # train_subdir = os.path.join(TRAIN_DIR, subdirectory)
    # test_subdir = os.path.join(TEST_DIR, subdirectory)
    
    try:
        split_directory(subdirectory_path, TRAIN_DIR, TEST_DIR)
        print(f"Successfully processed {subdirectory}")
    except Exception as e:
        print(f"Error processing {subdirectory}: {str(e)}")

# Process each subdirectory in the input directory.
def process_subdirectories(input_dir):
    for item in os.listdir(input_dir):
        item_path = os.path.join(input_dir, item)
        if os.path.isdir(item_path):
            print(f"Processing subdirectory: {item}")
            process_subdirectory(item)

def main():
    """Main function to process all subdirectories."""
    if not os.path.exists(INPUT_DIR):
        print(f"Error: Input directory {INPUT_DIR} does not exist.")
        return
    # Create train and test directories if they don't exist
    os.makedirs(TRAIN_DIR, exist_ok=True)
    os.makedirs(TEST_DIR, exist_ok=True)
    
    process_subdirectories(INPUT_DIR)

if __name__ == "__main__":
    main()