import os
from datetime import datetime
from typing import List

import pandas as pd

INPUT_DIR = "./archive/Dataset"
OUTPUT_DIR = "./processed"

KB_IN_MB = 1000
BITS_IN_BYTE = 8
BITS_IN_MB = 1000000
CSV_EXTENSION = ".csv"
LOG_EXTENSION = ".log"

# Convert timestamp to seconds since start time.
def convert_timestamp_to_seconds(start_time: datetime, timestamp: str) -> float:
    dt = datetime.strptime(timestamp, "%Y.%m.%d_%H.%M.%S")
    return (dt - start_time).total_seconds() + 1

# Process a single CSV file and write the output to a log file.
def process_csv_file(file_path: str, output_path: str) -> None:
    try:
        df = pd.read_csv(file_path)
        start_time = datetime.strptime(df['Timestamp'].iloc[0], "%Y.%m.%d_%H.%M.%S")
        df['timestamp_seconds'] = df['Timestamp'].apply(lambda x: convert_timestamp_to_seconds(start_time, x))
        df['DL_bitrate_bytes_per_s'] = df['DL_bitrate'] / KB_IN_MB  # Convert kbps to megabits/s
        df_aggregated = df.groupby('timestamp_seconds')['DL_bitrate_bytes_per_s'].mean().reset_index()
        
        processed_lines: List[str] = [
            f"{row['timestamp_seconds']}\t{row['DL_bitrate_bytes_per_s']}\n"
            for _, row in df_aggregated.iterrows()
        ]
        
        with open(output_path, 'w') as output_file:
            output_file.writelines(processed_lines)
    except Exception as e:
        print(f"Error processing {file_path}: {str(e)}")

# Process all CSV files in the input directory and its subdirectories.
def process_all_logs(input_dir: str, output_dir: str) -> None:
    os.makedirs(output_dir, exist_ok=True)
    for root, _, files in os.walk(input_dir):
        for filename in files:
            if filename.endswith(CSV_EXTENSION):
                input_path = os.path.join(root, filename)
                relative_path = os.path.relpath(root, input_dir)
                subdirectory = os.path.basename(relative_path)
                new_subdir = os.path.join(output_dir, subdirectory)
                os.makedirs(new_subdir, exist_ok=True) # Make subdirectories for each - shuffle/create_splits resulted in poor distribution of datasets
                
                # Output files as .log for consistency with other datasets
                if subdirectory == os.path.basename(input_dir):
                    output_filename = filename.replace(CSV_EXTENSION, LOG_EXTENSION)
                else:
                    output_filename = f"{subdirectory}_{filename.replace(CSV_EXTENSION, LOG_EXTENSION)}"
                
                output_path = os.path.join(output_dir, subdirectory, output_filename)
                process_csv_file(input_path, output_path)
                print(f"Processed {filename} from {subdirectory}")

def main():
    process_all_logs(INPUT_DIR, OUTPUT_DIR)
    
if __name__ == '__main__':
    main()
