import os
import numpy as np

DATA_PATH = './cooked_test_traces'
OUTPUT_PATH = './mahimahi'
os.makedirs(OUTPUT_PATH, exist_ok=True)

BYTES_PER_PKT = 1500.0 # 1 MTU
BITS_IN_BYTE = 8
MILLISEC_IN_SEC = 1000.0
MEGA = 10**6 # 1 million bits = 1 megabit

def megabits_to_bytes(megabits):
    return (megabits * MEGA) / BITS_IN_BYTE

# Reads traces processed for simulations [Timestamp(s), Throughput(Mbps)]
# Converts into mahimahi format: Each line contains the timestamp in milliseconds for 1x MTU packet to be consumed
def process_log(input_file, output_file):
    with open(input_file, 'r') as f, open(output_file, 'w') as mf:
        timestamps = []
        throughputs = []

        # Read and parse the log file
        for line in f:
            timestamp, throughput = map(float, line.strip().split())
            timestamps.append(timestamp)
            throughputs.append(throughput)

        # Convert to numpy arrays - better for processing
        timestamps = np.array(timestamps)
        throughputs = np.array(throughputs)

        # Process each throughput value
        millisec_time = 0
        mf.write(str(millisec_time) + '\n')

        for i in range(len(throughputs)):     
            throughput = megabits_to_bytes(throughputs[i]) # Read megabit/s throughput and convert to byte/s
            pkt_per_millisec = throughput / (BYTES_PER_PKT * MILLISEC_IN_SEC) 
            millisec_count = 0
            pkt_count = 0

            while True:
                millisec_count += 1
                millisec_time += 1
                to_send = (millisec_count * pkt_per_millisec) - pkt_count
                to_send = np.floor(to_send)

                for _ in range(int(to_send)):
                    # Each line = Timestamp in milliseconds for 1 1500 Byte MTU
                    mf.write(str(millisec_time) + '\n')

                pkt_count += to_send

                # Use the difference between consecutive timestamps as recv_time
                if i < len(timestamps) - 1:
                    recv_time = timestamps[i+1] - timestamps[i]
                else:
                    recv_time = 1  # Assume 1 ms for the last entry

                if millisec_count >= recv_time * MILLISEC_IN_SEC:
                    break

def main():
    files = os.listdir(DATA_PATH)
    for f in files:
        input_file = os.path.join(DATA_PATH, f)
        output_file = os.path.join(OUTPUT_PATH, f)
        print("Processing file: " + input_file)
        process_log(input_file, output_file)

if __name__ == '__main__':
    main()