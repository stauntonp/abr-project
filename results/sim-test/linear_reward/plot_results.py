import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import numpy as np

# Input data as a string
input_data = """
Offline Optimal	51.45	198.55	198.55	177.62	1096.88	4226.04	4226.04	3784.24
Buffer-based	39.55	187.65	186.2	161.36	1059.38	4017.71	3987.5	3569.05
MPC	37.53	198.55	198.55	173.62	1096.88	4226.04	4226.04	3780.42
Oboe MPC	37.55	196.1	198.55	173.62	1012.5	4226.04	4175	3696.83
Pensieve (Max)	-75.32	198.55	198.55	171.86	1190.62	4226.04	4226.04	3758.78
Transfer (25k)	-69.81	198.55	198.55	170.61	1069.79	4226.04	4226.04	3739.87
Transfer (50k)	-60.91	198.55	198.55	169.65	1171.88	4226.04	4226.04	3782.35
Transfer (Max)	-19.52	198.55	198.55	173.72	1011.46	4226.04	4226.04	3751.04
"""
# Used to contract title of chart
dataset = "FCC httpget Broadband"
# dataset = "Belgium/Cork 4G/LTE"
# dataset = "Norway HSDPA"

def process_input_data(input_string):
    lines = input_string.strip().split('\n')
    processed_data = []
    algorithms = []
    for line in lines:
        try:
            # Split by tabs
            parts = line.split('\t')
            algorithms.append(parts[0])
            values = [float(val) for val in parts[1:]]
            if len(values) == 8:  # Ensure correct number of values
                processed_data.append(values)
            else:
                print(f"Warning: Skipping invalid line (incorrect number of values): {line}")
        except ValueError:
            print(f"Warning: Skipping invalid line (non-numeric value): {line}")
    return processed_data, algorithms

def create_bar_chart(ax, data, title, ylabel, algorithms):
    x = np.arange(len(algorithms))
    width = 0.35

    medians = data[:, 0]
    means = data[:, 1]

    # Set zorder for bars directly in the bar() function
    ax.bar(x - width/2, medians, width, label='Median', color=color_median, zorder=3)
    ax.bar(x + width/2, means, width, label='Mean', color=color_mean, zorder=3)

    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.set_xticks(x)
    ax.set_xticklabels(algorithms, rotation=45, ha='right')
    ax.legend()

    # Add major gridlines
    ax.grid(axis='y', linestyle='-', alpha=0.2, zorder=0)
    
    # Add minor gridlines
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.grid(axis='y', which='minor', linestyle=':', alpha=0.2, zorder=0)

    # Remove top and right spines
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # Ensure ticks are only on the left and bottom
    ax.tick_params(axis='y', which='both', left=True, right=False)
    ax.tick_params(axis='x', which='both', bottom=True, top=False)

# Define colors
color_median = '#87CEEB'  # Sky Blue
color_mean = '#FF7F50'    # Coral

# Process input data
data, algorithms = process_input_data(input_data)

if len(data) != len(algorithms):
    print(f"Error: Mismatch between data rows ({len(data)}) and algorithms ({len(algorithms)}). Please check your input data.")
else:
    # Prepare data for plotting
    reward_data = np.array([[d[2], d[3]] for d in data])  # median, mean
    bitrate_data = np.array([[d[6], d[7]] for d in data])  # median, mean

    # Create the plot
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 8))

    create_bar_chart(ax1, reward_data, 'Reward Comparison', 'Reward', algorithms)
    create_bar_chart(ax2, bitrate_data, 'Bitrate Comparison', 'Bitrate (kbps)', algorithms)

    # Add a combined title for the entire figure
    fig.suptitle(f"Simulation Test Results with {dataset} Dataset: QoE-linear reward", fontsize=16)

    plt.tight_layout()
    plt.subplots_adjust(top=0.93)  # Adjust the top margin to make room for the suptitle
    plt.show()

print("\nInput Data Format:")
print("Each line should contain: Algorithm_Name<tab>min_reward<tab>max_reward<tab>median_reward<tab>mean_reward<tab>min_bitrate<tab>max_bitrate<tab>median_bitrate<tab>mean_bitrate")
print("with one line for each algorithm")