import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import numpy as np

def process_input_data(input_string):
    lines = input_string.strip().split('\n')
    processed_data = []
    algorithms = []
    for line in lines:
        parts = line.split('\t')
        algorithms.append(' '.join(parts[:-8]))
        values = [float(val) for val in parts[-8:]]
        processed_data.append(values)
    return np.array(processed_data), algorithms


def set_axis_limit(ax, values):
    min_val = min(values)
    max_val = max(values)
    if max_val > 100:
        ax.set_ylim(bottom=min_val * 0.5)
    else:
        ax.set_ylim(bottom=0)
    ax.set_ylim(top=max_val * 1.1)  # Add 10% padding at the top

def create_dual_axis_chart(ax, data, title, algorithms):
    x = np.arange(len(algorithms))
    width = 0.35

    mean_rewards = data[:, 3]
    mean_bitrates = data[:, 7]

    ax1 = ax
    ax2 = ax1.twinx()

    rects1 = ax1.bar(x - width/2, mean_rewards, width, label='Mean Reward', color='#87CEEB', zorder=3)
    rects2 = ax2.bar(x + width/2, mean_bitrates, width, label='Mean Bitrate', color='#FF7F50', zorder=3)

    ax1.set_ylabel('Reward (QoE-linear)')
    ax2.set_ylabel('Bitrate (kbps)')
    ax1.set_title(title)
    ax1.set_xticks(x)
    ax1.set_xticklabels(algorithms, rotation=45, ha='right')

    set_axis_limit(ax1, mean_rewards)
    set_axis_limit(ax2, mean_bitrates)

    ax1.tick_params(axis='y')
    ax2.tick_params(axis='y')

    ax1.grid(axis='y', linestyle='-', alpha=0.2, zorder=0)
    ax2.grid(axis='y', linestyle='--', alpha=0.2, zorder=0)

    ax1.spines['top'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax2.spines['left'].set_visible(False)

    return rects1, rects2

def plot_multi_dataset_comparison(datasets):
    fig, axs = plt.subplots(1, 3, figsize=(24, 5))
    
    for i, (dataset, input_data) in enumerate(datasets.items()):
        data, algorithms = process_input_data(input_data)
        
        rects1, rects2 = create_dual_axis_chart(axs[i], data, dataset, algorithms)
    
    # Add a single legend for the entire figure
    fig.legend((rects1, rects2), ('Mean Reward', 'Mean Bitrate'), loc='upper right', bbox_to_anchor=(0.95, 1), ncol=2)
    
    plt.tight_layout()
    plt.subplots_adjust(top=0.85, right=0.95, bottom=0.2)
    plt.show()

# Define colors
color_median = '#87CEEB'  # Sky Blue
color_mean = '#FF7F50'    # Coral

# Input data as a string

dataset1 = {
    "FCC httpget Broadband": """
Optimal	51.45	198.55	198.55	177.62	1096.88	4226.04	4226.04	3784.24
BB	39.55	187.65	186.2	161.36	1059.38	4017.71	3987.5	3569.05
MPC	37.53	198.55	198.55	173.62	1096.88	4226.04	4226.04	3780.42
Oboe-MPC	37.55	196.1	198.55	173.62	1012.5	4226.04	4175	3696.83
RL (Max)	-75.32	198.55	198.55	171.86	1190.62	4226.04	4226.04	3758.78
TL (25k)	-69.81	198.55	198.55	170.61	1069.79	4226.04	4226.04	3739.87
TL (50k)	-60.91	198.55	198.55	169.65	1171.88	4226.04	4226.04	3782.35
TL (Max)	-19.52	198.55	198.55	173.72	1011.46	4226.04	4226.04	3751.04

    """
}
dataset2 = {
    "Belgium/Cork 4G/LTE": """
Optimal	41.35	198.55	198.55	184.21	300	4226.04	4226.04	3900.59
BB	-182.36	187.65	186.2	162.2	618.75	4017.71	3987.5	3684.87
MPC	-56.82	198.55	198.55	179.57	628.12	4226.04	4226.04	3890.59
Oboe-MPC	-234.51	198.55	198.55	167.95	571.88	4226.04	4226.04	3798.99
RL	-134.09	198.55	194.2	168.23	535.42	4226.04	4170.31	3727.79
TL (25k)	7.96	198.55	195.45	169.09	542.71	4226.04	4161.46	3720.95
TL (50k)	-127.72	198.55	196.1	171.21	547.92	4226.04	4185.42	3778.26
TL (Max)	-111.06	198.55	195.45	170.58	538.54	4226.04	4195.83	3810.11
    """
}
dataset3 = {
    "Norway 3G/HSDPA": """
Offline Optimal	20.85	198.55	46.96	55.95	468.75	4226.04	1009.9	1212.95
BB	-52.23	175.4	23.79	30.04	459.38	3883.33	959.9	1132.59
MPC	2.65	187.77	35.68	43.94	450	4195.83	986.46	1181.17
Oboe-MPC	-124.51	187.3	33.67	41.41	393.75	3991.67	920.31	1092
RL	-52.39	182.75	33.92	41.34	365.62	3896.88	840.64	1017.94
TL (25k)	-160.86	163.7	26.32	31.75	421.88	3545.83	869.79	1044.08
TL (50k)	-8.66	112.65	28.34	36.78	337.5	2443.75	844.27	995.11
TL (Max)	-79.75	183.65	32.65	38.16	337.5	3934.38	907.81	1070.86
    """
}

# Combine all datasets
all_datasets = {**dataset1, **dataset2, **dataset3}

# Plot the multi-dataset comparison
plot_multi_dataset_comparison(all_datasets)

print("\nInput Data Format:")
print("Each line should contain: Algorithm_Name<tab>min_reward<tab>max_reward<tab>median_reward<tab>mean_reward<tab>min_bitrate<tab>max_bitrate<tab>median_bitrate<tab>mean_bitrate")
print("with one line for each algorithm")