import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import numpy as np

def process_input_data(input_string):
    lines = input_string.strip().split('\n')
    processed_data = []
    algorithms = []
    for line in lines:
        try:
            # Split by tabs
            parts = line.split('\t')
            algorithms.append(' '.join(parts[:-8])) # can handle algorithm names containing spaces
            values = [float(val) for val in parts[1:]]
            if len(values) == 8:  # Ensure correct number of values
                processed_data.append(values)
            else:
                print(f"Warning: Skipping invalid line (incorrect number of values): {line}")
        except ValueError:
            print(f"Warning: Skipping invalid line (non-numeric value): {line}")
    return processed_data, algorithms

# Creates single bar chart
def create_bar_chart(ax, data, title, ylabel, algorithms):
    x = np.arange(len(algorithms))
    width = 0.35

    medians = data[:, 0]
    means = data[:, 1]

    rects1 = ax.bar(x - width/2, medians, width, label='Median', color=color_median, zorder=3)
    rects2 = ax.bar(x + width/2, means, width, label='Mean', color=color_mean, zorder=3)

    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.set_xticks(x)
    ax.set_xticklabels(algorithms, rotation=45, ha='right')

    # Set y-axis limits to focus on the data range
    y_min = min(min(medians), min(means))
    y_max = max(max(medians), max(means))
    y_range = y_max - y_min
    # ax.set_ylim(y_min - 0.1 * y_range, y_max + 0.1 * y_range)
    ax.set_ylim(y_min - 0.5 * y_range, y_max + 0.1 * y_range)

    ax.grid(axis='y', linestyle='-', alpha=0.2, zorder=0)
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.grid(axis='y', which='minor', linestyle=':', alpha=0.2, zorder=0)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.tick_params(axis='y', which='both', left=True, right=False)
    ax.tick_params(axis='x', which='both', bottom=True, top=False)
    
    return rects1, rects2

# Stack all bar charts into single image as 2x3 grid of plots
# Stack all bar charts into single image as 2x3 grid of plots
def plot_multi_dataset_comparison(datasets):
    fig, axs = plt.subplots(2, 3, figsize=(30, 16))
    # fig.suptitle("Simulation Test Results: QoE-linear reward", fontsize=16)
    
    for i, (dataset, input_data) in enumerate(datasets.items()):
        data, algorithms = process_input_data(input_data)
        
        if len(data) != len(algorithms):
            print(f"Error: Mismatch between data rows ({len(data)}) and algorithms ({len(algorithms)}) for {dataset}. Please check your input data.")
            continue
        
        reward_data = np.array([[d[2], d[3]] for d in data])  # median, mean
        bitrate_data = np.array([[d[6], d[7]] for d in data])  # median, mean
        
        rects1, rects2 = create_bar_chart(axs[0, i], reward_data, f'{dataset}', 'Reward (QoE-HD)', algorithms)
        create_bar_chart(axs[1, i], bitrate_data, "", 'Bitrate (kbps)', algorithms)
    
    # Add a single legend for the entire figure
    fig.legend((rects1, rects2), ('Median', 'Mean'), loc='upper right', bbox_to_anchor=(0.99, 0.99), ncol=2)
    
    plt.tight_layout()
    plt.subplots_adjust(top=0.95, right=0.99)
    plt.show()

# Define colors
color_median = '#87CEEB'  # Sky Blue
color_mean = '#FF7F50'    # Coral

# Input data as a string

dataset1 = {
    "FCC httpget Broadband": """
Buffer-based	21	875	870	762.1	1059.38	4017.71	3987.5	3569.05
MPC	102.8	922	922	823.34	1096.88	4226.04	4226.04	3780.42
Oboe MPC	117	922	914	812.08	1012.5	4226.04	4175	3696.83
Pensieve (Max)	-61.07	922	922	798.42	1190.62	4226.04	4226.04	3760.08
Transfer (50k)	-32.59	922	922	801.97	1171.88	4226.04	4226.04	3782.35
Transfer (Max)	158.96	922	922	825.28	1011.46	4226.04	4226.04	3751.85


    """
}
dataset2 = {
    "Belgium/Cork 4G/LTE": """
Buffer-based	-125	875	870	767.47	618.75	4017.71	3987.5	3684.87
MPC	77	922	922	835.3	628.12	4226.04	4226.04	3890.59
Oboe MPC	-193.39	922	922	798.95	571.88	4226.04	4226.04	3798.99
Pensieve (Max)	-97.63	922	904.5	801.32	535.42	4226.04	4170.31	3727.79
Transfer (50k)	-47.6	922	914	788.69	547.92	4226.04	4185.42	3778.26
Transfer (Max)	15.82	922	907	797.89	538.54	4226.04	4195.83	3810.11

    """
}
dataset3 = {
    "Norway 3G/HSDPA": """
Buffer-based	-74.13	826	67	134.11	459.38	3883.33	959.9	1132.59
MPC	27.48	899.64	109.89	195.84	450	4195.83	986.46	1181.17
Oboe MPC	-80.18	885	110.73	110.73	393.75	3991.67	920.31	1092
Pensieve (Max)	40	864	154	215.73	365.62	3896.88	840.62	1017.94
Transfer (50k)	22.44	591	136.37	180.89	337.5	2443.75	844.27	995.11
Transfer (Max)	-78.59	856	175.7	209.13	337.5	394.38	907.81	1070.86

    """
}

# Combine all datasets
all_datasets = {**dataset1, **dataset2, **dataset3}

# Plot the multi-dataset comparison
plot_multi_dataset_comparison(all_datasets)

print("\nInput Data Format:")
print("Each line should contain: Algorithm_Name<tab>min_reward<tab>max_reward<tab>median_reward<tab>mean_reward<tab>min_bitrate<tab>max_bitrate<tab>median_bitrate<tab>mean_bitrate")
print("with one line for each algorithm")