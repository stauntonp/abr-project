import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import numpy as np

# Define the dataset
data = {
    "3G Norway": [10.73, 5.25, 7.34],
    "4G/LTE": [43.64, 44.50, 46.76],
    "FCC Broadband": [48.64, 49.69, 47.88],
    "3G Norway Slice": [17.92, 8.69, 28.64],
    "4G/LTE Slice": [46.69, 46.32, 50.66],
    "FCC Broadband Slice": [50.08, 51.83, 51.86],
    "Mean (Normalised)": [31.18, 28.03, 32.67]
}

def create_grouped_bar_chart(data):
    datasets = list(data.keys())
    n_datasets = len(datasets)
    n_algorithms = 3  # RL, TL 50k, TL Max
    
    fig, ax = plt.subplots(figsize=(12, 6))
    
    index = np.arange(n_datasets)
    bar_width = 0.25
    
    colors = ['#FF7F50', '#87CEEB', '#4682B4']  # Salmon, Light Blue, Dark Blue
    
    # Add gridlines first (lower z-order)
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.grid(which='major', axis='y', linestyle='-', alpha=0.4, zorder=0)
    ax.grid(which='minor', axis='y', linestyle=':', alpha=0.2, zorder=0)
    
    for i in range(n_algorithms):
        values = [data[dataset][i] for dataset in datasets]
        rects = plt.bar(index + i*bar_width, values, bar_width,
                        color=colors[i], label=['RL', 'TL 50k', 'TL Max'][i],
                        zorder=3)  # Higher z-order to appear above gridlines
    
    plt.ylabel('Mean Reward (QoE-linear)')
    plt.title('Comparison of RL, TL 50k, and TL Max across Datasets')
    plt.xticks(index + bar_width, datasets, rotation=45, ha='right')
    plt.legend()
    
    # Remove top and right spines
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    
    # Keep the bottom spine (x-axis line) visible
    ax.spines['bottom'].set_visible(True)
    
    # Remove x-axis ticks
    ax.tick_params(axis='x', which='both', length=0)
    
    plt.tight_layout()
    plt.show()
    
# Create the grouped bar chart
create_grouped_bar_chart(data)