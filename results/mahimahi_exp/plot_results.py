import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import numpy as np

# Input data as a string
input_data = """
FESTIVE	9.75	23.9	23.9	22.65	750	1910.71	1910.71	1809.6
BB	42.9	42.9	42.9	42.9	3435.71	3435.71	3435.71	3435.71
BOLA	9.3	52.35	52.35	45.31	782.14	4046.43	4046.43	3517.63
robust MPC	11.1	52.35	52.35	49.59	1060.71	4046.43	4046.43	3847.9
oboe MPC	14.756	52.35	49.9	46.09	1250	4046.43	3848.21	3590.89
Pensieve (RL)	24.65	52.35	52.35	48.64	2067.86	4046.43	4046.43	3781.92
Transfer (50k)	27.95	52.35	52.35	49.69	2303.57	4046.43	4046.43	3856.07
Transfer (Max)	14.85	52.35	52.35	47.88	1367.86	4046.43	4046.43	3728.12
"""
# Used to contract title of chart
dataset = "FCC httpget Broadband"
# dataset = "Belgium/Cork 4G/LTE"
# dataset = "Norway HSDPA"
sliced = " "#(Network Sliced)"

def process_input_data(input_string):
    lines = input_string.strip().split('\n')
    processed_data = []
    algorithms = []
    for line in lines:
        parts = line.split()
        try:
            # Assume the last 8 parts are numeric values
            values = [float(val) for val in parts[-8:]]
            # Everything before the numeric values is the algorithm name
            algorithm = ' '.join(parts[:-8])
            algorithms.append(algorithm)
            if len(values) == 8:  # Ensure correct number of values
                processed_data.append(values)
            else:
                print(f"Warning: Skipping invalid line (incorrect number of values): {line}")
        except ValueError:
            print(f"Warning: Skipping invalid line (non-numeric value): {line}")
    return processed_data, algorithms

def create_bar_chart(ax, data, title, ylabel, algorithms):
    x = np.arange(len(algorithms))
    width = 0.35

    medians = data[:, 0]
    means = data[:, 1]

    # Set zorder for bars directly in the bar() function
    ax.bar(x - width/2, medians, width, label='Median', color=color_median, zorder=3)
    ax.bar(x + width/2, means, width, label='Mean', color=color_mean, zorder=3)

    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.set_xticks(x)
    ax.set_xticklabels(algorithms, rotation=45, ha='right')
    ax.legend()

    # Add major gridlines
    ax.grid(axis='y', linestyle='-', alpha=0.2, zorder=0)
    
    # Add minor gridlines
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.grid(axis='y', which='minor', linestyle=':', alpha=0.2, zorder=0)

    # Remove top and right spines
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # Ensure ticks are only on the left and bottom
    ax.tick_params(axis='y', which='both', left=True, right=False)
    ax.tick_params(axis='x', which='both', bottom=True, top=False)

# Define colors
color_median = '#87CEEB'  # Sky Blue
color_mean = '#FF7F50'    # Coral

# Process input data
data, algorithms = process_input_data(input_data)

if len(data) != len(algorithms):
    print(f"Error: Mismatch between data rows ({len(data)}) and algorithms ({len(algorithms)}). Please check your input data.")
else:
    # Prepare data for plotting
    reward_data = np.array([[d[2], d[3]] for d in data])  # median, mean
    bitrate_data = np.array([[d[6], d[7]] for d in data])  # median, mean

    # Create the plot
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 8))

    create_bar_chart(ax1, reward_data, 'Reward Comparison', 'Reward', algorithms)
    create_bar_chart(ax2, bitrate_data, 'Bitrate Comparison', 'Bitrate (kbps)', algorithms)

    # Add a combined title for the entire figure
    fig.suptitle(f"Mahimahi Experiments with {dataset} Dataset{sliced}", fontsize=16)

    plt.tight_layout()
    plt.subplots_adjust(top=0.93)  # Adjust the top margin to make room for the suptitle
    plt.show()

print("\nInput Data Format:")
print("Each line should contain: Algorithm_Name<tab>min_reward<tab>max_reward<tab>median_reward<tab>mean_reward<tab>min_bitrate<tab>max_bitrate<tab>median_bitrate<tab>mean_bitrate")
print("with one line for each algorithm")