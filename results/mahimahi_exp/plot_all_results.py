import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import numpy as np

# Define your datasets here
datasets = {
    "FCC Broadband": """
BOLA	9.3	52.35	52.35	45.31	782.14	4046.43	4046.43	3517.63
Robust MPC	11.1	52.35	52.35	49.59	1060.71	4046.43	4046.43	3847.9
Oboe MPC	14.756	52.35	49.9	46.09	1250	4046.43	3848.21	3590.89
RL (Max)	24.65	52.35	52.35	48.64	2067.86	4046.43	4046.43	3781.92
TL (50k)	27.95	52.35	52.35	49.69	2303.57	4046.43	4046.43	3856.07
TL (Max)	14.85	52.35	52.35	47.88	1367.86	4046.43	4046.43	3728.12
    """,
    "FCC Broadband-Network Slice": """
BOLA	52.35	52.35	52.35	52.35	4046.43	4046.43	4046.43	4046.43
Robust MPC	52.35	52.35	52.35	52.35	4046.43	4046.43	4046.43	4046.43
Oboe MPC	44.45	52.35	49.45	48.99	3482.14	4046.43	3839.29	3806.43
RL (Max)	27.85	52.35	52.35	50.08	2582.14	4046.43	4046.43	3907.36
TL (50k)	50.9	52.35	52.35	51.83	3942.86	4046.43	4046.43	4009.14
TL (Max)	50.9	52.35	52.35	51.86	3942.86	4046.43	4046.43	4011.21
    """,
    "Belgium/Cork 4G/LTE": """
BOLA	6.15	52.35	52.35	46.93	576.92	4046.32	4046.43	3593.6
Robust MPC	2.8	52.35	52.35	48.21	442.86	4046.43	4046.43	3672.43
Oboe MPC	9.75	50.9	49.9	47.44	750	3942.86	3871.43	3623.8
RL (Max)	1.9	52.35	49.9	43.64	442.86	4046.43	3871.43	3367.77
TL (50k)	3.45	52.35	49.25	44.5	621.43	4046.43	3825	3418.9
TL (Max)	3.45	52.35	50.9	46.76	332.14	4046.43	3942.86	3559.35
    """,
    "Belgium/Cork 4G/LTE-Network Slice": """
BOLA	46.55	52.35	52.35	51.18	3735.71	4046.43	4046.43	4038.14
Robust MPC	52.35	52.35	52.35	52.35	4046.43	4046.43	4046.43	4046.43
Oboe MPC	44.35	52.35	52.35	50.19	3514.29	4046.43	4046.43	3893.43
RL (Max)	27.85	52.35	52.35	46.69	2582.14	4046.43	4046.43	3700.71
TL (50k)	27.55	52.35	52.35	46.32	2414.29	4046.43	4046.43	3638.14
TL (Max)	42.45	52.35	52.35	50.66	3546.43	4046.43	4046.43	3930
    """,
    "Norway 3G/HSDPA": """
BOLA	3.45	34.95	9.3	10.66	332.14	2700	750	862.25
Robust MPC	2.55	50.9	10.55	14.07	332.14	3942.86	950	1212.99
Oboe MPC	3.45	41.8	13.5	16.77	364.29	3292.86	1135.71	1357.04
RL (Max)	3.9	36.55	7.45	10.73	428.57	2917.86	746.43	950.94
TL (50k)	3	29.4	3.45	5.25	332.14	2367.86	332.14	506.18
TL (Max)	3	38.55	3.45	7.34	332.14	3125	364.29	669.6
    """,
    "Norway 3G/HSDPA-Network Slice": """
BOLA	13.85	34.95	16.45	18.75	1167.86	2700	1400	1502.14
Robust MPC	34.75	52.35	39.2	40.09	2789.29	4046.43	3107.14	3171.07
Oboe MPC	25.35	43.1	27.58	29.64	2117.86	3385.75	2276.79	2424.29
RL (Max)	14.55	36.55	14.55	17.92	1235.71	2917.86	1410.71	1571.79
TL (50k)	5	29.4	6	8.69	625	2367.86	696.43	903.86
TL (Max)	23.7	47.35	26.8	28.64	2000	3689.29	2221.43	2352.5
    """
}

def process_input_data(input_string):
    lines = input_string.strip().split('\n')
    processed_data = []
    algorithms = []
    for line in lines:
        parts = line.split()
        algorithms.append(' '.join(parts[:-8]))
        values = [float(val) for val in parts[-8:]]
        processed_data.append(values)
    return np.array(processed_data), algorithms

def set_axis_limit(ax, values):
    min_val = min(values)
    max_val = max(values)
    if max_val > 100:
        ax.set_ylim(bottom=min_val * 0.5)
    else:
        ax.set_ylim(bottom=0)
    ax.set_ylim(top=max_val * 1.1)  # Add 10% padding at the top

def create_paired_reward_chart(ax, data1, data2, title, algorithms1, algorithms2):
    x = np.arange(max(len(algorithms1), len(algorithms2)))
    width = 0.35

    mean_rewards1 = data1[:, 3]
    mean_rewards2 = data2[:, 3]

    rects1 = ax.bar(x - width/2, mean_rewards1, width, label='Original', color='#87CEEB', zorder=3)
    rects2 = ax.bar(x + width/2, mean_rewards2, width, label='Network Slice', color='#FF7F50', zorder=3)

    ax.set_ylabel('Reward (QoE-linear)')
    ax.set_title(title)
    
    # Remove x-axis ticks
    ax.set_xticks([])
    
    # Add x-axis labels below the bars
    ax.set_xticks(x)
    ax.set_xticklabels(algorithms1 if len(algorithms1) > len(algorithms2) else algorithms2, rotation=45, ha='right')
    ax.tick_params(axis='x', which='major', pad=5)

    set_axis_limit(ax, np.concatenate((mean_rewards1, mean_rewards2)))
    
    # Add only y-axis gridlines
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.grid(which='major', axis='y', linestyle='-', alpha=0.4, zorder=0)
    ax.grid(which='minor', axis='y', linestyle=':', alpha=0.2, zorder=0)
    
    ax.tick_params(axis='y')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    
    # Keep the bottom spine (x-axis line) visible
    ax.spines['bottom'].set_visible(True)
    
    # Remove x-axis ticks
    ax.tick_params(axis='x', which='both', length=0)

    return rects1, rects2

    return rects1, rects2

def plot_paired_dataset_comparison(datasets):
    fig, axs = plt.subplots(3, 1, figsize=(12, 18))
    
    dataset_pairs = [
        ("FCC Broadband", "FCC Broadband-Network Slice"),
        ("Belgium/Cork 4G/LTE", "Belgium/Cork 4G/LTE-Network Slice"),
        ("Norway 3G/HSDPA", "Norway 3G/HSDPA-Network Slice")
    ]
    
    for i, (dataset1, dataset2) in enumerate(dataset_pairs):
        data1, algorithms1 = process_input_data(datasets[dataset1])
        data2, algorithms2 = process_input_data(datasets[dataset2])
        
        title = dataset1  # Take title of dataset1, excluding the -Network Slice part
        rects1, rects2 = create_paired_reward_chart(axs[i], data1, data2, title, algorithms1, algorithms2)
    
    # Add a single legend for the entire figure
    fig.legend((rects1, rects2), ('Original', 'Network Slice'), loc='upper right', bbox_to_anchor=(0.95, 0.98), ncol=2)
    
    plt.tight_layout()
    plt.subplots_adjust(top=0.95, hspace=0.3)
    plt.show()


# Plot the paired dataset comparison
plot_paired_dataset_comparison(datasets)

print("\nInput Data Format:")
print("Each line should contain: Algorithm_Name<tab>min_reward<tab>max_reward<tab>median_reward<tab>mean_reward<tab>min_bitrate<tab>max_bitrate<tab>median_bitrate<tab>mean_bitrate")
print("with one line for each algorithm")